# Data Explorer

The .Stat Data Explorer (DE) is a JavaScript application for easy finding, understanding and using data through an efficient well-tuned navigation and search approach, appropriate data previews, and download in standard formats, APIs or share features.

## Webapp

This project uses boilerplate dotstatsuite-webapp
  - Multitenant
  - Proxy
  - Config

You can find more about [here](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-webapp)

## Usage

To start Data-Explorer

First, start a config server:

### Config

```
$ docker run -d --name config --restart always -p 5007:80 siscc/dotstatsuite-config-dev:latest
```

If unlikely you cannot run docker images on your development workstation or you want a custom config,
see how to [run config server](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-config) from source.

### Setup

Clone the data-explorer repository.
```
$ yarn
```
Now start `de` server:

```
$ yarn start:srv
...
server started
```

and proxy
```
$ yarn start:proxy
```

Launch your preferred browser on http://localhost:7000

If it does not work, check log messages

# How to use the contact form

## configuration

### Variables
To be able to send an email you have to setup the following variables:
- MAIL_FROM
- MAIL_TO
- HFROM (optionnal)
- SMTP_[see smtp readme](https://gitlab.com/group_mike/dotstatsuite-mailer)

To be able to use the captcha you have to setup the following variables
- CAPTCHA_SECRET_KEY (**private**, used by the server to verify the user captcha data)
- CAPTCHA_SITE_KEY (public, used in the client)

### How retrieve captcha secret key/site
https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-explorer/-/issues/654#note_1095119875

### display captcha
First, the contact form will still work there is not captcha that has been configured.
if one of these variable is not set then the captcha will be hidden.

## settings.json
It is possible to disable the captcha and/or the contact form:
```js
{
  app: {
    contact: {
      form: false,
      captcha: false
    }
  }
}
```
By default value are set to true

# Sentry
Sentry is used to gather data from live data explorers.
Check [Sentry](https://sentry.io/) for more info.

## Profiled components
Sentry allows to profiles React components, mainly to understand their impact of the performance (ie the user experience).
Each profiled component has a name to help find it in Sentry UI.

List of profiled components:
- the table in the visualisation page, named `Visualisation table`

## Env variables
These variables are used by the app (sentry client) and should be injected at deployment stage.

- `SENTRY_DSN` provided by sentry
- `SENTRY_ENV` free value, ie qa, staging
- `SENTRY_RELEASE` free value, ie develop, master or tags

_note: org and project (see gitlab variables) are included in the dsn._

### gitlab
These variables are used in the gitlab pipeline to send sourcemaps to sentry.
Data explorer production build doesn't have sourcemaps and sourcemaps are not sent to client, it's sentry that maps error to sources.

- `SENTRY_AUTH_TOKEN` provided by sentry
- `SENTRY_ORG` provided by sentry
- `SENTRY_PROJECT` free value, default 'dotstatsuite-data-explorer'