module.exports = {
  clearMocks: true,
  coverageReporters: ['text'],
  coverageDirectory: 'coverage',
  collectCoverageFrom: [
    '!src/proxy/**',
    '!src/mock/**',
    '!src/**/*.test.{js,jsx}',
    '!**/*e2e.js',
    'src/**/*.js',
  ],
  testRegex: 'src/.*\\.test\\.js$',
  testEnvironment: 'jsdom',
  testURL: 'http://localhost',
  transform: {
    '\\.[jt]sx?$': 'babel-jest',
    '^.+\\.css$': '<rootDir>/config/jest/cssTransform.js',
    '^.+\\.png$': '<rootDir>/config/jest/fileTransform.js',
  },
  transformIgnorePatterns: [
    '[/\\\\]node_modules[/\\\\].+\\.(js|jsx|ts|tsx)$',
    '^.+\\.module\\.(css|sass|scss)$',
  ],
};
