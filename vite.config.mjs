import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import commonjs from 'vite-plugin-commonjs';
import { nodePolyfills } from 'vite-plugin-node-polyfills';
import replace from '@rollup/plugin-replace';
import viteCompression from 'vite-plugin-compression';
import { sentryVitePlugin } from "@sentry/vite-plugin";

export default defineConfig(() => ({
  plugins: [
    commonjs(),
    nodePolyfills(),
    react(),
    sentryVitePlugin({
      authToken: process.env.SENTRY_AUTH_TOKEN,
      org: process.env.SENTRY_ORG,
      release: process.env.SENTRY_RELEASE || 'development',
      project: process.env.SENTRY_PROJECT || 'dotstatsuite-data-explorer',
      reactComponentAnnotation: { enabled: true },
    }),
    replace({
      preventAssignment: true,
      delimiters: ['', ''],
      values: {
        'var d3_document = this.document;':
          'var d3_document = globalThis.document;',
        'this[d3_vendorSymbol(this, "requestAnimationFrame")]':
          'globalThis[d3_vendorSymbol(globalThis, "requestAnimationFrame")]',
        'this.navigator && /WebKit/.test(this.navigator.userAgent)':
          'globalThis.navigator && /WebKit/.test(globalThis.navigator.userAgent)',
      },
      include: ['node_modules/d3/**'],
    }),
    viteCompression({ algorithm: 'brotliCompress' }),
  ],
  build: {
    minify: true,
    manifest: true,
    sourcemap: true, // for sentry
    chunkSizeWarningLimit: 5000,
    outDir: 'build',
    rollupOptions: {
      input: 'src/web/index.js',
      output: {
        entryFileNames: 'static/js/bundle.js',
        chunkFileNames: 'static/js/vendors~main.chunk.js',
        manualChunks(id) {
          if (id.includes('node_modules')) {
            return 'vendors';
          }
        },
      },
    },
  },
  esbuild: {
    loader: 'jsx',
    include: /src\/.*\.jsx?$/,
    exclude: [],
  },
}));
