const NodeEnvironment = require('jest-environment-node');
const chalk = require('chalk');
const fs = require('fs');
const path = require('path');
const puppeteer = require('puppeteer');
const os = require('os');

const DIR = path.join(os.tmpdir(), 'jest_puppeteer_global_setup');
const srcPath = path.resolve(__dirname, '../..', 'dist/server');

console.log(chalk.green(`Resolving tests from ${srcPath}`));

const runE2EServer = require(path.resolve(srcPath, 'e2e'));
const ConfigProvider = require(path.resolve(srcPath, 'configProvider')).default;

const e2eConfig = {
  appId: 'data-explorer',
  env: 'production',
  isProduction: true,
  server: { host: '127.0.0.1' },
};

async function launchE2EServer(config) {
  console.log(chalk.green('Setup E2E servers'));
  const E2E = {
    makeUrl(urlPath, name = 'proxy') {
      return `${this.servers.get(name).url}${urlPath}`;
    },
    servers: (function ServerFactory() {
      const s = {};
      return {
        add: (name, server) => (s[name] = server),
        get: name => s[name],
        close: () => {
          s['de'].close();
          s['proxy'].close();
        },
      };
    })(),
  };

  const configServer = global._CONFIG_;
  const sfsServer = global._SFS_;
  const nsiServer = global._NSI_;
  process.env.SFS_URL = new URL('api', sfsServer.url);
  process.env.NSI_URL = new URL('rest', nsiServer.url);
  const newConfig = {
    ...config,
    configUrl: configServer.url,
    authServerUrl: configServer.url,
  };

  return runE2EServer({
    config: newConfig,
    configProvider: ConfigProvider(newConfig),
  }).then(ctx => {
    E2E.servers.add('de', ctx.httpServer);
    E2E.servers.add('config', configServer);
    E2E.servers.add('sfs', sfsServer);
    E2E.servers.add('nsi', nsiServer);
    E2E.servers.add('proxy', ctx.proxyServer);
    E2E.ctx = ctx;
    return E2E;
  });
}

class E2EEnvironment extends NodeEnvironment {
  constructor(config) {
    super(config);
  }

  async setup() {
    await super.setup();
    const wsEndpoint = fs.readFileSync(path.join(DIR, 'wsEndpoint'), 'utf8');
    if (!wsEndpoint) {
      throw new Error('wsEndpoint not found');
    }
    try {
      this.global._E2E_ = await launchE2EServer(e2eConfig);
      this.global.__BROWSER__ = await puppeteer.connect({
        browserWSEndpoint: wsEndpoint,
      });
    } catch (e) {
      console.error(e);
    }
  }

  async teardown() {
    console.log(chalk.yellow('Teardown Test Environment.'));
    await super.teardown();
    this.global._E2E_.servers.close();
  }
  runScript(script) {
    return super.runScript(script);
  }
}

module.exports = E2EEnvironment;
