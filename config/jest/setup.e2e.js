const puppeteer = require('puppeteer');
const mkdirp = require('mkdirp');
const path = require('path');
const fs = require('fs');
const os = require('os');
const { runServer } = require('farso/server');
const configFarsoConfig = require('../../src/mocks/config/farso.config.js');
const sfsFarsoConfig = require('../../src/mocks/sfs/farso.config.js');
const nsiFarsoConfig = require('../../src/mocks/nsi/farso.config');
const DIR = path.join(os.tmpdir(), 'jest_puppeteer_global_setup');

const isCI = process.env.E2E_ENV === 'ci';

module.exports = async function() {
  const browser = await puppeteer.launch({
    headless: isCI, // to browse visual
    args: [
      '--lang=fr-fr',
      '--no-sandbox',
      '--runInBand',
      '--window-size=1200,940',
      '--disable-dev-shm-usage',
      '--user-agent="Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3312.0 Safari/537.36"',
    ],
  });
  global._BROWSER_ = browser;
  mkdirp.sync(DIR);
  fs.writeFileSync(path.join(DIR, 'wsEndpoint'), browser.wsEndpoint());
  const { server: mockConfig } = await runServer(configFarsoConfig);
  global._CONFIG_ = mockConfig;
  const { server: mockSfs } = await runServer(sfsFarsoConfig);
  global._SFS_ = mockSfs;
  const { server: mockNsi } = await runServer(nsiFarsoConfig);
  global._NSI_ = mockNsi;
  //await browser.close();
};
