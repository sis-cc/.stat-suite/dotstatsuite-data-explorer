const mockedDate = new Date();
global.Date = class {
  constructor() {
    return mockedDate;
  }
};
global.Date.now = () => mockedDate;

new Date();
Date.now();
