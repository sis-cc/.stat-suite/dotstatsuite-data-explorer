export const selection = {
  cf5d5b8f74d437ad06eb6a6e333018a1: {
    filterId: 'topic_s',
    valueId: 'TESTAUT_CS1|TESTAUT_CAT1',
  },
};

export const api = {
  baseURL: 'http://dotstatcor-dev1.main.oecd.org/Demo2SearchService/search/1.1/Search',
  responseType: 'json',
};

export const categorySchemeId = 'OECD:TESTAUT_CS1(1.0)';
export const pageSize = '20';
export const page = 1;
export const term = 'test';
