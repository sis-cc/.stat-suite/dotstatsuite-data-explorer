import React, { Fragment } from 'react';
import Collapse from '@mui/material/Collapse';
import makeStyles from '@mui/styles/makeStyles';
import { ShortUrls } from '@sis-cc/dotstatsuite-visions';
import { IconButton, Paper } from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import useShortenUrl from '../../hooks/useShortenUrl';

const useStyles = makeStyles(theme => ({
  container: {
    margin: theme.spacing(1, 0, 1, 0),
    padding: theme.spacing(2),
  },
}));

const ShortUrl = ({ open, setOpen }) => {
  const classes = useStyles();
  const { props, shortenUrl } = useShortenUrl();

  return (
    <Fragment>
      <Collapse in={open}>
        <Paper elevation={2} className={classes.container}>
          <IconButton
            style={{ float: 'right' }}
            size="small"
            color="primary"
            onClick={() => setOpen(false)}
          >
            <CloseIcon fontSize="small" />
          </IconButton>
          <ShortUrls {...props} onClick={shortenUrl} />
        </Paper>
      </Collapse>
    </Fragment>
  );
};

export default ShortUrl;
