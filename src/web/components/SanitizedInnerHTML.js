import React from 'react';
import * as R from 'ramda';
import sanitizeHtml from 'sanitize-html';
import { sanitizeOptions } from '../lib/settings';

const SanitizedInnerHTML = ({ html = '', ...rest }) => {
  if (!R.is(String, html)) {
    return html;
  }
  return (
    <span
      dangerouslySetInnerHTML={{ __html: sanitizeHtml(html, sanitizeOptions) }}
      {...rest}
    />
  );
};

export default SanitizedInnerHTML;
