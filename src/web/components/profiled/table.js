import React from 'react';
import { withProfiler } from '@sentry/react';
import { Viewer } from '@sis-cc/dotstatsuite-components';

const Table = props => <Viewer {...props} />;

export default withProfiler(Table, { name: 'Visualisation table' });
