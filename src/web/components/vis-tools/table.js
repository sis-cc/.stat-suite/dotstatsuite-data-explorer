import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import * as R from 'ramda';
import { rules2 } from '@sis-cc/dotstatsuite-components';
import { TableLayout } from '@sis-cc/dotstatsuite-visions';
import { useIntl } from 'react-intl';
import { FormattedMessage, formatMessage } from '../../i18n';
import { changeLayout, changeTimeDimensionOrders } from '../../ducks/vis';
import { getVisDimensionFormat } from '../../selectors';
import { getTimePeriodArtefact } from '../../selectors/sdmx';
import {
  getHasAccessibility,
  getTableLayout as getRouterLayoutIds,
  getTimeDimensionOrders,
} from '../../selectors/router';
import messages from '../messages';
import {
  cleanLayoutIds,
  getTableConfigLayout,
  getLayoutCombinations,
  refineWithCurrent,
} from '../../lib/layout';
import { getObsAttributes } from '../../selectors/data';
import useDataTable from '../../hooks/useDataTable';

const Table = () => {
  const intl = useIntl();
  const dispatch = useDispatch();
  const _changeLayoutHandler = (...args) => dispatch(changeLayout(...args));
  const changeTimeDimensionOrdersHandler = (...args) =>
    dispatch(changeTimeDimensionOrders(...args));

  const accessibility = useSelector(getHasAccessibility);
  const itemRenderer = useSelector(getVisDimensionFormat());
  const itemButton = useSelector(getTimeDimensionOrders);
  const timePeriod = useSelector(getTimePeriodArtefact);
  const obsAttributes = useSelector(getObsAttributes);
  const routerLayoutIds = useSelector(getRouterLayoutIds);
  const { layout, combinations } = useDataTable();
  const configLayout = getTableConfigLayout(layout);
  const _combinations = getLayoutCombinations(combinations);
  const changeLayoutHandler = layout => {
    const next = R.pipe(
      l => rules2.injectCombinationsInLayout(_combinations, l),
      l => refineWithCurrent(routerLayoutIds, l, _combinations),
      l => cleanLayoutIds(_combinations, l),
    )(layout);
    _changeLayoutHandler(next);
  };

  const asc = formatMessage(intl)(messages.asc);
  const desc = formatMessage(intl)(messages.desc);
  const timePeriodId = R.propOr('', 'id')(timePeriod);
  const isTimeDimensionInverted = R.propOr(false, timePeriodId)(itemButton);
  const timePeriodButton = {
    ...timePeriod,
    value: isTimeDimensionInverted ? desc : asc,
    options: [asc, desc],
    onChange: v =>
      changeTimeDimensionOrdersHandler(timePeriodId, R.equals(desc)(v)),
  };

  return (
    <TableLayout
      {...configLayout}
      accessibility={accessibility}
      commit={changeLayoutHandler}
      itemButton={itemButton}
      itemRenderer={itemRenderer}
      itemButtonProps={{ [timePeriodId]: timePeriodButton }}
      optionalItem={
        R.isEmpty(obsAttributes)
          ? null
          : {
              id: 'OBS_ATTRIBUTES',
              name: formatMessage(intl)(messages.layoutAttributes),
              activationLabel: formatMessage(intl)(
                messages.activateLayoutAttributes,
              ),
              count: obsAttributes.length + 1,
            }
      }
      labels={{
        commit: formatMessage(intl)(messages.apply),
        cancel: formatMessage(intl)(messages.cancelChanges),
        row: formatMessage(intl)(messages.row),
        column: formatMessage(intl)(messages.column),
        section: formatMessage(intl)(messages.section),
        asc: <FormattedMessage id="de.table.layout.time.asc" />,
        desc: <FormattedMessage id="de.table.layout.time.desc" />,
        help: <FormattedMessage id="de.table.layout.help" />,
        table: <FormattedMessage id="de.table.layout.table" />,
        one: <FormattedMessage id="de.table.layout.value.one" />,
        wcagDragStart: props => formatMessage(intl)(messages.start, props),
        wcagDragUpdate: props => formatMessage(intl)(messages.update, props),
        wcagDragEnd: props => formatMessage(intl)(messages.end, props),
        wcagDragCancel: formatMessage(intl)(messages.cancel),
        wcagDragExplanation: formatMessage(intl)(messages.explanation),
      }}
    />
  );
};

export default Table;
