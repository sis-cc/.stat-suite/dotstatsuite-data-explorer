import { HierarchicalFilter } from '@sis-cc/dotstatsuite-visions';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import React from 'react';
import { useIntl } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import { changeDataquery } from '../../ducks/sdmx';
import { changeFilter } from '../../ducks/vis';
import { formatMessage } from '../../i18n';
import { getFilter, getVisDimensionFormat } from '../../selectors';
import {
  getHasAccessibility,
  getHasDataAvailability,
} from '../../selectors/router';
import { getFilters } from '../../selectors/sdmx';
import { getFilterLabel } from '../../utils';
import messages from '../messages';
import SanitizedInnerHTML from '../SanitizedInnerHTML';
import { tagAccessor } from './utils';
import useSdmxAC from '../../hooks/sdmx/useSdmxAC';
import useSdmxStructure from '../../hooks/useSdmxStructure';
import { displayChildren } from '../../lib/settings';

const Filters = ({ setAdvancedFilterState, defaultProps }) => {
  const intl = useIntl();
  const hasDataAvailability = useSelector(getHasDataAvailability);
  const { availableConstraints } = useSdmxAC();
  const { automatedSelections } = useSdmxStructure();
  const filters = useSelector(state =>
    getFilters(state, { availableConstraints, automatedSelections }),
  );
  const activePanelId = useSelector(getFilter);
  const accessibility = useSelector(getHasAccessibility);
  const labelAccessor = useSelector(getVisDimensionFormat());

  const dispatch = useDispatch();
  const changeSelection = (...args) => dispatch(changeDataquery(...args));
  const onChangeActivePanel = (...args) => dispatch(changeFilter(...args));

  const labels = {
    disableItemLabel: formatMessage(intl)(messages.disableItemLabel),
    placeholder: total => formatMessage(intl)(messages.primary, { total }),
    iconLabel: formatMessage(intl)(messages.advancedFilter),
    navigateNext: formatMessage(intl)(messages.next),
  };

  return R.map(
    ({ id, label, values = [] }) => (
      <HierarchicalFilter
        changeSelection={changeSelection}
        onChangeActivePanel={onChangeActivePanel}
        activePanelId={activePanelId}
        accessibility={accessibility}
        labels={labels}
        HTMLRenderer={SanitizedInnerHTML}
        disableAccessor={R.pipe(R.prop('isEnabled'), R.not)}
        displayAccessor={hasDataAvailability ? R.prop('hasData') : undefined}
        tagAccessor={tagAccessor}
        tagAriaLabel={(count, total) =>
          formatMessage(intl)(messages.tagLabel, { count, total })
        }
        labelRenderer={getFilterLabel(labelAccessor)}
        id={id}
        key={id}
        label={label}
        items={values}
        toggleBulk={props =>
          setAdvancedFilterState({
            id,
            label,
            items: values,
            isOpen: true,
            ...props,
          })
        }
        defaultSpotlight={
          R.has(id, defaultProps)
            ? R.path([id, 'spotlight'], defaultProps)
            : null
        }
        defaultExpandedIds={
          R.has(id, defaultProps)
            ? R.path([id, 'expandedIds'], defaultProps)
            : null
        }
        testId="filter"
        displayChildren={displayChildren}
      />
    ),
    filters,
  );
};

Filters.propTypes = {
  setAdvancedFilterState: PropTypes.func,
  defaultProps: PropTypes.object,
  isNarrow: PropTypes.bool,
};

export default Filters;
