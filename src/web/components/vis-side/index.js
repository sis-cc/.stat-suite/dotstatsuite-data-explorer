import * as R from 'ramda';
import React, { Fragment, useState } from 'react';
import { useDispatch } from 'react-redux';
import { changeDataquery } from '../../ducks/sdmx';
import LastNPeriod from './lastn-period';
import Period from './period';
import DataAvailability from './data-availability';
import FilterPeriod from './filter-period';
import AdvancedPopup from './advanced-popup';
import Filters from './filters';

const Side = () => {
  const [
    { isOpen, label, items, id, spotlight, expandedIds, filtersDefaultProps },
    setAdvancedFilterState,
  ] = useState({
    isOpen: false,
    items: [],
    id: null,
    label: null,
    spotlight: null,
    expandedIds: null,
    filtersDefaultProps: {},
  });

  const dispatch = useDispatch();

  return (
    <Fragment>
      <FilterPeriod>
        <Period />
        <LastNPeriod />
      </FilterPeriod>
      <AdvancedPopup
        isOpen={isOpen}
        items={items}
        id={id}
        title={label}
        onClose={() =>
          setAdvancedFilterState({
            isOpen: false,
            items: [],
            id: null,
            spotlight: null,
            expandedIds: null,
          })
        }
        changeSelection={(id, selection, state) => {
          setAdvancedFilterState({
            isOpen: false,
            items: [],
            id: null,
            spotlight: null,
            expandedIds: null,
            filtersDefaultProps: R.assoc(id, state, filtersDefaultProps),
          });
          dispatch(changeDataquery(id, selection));
        }}
        defaultExpandedIds={expandedIds}
        defaultSpotlight={spotlight}
      />
      <Filters
        setAdvancedFilterState={setAdvancedFilterState}
        defaultProps={filtersDefaultProps}
      />
      <DataAvailability />
    </Fragment>
  );
};

export default Side;
