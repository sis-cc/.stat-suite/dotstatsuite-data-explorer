import { PeriodPicker } from '@sis-cc/dotstatsuite-visions';
import dateFns from 'date-fns';
import * as R from 'ramda';
import React, { useEffect, useRef } from 'react';
import { useIntl } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import {
  CHANGE_FREQUENCY_PERIOD,
  changeFrequencyPeriod,
} from '../../ducks/sdmx';
import { formatMessage } from '../../i18n';
import {
  getAvailableFrequencies,
  getDatesBoundaries,
  getFrequency,
  getFrequencyArtefact,
  getPeriod,
} from '../../selectors/sdmx';
import { periodMessages } from '../messages';
import { getSdmxPeriod } from '../../lib/sdmx/frequency';
import useSdmxAC from '../../hooks/sdmx/useSdmxAC';
import { getTimePeriodBoundaries } from '../../lib/sdmx/accessors/constraints';
import { getLastNMode } from '../../selectors/router';
import { LASTNPERIODS } from '../../utils/used-filter';

const Period = () => {
  const intl = useIntl();
  const period = useSelector(getPeriod);
  const frequency = useSelector(getFrequency);
  const availableFrequencies = useSelector(getAvailableFrequencies);
  const boundaries = useSelector(getDatesBoundaries);
  const frequencyArtefact = useSelector(getFrequencyArtefact);
  const { availableConstraints } = useSdmxAC();
  const availableBoundaries = getTimePeriodBoundaries(availableConstraints);
  const lastNMode = useSelector(getLastNMode);
  const [availableStart, availableEnd] = availableBoundaries;
  const dispatch = useDispatch();
  const changePeriod = period =>
    dispatch(changeFrequencyPeriod({ valueId: frequency, period }));
  const changeFrequency = frequency =>
    dispatch(
      changeFrequencyPeriod({
        valueId: frequency,
        period: [
          dateFns.startOfYear(R.head(period)),
          dateFns.endOfYear(R.last(period)),
        ],
      }),
    );
  const prevState = useRef();
  const labels = R.mergeRight(
    R.reduce(
      (memo, [id, message]) => R.assoc(id, formatMessage(intl)(message), memo),
      {},
      R.toPairs(periodMessages),
    ),
  )(availableFrequencies);

  const newPeriod = R.map(date =>
    dateFns.compareAsc(date, availableStart) === -1 ||
    dateFns.compareDesc(date, availableEnd) === -1
      ? undefined
      : date,
  )(period);

  useEffect(() => {
    const action = {
      type: CHANGE_FREQUENCY_PERIOD,
      pushHistory: {
        pathname: '/vis',
        payload: { period: R.map(getSdmxPeriod(frequency))(newPeriod) },
      },
    };

    if (
      !R.equals(prevState.current?.availableBoundaries, availableBoundaries) &&
      !R.equals(prevState.current?.frequency, frequency) &&
      !R.isEmpty(availableBoundaries) &&
      !R.equals(lastNMode, LASTNPERIODS)
    ) {
      dispatch(R.assoc('request', 'getData', action));
    }
    prevState.current = { frequency, availableBoundaries };
  }, [availableBoundaries, frequency]);

  return (
    <PeriodPicker
      period={period}
      availableFrequencies={R.keys(availableFrequencies)}
      availableBoundaries={availableBoundaries}
      boundaries={boundaries}
      isBlank={R.isNil(period)}
      labels={labels}
      frequencyDisabled={R.pipe(
        R.prop('display'),
        R.equals(false),
      )(frequencyArtefact)}
      defaultFrequency={frequency}
      changePeriod={changePeriod}
      changeFrequency={changeFrequency}
    />
  );
};

export default Period;
