import { changeVibe, testidSelector, dimHeaderSelector } from './utils';

describe('table display of differents observations types', () => {
  let page;
  beforeAll(async done => {
    await changeVibe('config')('main');
    await changeVibe('nsi')('types');
    page = await global.__BROWSER__.newPage();
    await page.setViewport({ width: 1200, height: 800 });
    const url = global._E2E_.makeUrl(
      `/vis?tenant=oecd:de&lc=en&df[ds]=hybrid&df[id]=DF_TEST_GREGORIANDAY&df[ag]=OECD&df[vs]=1.0&vw=tb`,
    );
    await page.goto(url);
    done();
  });
  it('load the viz page and wait for the table to be displayed', async () => {
    await page.waitForSelector(testidSelector('vis-table'));
    if (process.env.E2E_ENV === 'debug') await page.waitFor(250);
  });
  it('test boolean values', async () => {
    const cells = await page.$$eval(
      dimHeaderSelector('DATATYPE', 'BOOLEAN'),
      cells => cells.map(cell => cell.textContent),
    );
    expect(cells).toEqual([
      'Yes',
      'No',
      '1',
      '0',
      'true',
      'false',
      '..',
      '..',
      '..',
      '..',
      '..',
      '..',
      '..',
      '..',
      '..',
    ]);
  });
  it('test string values', async () => {
    const cells = await page.$$eval(
      dimHeaderSelector('DATATYPE', 'STRING'),
      cells => cells.map(cell => cell.textContent),
    );
    expect(cells).toEqual([
      'string content',
      '..',
      '..',
      '..',
      '..',
      '..',
      '..',
      '..',
      '..',
      '..',
      '..',
      '..',
      '..',
      '..',
      '..',
    ]);
  });
  it('test month values', async () => {
    const cells = await page.$$eval(
      dimHeaderSelector('DATATYPE', 'MONTH'),
      cells => cells.map(cell => cell.textContent),
    );
    expect(cells).toEqual([
      '01',
      '10',
      '..',
      '..',
      '..',
      '..',
      '..',
      '..',
      '..',
      '..',
      '..',
      '..',
      '..',
      '..',
      '..',
    ]);
  });
  it('test month-day values', async () => {
    const cells = await page.$$eval(
      dimHeaderSelector('DATATYPE', 'MON-DAY'),
      cells => cells.map(cell => cell.textContent),
    );
    expect(cells).toEqual([
      '01-31',
      '10-13',
      '..',
      '..',
      '..',
      '..',
      '..',
      '..',
      '..',
      '..',
      '..',
      '..',
      '..',
      '..',
      '..',
    ]);
  });
  it('test day values', async () => {
    const cells = await page.$$eval(
      dimHeaderSelector('DATATYPE', 'DAY'),
      cells => cells.map(cell => cell.textContent),
    );
    expect(cells).toEqual([
      '28',
      '..',
      '..',
      '..',
      '..',
      '..',
      '..',
      '..',
      '..',
      '..',
      '..',
      '..',
      '..',
      '..',
      '..',
    ]);
  });
  it('test formatted number values', async () => {
    const cells = await page.$$eval(
      dimHeaderSelector('DATATYPE', 'NUMBER'),
      cells => cells.map(cell => cell.textContent),
    );
    expect(cells).toEqual([
      '654,958,468.6544899',
      '654,958,469',
      '654,958,468.7',
      '654,958,468.65',
      '654,958,468.654',
      '6,549,584.6865449',
      '6,549,585',
      '6,549,584.7',
      '6,549,584.69',
      '6,549,584.687',
      '654,958,468,654.4899',
      '654,958,468,654',
      '654,958,468,654.5',
      '654,958,468,654.49',
      '654,958,468,654.490',
    ]);
  });
  it('test html render', async () => {
    const htmlText = await page.$eval(
      '[headers*="DATATYPE=HTML"] > span > p > b',
      node => node.textContent,
    );
    expect(htmlText).toEqual('Bold');
  });
  it("click on locale 'Français'", async () => {
    await page.click('#languages');
    if (process.env.E2E_ENV === 'debug') await page.waitFor(1000);
    await page.click('li#fr');
    await page.waitForSelector(testidSelector('vis-table'));
  });
  it('re test boolean values', async () => {
    const cells = await page.$$eval(
      dimHeaderSelector('DATATYPE', 'BOOLEAN'),
      cells => cells.map(cell => cell.textContent),
    );
    expect(cells).toEqual([
      'Oui',
      'Non',
      '1',
      '0',
      'true',
      'false',
      '..',
      '..',
      '..',
      '..',
      '..',
      '..',
      '..',
      '..',
      '..',
    ]);
  });
  it('re test formatted number values', async () => {
    const cells = await page.$$eval(
      dimHeaderSelector('DATATYPE', 'NUMBER'),
      cells => cells.map(cell => cell.textContent),
    );
    expect(cells).toEqual([
      '654 958 468,6544899',
      '654 958 469',
      '654 958 468,7',
      '654 958 468,65',
      '654 958 468,654',
      '6 549 584,6865449',
      '6 549 585',
      '6 549 584,7',
      '6 549 584,69',
      '6 549 584,687',
      '654 958 468 654,4899',
      '654 958 468 654',
      '654 958 468 654,5',
      '654 958 468 654,49',
      '654 958 468 654,490',
    ]);
  });
  it('switch to fixed boolen type', async () => {
    await changeVibe('nsi')('types:boolean');
    await page.reload();
    await page.waitForSelector(testidSelector('vis-table'));
  });
  it('test boolean values', async () => {
    const cells = await page.$$eval(
      dimHeaderSelector('DATATYPE', 'BOOLEAN'),
      cells => cells.map(cell => cell.textContent),
    );
    expect(cells).toEqual([
      'Oui',
      'Non',
      'Oui',
      'Non',
      'Oui',
      'Non',
      '..',
      '..',
      '..',
      '..',
      '..',
      '..',
      '..',
      '..',
      '..',
    ]);
  });
});
