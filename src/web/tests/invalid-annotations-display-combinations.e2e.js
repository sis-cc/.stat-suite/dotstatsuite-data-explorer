import { changeVibe, testidSelector } from './utils';

describe('vis: invalid annotations and display data from sdmx in combinations', () => {
  let page;
  const urlFactory = () =>
    `/vis?tenant=oecd:de&lc=en&df[ds]=hybrid&df[id]=DSD_NASEC20%40DF_T7II_Q&df[ag]=OECD.SDD.NAD&df[vs]=1.0&av=true&pd=%2C&dq=Q..AUS..S12L.S11......XDC......&ly[rw]=INSTR_ASSET%2CCOUNTERPART_SECTOR&ly[cl]=TIME_PERIOD&ly[rs]=SECTOR&to[TIME_PERIOD]=false&lo=5&lom=LASTNPERIODS&vw=tb`;

  beforeAll(async done => {
    await changeVibe('config')('combinations');
    await changeVibe('nsi')('nase');
    page = await global.__BROWSER__.newPage();
    await page.setViewport({ width: 1200, height: 800 });
    const url = global._E2E_.makeUrl(urlFactory());
    await page.goto(url);
    done();
  });

  it('load the viz page and wait for the table to be displayed', async () => {
    await page.waitForSelector(testidSelector('vis-table'));
    if (process.env.E2E_ENV === 'debug') await page.waitFor(250);
  });
});
