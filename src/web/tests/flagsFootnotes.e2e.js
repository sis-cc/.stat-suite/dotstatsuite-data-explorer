import { changeVibe, testidSelector } from './utils';
import * as R from 'ramda';

describe('vis: flags and footnotes display', () => {
  let page;
  beforeAll(async done => {
    await changeVibe('config')('main');
    await changeVibe('nsi')('sna');
    page = await global.__BROWSER__.newPage();
    await page.setViewport({ width: 1200, height: 800 });
    const url = global._E2E_.makeUrl(
      `/vis?tenant=oecd:de&lc=en&df[ds]=hybrid&df[id]=SNA_TABLE1&df[ag]=OECD&df[vs]=1.0&av=true&pd=2019%2C2020`,
    );
    await page.goto(url);
    done();
  });
  it('switch to table view', async () => {
    await page.waitForSelector(testidSelector('table-button'));
    const tableButton = await page.$(testidSelector('table-button'));
    await tableButton.click();
  });
  it('load the viz page and wait for the table to be displayed', async () => {
    await page.waitForSelector(testidSelector('vis-table'));
    if (process.env.E2E_ENV === 'debug') await page.waitFor(250);
  });
  it('should find 20 flags icon in the page', async () => {
    const icons = await page.$$(testidSelector('cell-flags-footnotes-icon'));
    expect(R.length(icons)).toEqual(20);
  });
  it('hover a flag icon should display tooltip', async () => {
    const iconHandle = await page.$(
      testidSelector('cell-flags-footnotes-icon'),
    );
    await iconHandle.hover();
    iconHandle.dispose();
    await page.waitForSelector(testidSelector('cell-flags-footnotes-content'));
    const tooltip = await page.$(
      testidSelector('cell-flags-footnotes-content'),
    );
    expect(tooltip).not.toBe(null);
  });
});
