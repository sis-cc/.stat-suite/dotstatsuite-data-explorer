import { changeVibe, testidSelector } from './utils';

describe('no time period tests', () => {
  let page;
  beforeAll(async done => {
    await changeVibe('config')('main');
    await changeVibe('nsi')('no_time');
    page = await global.__BROWSER__.newPage();
    await page.setViewport({ width: 1200, height: 800 });
    const url = global._E2E_.makeUrl(
      `/vis?tenant=oecd:de&lc=en&df[ds]=hybrid&df[id]=CEN18_HOU&df[ag]=STATSNZ&df[vs]=1.0&vw=tb&lo=5&lom=LASTNPERIODS`,
    );
    await page.goto(url);
    done();
  });
  it('load the viz page and wait for the table to be displayed', async () => {
    await page.waitForSelector(testidSelector('vis-table'));
    if (process.env.E2E_ENV === 'debug') await page.waitFor(250);
  });
  it('should not display time related content in applied filters', async () => {
    const appliedChips = await page.$$eval(
      testidSelector('chips-test-id'),
      nodes => nodes.map(n => n.ariaLabel),
    );
    expect(appliedChips).toEqual([
      'Access to basic amenities:',
      'Sector of landlord:',
    ]);
  });
  it('url should be cleaned of non relevant parameters', async () => {
    const url = await page.url();
    expect(url).not.toContain('lo=5');
    expect(url).not.toContain('lom=LASTNPERIODS');
  });
  it('should not display time period filter', async () => {
    const timeFilterPanel = await page.$('div[id="PANEL_PERIOD"]');
    expect(timeFilterPanel).toBe(null);
  });
  it('should find metadata info icon and click', async () => {
    const metadataHandle = await page.$(testidSelector('ref-md-info'));
    await metadataHandle.click();
    metadataHandle.dispose();
    if (process.env.E2E_ENV === 'debug') await page.waitFor(500);
  });
  it('should load and display the metadata', async () => {
    await page.waitForSelector(`${testidSelector('ref-md-panel')} > div > h5`);
    const metadataEntry = await page.$(
      `${testidSelector('ref-md-panel')} > div > h5`,
    );
    expect(metadataEntry).not.toBe(null);
  });
});
