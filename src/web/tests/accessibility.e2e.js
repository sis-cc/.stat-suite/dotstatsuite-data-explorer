import { changeVibe, testidSelector } from './utils';
describe('Accessibility vis page: filters, applied filters, toolbar', () => {
  let page;
  beforeAll(async done => {
    await changeVibe('nsi')('crs1');
    page = await global.__BROWSER__.newPage();
    await page.setViewport({ width: 1200, height: 800 });
    const url = global._E2E_.makeUrl(
      `/vis?tenant=oecd:de&lc=en&df[ds]=hybrid&df[id]=DSD_CRS%40DF_CRS1&df[ag]=OECD.DCD&df[vs]=1.0&av=true&pd=2019%2C2020&&dq=AUS........A`,
    );
    await page.goto(url);
    done();
  });
  it('load the viz and open RECIPIENT filter (keyboard)', async () => {
    await page.waitForSelector('div[id="RECIPIENT"]');
    await page.focus('div[aria-label="Recipient"]');
    await page.keyboard.press('Enter');
    const ariaExpanded = await page.$eval('#RECIPIENT', el => el.ariaExpanded);
    expect(ariaExpanded).toEqual('true');
  });
  it('type "albania" in the spotlight', async () => {
    await page.waitFor(500);
    const spotlight = await page.$(
      `div[id="RECIPIENT"] > div > ${testidSelector('spotlight')}`,
    );
    await spotlight.$(testidSelector('spotlight_input'));
    await spotlight.click();
    await page.keyboard.type('albania');
  });
  it('select albania with keyboard', async () => {
    await page.focus('div[aria-label="Albania"]');
    await page.keyboard.press('Enter');
    await page.waitForSelector(
      `${testidSelector('value_DPGC.E.ALB')}[aria-checked="true"]`,
    );
    await page.waitForSelector(
      `${testidSelector('deleteChip-test-id')}[aria-label="Albania"]`,
    );
  });
  it('CEEC value not disabled anymore', async () => {
    const isValueDisabled = await page.$eval(
      testidSelector('value_CEEC'),
      node => node.ariaDisabled,
    );
    const ariaLabel = await page.$eval(
      testidSelector('value_CEEC'),
      node => node.ariaLabel,
    );
    expect(isValueDisabled).toEqual('false');
    expect(ariaLabel).toEqual('No data with current filters for:  CEEC');
  });
  it('deselect Albania', async () => {
    await page.$(testidSelector('value_DPGC.E.ALB'));
    await page.keyboard.press('Enter');
    const ariaChecked = await page.$eval(
      testidSelector('value_DPGC.E.ALB'),
      node => node.ariaChecked,
    );
    expect(ariaChecked).toEqual('false');
    await page.waitFor(500);
  });
  it('toolbar should contain two lists ', async () => {
    const toolbar = await page.$(testidSelector('detoolbar'));
    const lists = await toolbar.$$eval(
      `${testidSelector('detoolbar')} > div[role="list"]`,
      n => n.length,
    );
    expect(lists).toEqual(2);
  });
  it('applied filters should be accessible', async () => {
    const usedFilters = await page.$(testidSelector('usedFilters-vis-test-id'));
    await page.focus('span[aria-label="delete Australia"]');
    await page.keyboard.press('Enter');
    const usedFiltersLabels = await usedFilters.$$eval(
      testidSelector('deleteChip-test-id'),
      nodes => nodes.map(n => n.ariaLabel),
    );
    expect(usedFiltersLabels).not.toContain('Australia');
  });
});
