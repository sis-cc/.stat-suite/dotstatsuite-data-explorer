import axios from 'axios';
import * as R from 'ramda';

export const testidSelector = testid => `[data-testid="${testid}"]`;
export const changeVibe = srv => vibe =>
  axios.get(global._E2E_.makeUrl(`/_vibes_/${vibe}`, srv));

export const itShouldFindNbResults = async ({ page, nb }) => {
  await page.waitForSelector(testidSelector('search_results'));
  const nbOfResults = await page.$$eval(
    `${testidSelector('search_results')} > div`,
    e => e.length,
  );
  expect(nbOfResults).toEqual(nb);
};

export const itShouldFindNbCells = async ({ page, nb }) => {
  await page.waitForSelector(testidSelector('vis-table'));
  const nbOfResults = await page.$$eval(
    `${testidSelector('vis-table')} td`,
    e => e.length,
  );
  expect(nbOfResults).toEqual(nb);
};

export const dimHeaderSelector = (dimension, value) =>
  `[headers*="${dimension}=${value}"]`;

export const dragAndDrop = async (
  page,
  originSelector,
  destinationSelector,
) => {
  const origin = await page.waitForSelector(originSelector);
  const destination = await page.waitForSelector(destinationSelector);

  const ocp = await origin.clickablePoint();
  const dcp = await destination.clickablePoint();

  // eslint-disable-next-line no-console
  console.log(`Dragging from ${ocp.x}, ${ocp.y}`);
  await page.mouse.move(ocp.x, ocp.y);
  await page.waitForTimeout(100);
  await page.mouse.down();
  await page.waitForTimeout(50);

  // eslint-disable-next-line no-console
  console.log(`Dropping at   ${dcp.x}, ${dcp.y}`);
  await page.mouse.move(dcp.x, dcp.y, { steps: 10 });
  await page.waitForTimeout(250);
  await page.mouse.up();
  await page.waitForTimeout(50);
};

export const getTableLayout = async page => {
  const table = await page.$(testidSelector('vis-table'));
  const rows = await table.$$eval(testidSelector('row-dim'), nodes =>
    nodes.map(n => n.ariaLabel),
  );
  const sections = await table.$$eval(testidSelector('section-dim'), nodes =>
    nodes.map(n => n.ariaLabel),
  );
  const header = await table.$$eval(testidSelector('header-dim'), nodes =>
    nodes.map(n => n.ariaLabel),
  );
  return { header, rows, sections: R.uniq(sections) };
};

export const getCustomizeLayout = async page => {
  const dropColumns = await page.$(testidSelector('droppable-header'));
  const dropSections = await page.$(testidSelector('droppable-sections'));
  const dropRows = await page.$(testidSelector('droppable-rows'));

  const header = await dropColumns.$$eval('[data-testid*="draggable"]', nodes =>
    nodes.map(n => n.ariaLabel),
  );
  const sections = await dropSections.$$eval(
    '[data-testid*="draggable"]',
    nodes => nodes.map(n => n.ariaLabel),
  );
  const rows = await dropRows.$$eval('[data-testid*="draggable"]', nodes =>
    nodes.map(n => n.ariaLabel),
  );

  return { header, sections, rows };
};

export const waitForTransition = async (page, selector) =>
  await page.$eval(selector, el => async done => {
    const onEnd = () => {
      el.removeEventListener('transitionend', onEnd);
      done();
    };
    el.addEventListener('transitionend', onEnd);
  });
