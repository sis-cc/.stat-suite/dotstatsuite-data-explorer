import { changeVibe, testidSelector } from './utils';
import { split, includes, endsWith } from 'ramda';

describe('sdmx data: range header check without warning limitation disclaimer', () => {
  let page;
  beforeAll(async done => {
    await changeVibe('config')('main');
    await changeVibe('nsi')('sna');
    page = await global.__BROWSER__.newPage();
    await page.setViewport({ width: 1200, height: 800 });
    const url = global._E2E_.makeUrl(
      `/vis?tenant=oecd:de&lc=en&df[ds]=hybrid&df[id]=SNA_TABLE1&df[ag]=OECD&df[vs]=1.0&av=true&pd=2019%2C2020`,
    );
    await page.goto(url);
    done();
  });
  it('load the viz page and wait for the table to be displayed', async done => {
    await page.waitForSelector(testidSelector('vis-table'));
    done();
  });
  it('should not find the warning limitation disclaimer in the header', async done => {
    const disclaimer = await page.$(testidSelector('data-header-disclaimer'));
    expect(disclaimer).toBe(null);
    done();
  });
  it('open data downloads', async () => {
    const button = await page.$(testidSelector('downloads-button'));
    await button.click();
    await page.setRequestInterception(true);
    page.on('request', req => {
      setTimeout(() => req.continue(), 500);
    });
  });
  it('should have not disabled filtered data link', async () => {
    await page.waitForSelector(testidSelector('downloads-menu'));
    const optionIsDisabled = await page.$eval(
      'a[id="csv.selection"]',
      n => n.ariaDisabled,
    );
    expect(optionIsDisabled).toEqual(null);
  });
  it('click should trigger filtered data request', async () => {
    await page.waitForSelector(testidSelector('downloads-menu'));
    await page.evaluate(() => {
      document.querySelector('a[id="csv.selection"]').click();
    });
    await page.waitForResponse(res => {
      const url = res.url();
      const [query, params] = split('?', url);
      return (
        includes('/data/OECD,SNA_TABLE1,1.0/', query) &&
        includes('startPeriod=2019', params) &&
        includes('endPeriod=2020', params) &&
        includes('format=csvfilewithlabels', params)
      );
    });
  });
  it('should display dl info message', async () => {
    const info = await page.$(testidSelector('csv-dl-start'));
    expect(info).not.toEqual(null);
  });
  it('close info message', async () => {
    const info = await page.$(testidSelector('csv-dl-start'));
    const button = await info.$('button');
    await button.click();
  });
  it('reopen data downloads', async () => {
    const button = await page.$(testidSelector('downloads-button'));
    await button.click();
  });
  it('should have not disabled full data link', async () => {
    await page.waitForSelector(testidSelector('downloads-menu'));
    const menu = await page.$(testidSelector('downloads-menu'));
    const optionIsDisabled = await menu.$eval(
      'a[id="csv.all"]',
      n => n.ariaDisabled,
    );
    expect(optionIsDisabled).toEqual(null);
  });
  it('click should trigger full data request', async () => {
    await page.waitForSelector(testidSelector('downloads-menu'));
    await page.evaluate(() => {
      document.querySelector('a[id="csv.all"]').click();
    });
    await page.waitForResponse(res => {
      const url = res.url();
      const [query, params] = split('?', url);
      return (
        includes('/data/OECD,SNA_TABLE1,1.0/', query) &&
        !includes('startPeriod=2019', params) &&
        !includes('endPeriod=2020', params) &&
        includes('format=csvfilewithlabels', params)
      );
    });
  });
  it('should display dl info message', async () => {
    const info = await page.$(testidSelector('csv-dl-start'));
    expect(info).not.toEqual(null);
  });
  it('close info message', async () => {
    const info = await page.$(testidSelector('csv-dl-start'));
    const button = await info.$('button');
    await button.click();
  });
  it('reopen data downloads', async () => {
    const button = await page.$(testidSelector('downloads-button'));
    await button.click();
  });
  it('should have not disabled full metadata link', async () => {
    await page.waitForSelector(testidSelector('downloads-menu'));
    const menu = await page.$(testidSelector('downloads-menu'));
    const optionIsDisabled = await menu.$eval(
      'a[id="metadata.all"]',
      n => n.ariaDisabled,
    );
    expect(optionIsDisabled).toEqual(null);
  });
  it('click should trigger send a full metadata request', async () => {
    await page.waitForSelector(testidSelector('downloads-menu'));
    await page.evaluate(() => {
      document.querySelector('a[id="metadata.all"]').click();
    });
    await page.waitForResponse(res => {
      const url = res.url();
      const [query, params] = split('?', url);
      return (
        endsWith('/data/dataflow/OECD/SNA_TABLE1/1.0/', query) &&
        includes('format=csvfilewithlabels', params)
      );
    });
  });
  it('should display dl info message', async () => {
    const info = await page.$(testidSelector('csv-dl-start'));
    expect(info).not.toEqual(null);
  });
  it('deactivate csv links then reload', async () => {
    await changeVibe('config')('noCsvLink');
    await page.reload();
  });
  it('load the viz page and wait for the table to be displayed', async done => {
    await page.waitForSelector(testidSelector('vis-table'));
    done();
  });
  it('deactivate dl requests', async () => {
    await changeVibe('nsi')('sna:dl-error');
  });
  it('open data downloads', async () => {
    const button = await page.$(testidSelector('downloads-button'));
    await button.click();
  });
  it('should have not disabled filtered data link', async () => {
    await page.waitForSelector(testidSelector('downloads-menu'));
    const menu = await page.$(testidSelector('downloads-menu'));
    const optionIsDisabled = await menu.$eval(
      testidSelector('data.download.csv.selection-button'),
      n => n.ariaDisabled,
    );
    expect(optionIsDisabled).toEqual(null);
  });
  it('click should send a filtered data request', async () => {
    await page.waitForSelector(testidSelector('downloads-menu'));
    await page.evaluate(() => {
      document
        .querySelector('[data-testid="data.download.csv.selection-button"]')
        .click();
    });
    await page.waitForResponse(res => {
      const url = res.url();
      const [query, params] = split('?', url);
      return (
        includes('/data/OECD,SNA_TABLE1,1.0/', query) &&
        includes('startPeriod=2019', params) &&
        includes('endPeriod=2020', params) &&
        includes('format=csvfilewithlabels', params)
      );
    });
  });
  it('should display dl error message', async () => {
    await page.waitForSelector(testidSelector('csv-dl-error'));
    const info = await page.$(testidSelector('csv-dl-error'));
    expect(info).not.toEqual(null);
  });
  it('close info message', async () => {
    const info = await page.$(testidSelector('csv-dl-error'));
    const button = await info.$('button');
    await button.click();
  });
  it('reopen data downloads', async () => {
    const button = await page.$(testidSelector('downloads-button'));
    await button.click();
  });
  it('should have not disabled full data link', async () => {
    await page.waitForSelector(testidSelector('downloads-menu'));
    const menu = await page.$(testidSelector('downloads-menu'));
    const optionIsDisabled = await menu.$eval(
      testidSelector('data.download.csv.all-button'),
      n => n.ariaDisabled,
    );
    expect(optionIsDisabled).toEqual(null);
  });
  it('click should trigger full data request', async () => {
    await page.waitForSelector(testidSelector('downloads-menu'));
    await page.evaluate(() => {
      document
        .querySelector('[data-testid="data.download.csv.all-button"]')
        .click();
    });
    await page.waitForResponse(res => {
      const url = res.url();
      const [query, params] = split('?', url);
      return (
        includes('/data/OECD,SNA_TABLE1,1.0/', query) &&
        !includes('startPeriod=2019', params) &&
        !includes('endPeriod=2020', params) &&
        includes('format=csvfilewithlabels', params)
      );
    });
  });
  it('should display dl error message', async () => {
    await page.waitForSelector(testidSelector('csv-dl-error'));
    const info = await page.$(testidSelector('csv-dl-error'));
    expect(info).not.toEqual(null);
  });
  it('close info message', async () => {
    const info = await page.$(testidSelector('csv-dl-error'));
    const button = await info.$('button');
    await button.click();
  });
  it('reopen data downloads', async () => {
    const button = await page.$(testidSelector('downloads-button'));
    await button.click();
  });
  it('should have not disabled full metadata link', async () => {
    await page.waitForSelector(testidSelector('downloads-menu'));
    const menu = await page.$(testidSelector('downloads-menu'));
    const optionIsDisabled = await menu.$eval(
      testidSelector('metadata.download.csv.all-button'),
      n => n.ariaDisabled,
    );
    expect(optionIsDisabled).toEqual(null);
  });
  it('click should trigger send a full metadata request', async () => {
    await page.waitForSelector(testidSelector('downloads-menu'));
    await page.evaluate(() => {
      document
        .querySelector('[data-testid="metadata.download.csv.all-button"]')
        .click();
    });
    await page.waitForResponse(res => {
      const url = res.url();
      const [query, params] = split('?', url);
      return (
        endsWith('/data/dataflow/OECD/SNA_TABLE1/1.0/', query) &&
        includes('format=csvfilewithlabels', params)
      );
    });
  });
  it('should display dl error message', async () => {
    await page.waitForSelector(testidSelector('csv-dl-error'));
    const info = await page.$(testidSelector('csv-dl-error'));
    expect(info).not.toEqual(null);
  });
  it('click on metadata button and open metadata side panel', async () => {
    await page.evaluate(() => {
      document.querySelector('[data-testid="ref-md-info"]').click();
    });
    await page.waitForSelector(testidSelector('ref-md-panel'));
  });
  it('click on metadata selection dl button should send filtered metadata csv request', async () => {
    await page.waitForSelector(
      testidSelector('metadata.download.csv.selection-button'),
    );
    await page.evaluate(() => {
      document
        .querySelector('[data-testid="metadata.download.csv.selection-button"]')
        .click();
    });
    await page.waitForResponse(res => {
      const url = res.url();
      const [query, params] = split('?', url);
      return (
        endsWith('/data/dataflow/OECD/SNA_TABLE1/1.0/*.B1G_P119.*', query) &&
        includes('format=csvfilewithlabels', params)
      );
    });
  });
  it('should display dl error message', async () => {
    await page.waitForSelector(
      testidSelector('metadata.download.csv.selection.error'),
    );
    const info = await page.$(
      testidSelector('metadata.download.csv.selection.error'),
    );
    expect(info).not.toEqual(null);
  });
});

describe('sdmx data: too long requests', () => {
  let page;
  beforeAll(async done => {
    await changeVibe('config')('main');
    await changeVibe('nsi')('sna:too-long');
    page = await global.__BROWSER__.newPage();
    await page.setViewport({ width: 1200, height: 800 });
    const url = global._E2E_.makeUrl(
      `/vis?tenant=oecd:de&lc=en&df[ds]=hybrid&df[id]=SNA_TABLE1&df[ag]=OECD&df[vs]=1.0&av=true&pd=2019%2C2020`,
    );
    await page.goto(url);
    done();
  });
  it('load the viz page and wait for the table to be displayed', async done => {
    await page.waitForSelector(testidSelector('vis-table'));
    done();
  });
});

describe('sdmx data: range header check with warning limitation disclaimer', () => {
  let page;
  beforeAll(async done => {
    await changeVibe('config')('range');
    await changeVibe('nsi')('sna:content-range');
    page = await global.__BROWSER__.newPage();
    await page.setViewport({ width: 1200, height: 800 });
    const url = global._E2E_.makeUrl(
      `/vis?tenant=oecd:de&lc=en&df[ds]=hybrid&df[id]=SNA_TABLE1&df[ag]=OECD&df[vs]=1.0&av=true&pd=2019%2C2020`,
    );
    await page.goto(url);
    done();
  });
  it('load the viz page and wait for the table to be displayed', async done => {
    await page.waitForSelector(testidSelector('vis-table'));
    done();
  });
  it('should find the warning limitation disclaimer in the header', async done => {
    const disclaimer = await page.$(testidSelector('data-header-disclaimer'));
    expect(disclaimer).not.toBe(null);
    done();
  });
});
