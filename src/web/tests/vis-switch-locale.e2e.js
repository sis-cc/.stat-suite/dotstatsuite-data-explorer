import { changeVibe, testidSelector, waitForTransition } from './utils';
import * as R from 'ramda';

describe('vis page: switch locale', () => {
  let page;
  beforeAll(async () => {
    await changeVibe('config')('main');
    await changeVibe('nsi')('qna_comb');
    page = await global.__BROWSER__.newPage();
    await page.setViewport({ width: 1200, height: 800 });
    const url = global._E2E_.makeUrl(
      `/vis?tenant=oecd:de&df[ds]=hybrid&df[id]=DSD_QNA_COMB@DF_QNA_COMB&df[ag]=OECD.SDD.NAD&df[vs]=1.0`,
    );
    await page.setRequestInterception(true);
    page.on('request', req => {
      setTimeout(() => req.continue(), 500);
    });
    page.goto(url);
  });
  it('load the viz page and wait for the correct default data request in english then display table', async () => {
    await page.waitForResponse(res => {
      const url = res.url();
      const request = res.request();
      const headers = request.headers();
      return (
        R.includes(
          '/data/OECD.SDD.NAD,DSD_QNA_COMB@DF_QNA_COMB,1.0/Q.CZE............',
          url,
        ) && R.propEq('en', 'accept-language', headers)
      );
    });
    await page.waitForSelector(testidSelector('vis-table'));
  });
  it('open MEASURE filter', async () => {
    await page.evaluate(() => {
      document
        .querySelector('[data-testid="filter_panel"][id="MEASURE"]')
        .click();
    });
    await waitForTransition(
      page,
      `${testidSelector('filter_panel')}[id="MEASURE"] > div`,
    );
  });
  it('select Gross domestic products at market prices', async () => {
    const option = await page.$(testidSelector('value_B1GQ'));
    await option.click();
  });
  it('should send new data request', async () => {
    await page.waitForResponse(res => {
      const url = res.url();
      const request = res.request();
      const headers = request.headers();
      return (
        R.includes(
          '/data/OECD.SDD.NAD,DSD_QNA_COMB@DF_QNA_COMB,1.0/Q.CZE....B1GQ........',
          url,
        ) && R.propEq('en', 'accept-language', headers)
      );
    });
    await page.waitForSelector(testidSelector('vis-table'));
  });
  it('switch to french', async () => {
    const localesButton = await page.$('div[id="languages"]');
    await localesButton.click();
    const frenchOption = await page.$('li[id="fr"]');
    await frenchOption.click();
  });
  it('previous dataquery kept for data request in french', async () => {
    await page.waitForResponse(res => {
      const url = res.url();
      const request = res.request();
      const headers = request.headers();
      return (
        R.includes(
          '/data/OECD.SDD.NAD,DSD_QNA_COMB@DF_QNA_COMB,1.0/Q.CZE....B1GQ........',
          url,
        ) && R.propEq('fr', 'accept-language', headers)
      );
    });
    await page.waitForSelector(testidSelector('vis-table'));
  });
  it('switch back to english', async () => {
    const localesButton = await page.$('div[id="languages"]');
    await localesButton.click();
    const englishOption = await page.$('li[id="en"]');
    await englishOption.click();
  });
  it('data request kept unchanged and back in english', async () => {
    await page.waitForResponse(res => {
      const url = res.url();
      const request = res.request();
      const headers = request.headers();
      return (
        R.includes(
          '/data/OECD.SDD.NAD,DSD_QNA_COMB@DF_QNA_COMB,1.0/Q.CZE....B1GQ........',
          url,
        ) && R.propEq('en', 'accept-language', headers)
      );
    });
    await page.waitForSelector(testidSelector('vis-table'));
  });
  it('reload default url', async () => {
    const nextUrl = global._E2E_.makeUrl(
      `/vis?tenant=oecd:de&df[ds]=hybrid&df[id]=DSD_QNA_COMB@DF_QNA_COMB&df[ag]=OECD.SDD.NAD&df[vs]=1.0`,
    );
    page.goto(nextUrl);
  });
  it('load the viz page and wait for the correct default data request in english then display table', async () => {
    await page.waitForResponse(res => {
      const url = res.url();
      const request = res.request();
      const headers = request.headers();
      return (
        R.includes(
          '/data/OECD.SDD.NAD,DSD_QNA_COMB@DF_QNA_COMB,1.0/Q.CZE............',
          url,
        ) && R.propEq('en', 'accept-language', headers)
      );
    });
    await page.waitForSelector(testidSelector('vis-table'));
  });
  it('switch to french', async () => {
    const localesButton = await page.$('div[id="languages"]');
    await localesButton.click();
    const frenchOption = await page.$('li[id="fr"]');
    await frenchOption.click();
  });
  it('should send new data request in french then table', async () => {
    await page.waitForResponse(res => {
      const url = res.url();
      const request = res.request();
      const headers = request.headers();
      return (
        R.includes(
          '/data/OECD.SDD.NAD,DSD_QNA_COMB@DF_QNA_COMB,1.0/Q.CZE............',
          url,
        ) && R.propEq('fr', 'accept-language', headers)
      );
    });
    await page.waitForSelector(testidSelector('vis-table'));
  });
  it('open MEASURE filter', async () => {
    await page.evaluate(() => {
      document
        .querySelector('[data-testid="filter_panel"][id="MEASURE"]')
        .click();
    });
    await waitForTransition(
      page,
      `${testidSelector('filter_panel')}[id="MEASURE"] > div`,
    );
  });
  it('select Gross domestic products at market prices', async () => {
    const option = await page.$(testidSelector('value_B1GQ'));
    await option.click();
  });
  it('should send new data request with updated dataquery in french then table', async () => {
    await page.waitForResponse(res => {
      const url = res.url();
      const request = res.request();
      const headers = request.headers();
      return (
        R.includes(
          '/data/OECD.SDD.NAD,DSD_QNA_COMB@DF_QNA_COMB,1.0/Q.CZE....B1GQ........',
          url,
        ) && R.propEq('fr', 'accept-language', headers)
      );
    });
    await page.waitForSelector(testidSelector('vis-table'));
  });
  it('switch back to english', async () => {
    const localesButton = await page.$('div[id="languages"]');
    await localesButton.click();
    const englishOption = await page.$('li[id="en"]');
    await englishOption.click();
  });
  it('should send previous data request in english then table', async () => {
    await page.waitForResponse(res => {
      const url = res.url();
      const request = res.request();
      const headers = request.headers();
      return (
        R.includes(
          '/data/OECD.SDD.NAD,DSD_QNA_COMB@DF_QNA_COMB,1.0/Q.CZE....B1GQ........',
          url,
        ) && R.propEq('en', 'accept-language', headers)
      );
    });
    await page.waitForSelector(testidSelector('vis-table'));
  });
  it('dataquery shoud have been preserved in url', async () => {
    const url = await page.url();
    expect(url).toContain('dq=Q.CZE....B1GQ........');
  });
  it('reload default url with fr default locale in settings', async () => {
    await changeVibe('config')('main:fr');
    const nextUrl = global._E2E_.makeUrl(
      `/vis?tenant=oecd:de&df[ds]=hybrid&df[id]=DSD_QNA_COMB@DF_QNA_COMB&df[ag]=OECD.SDD.NAD&df[vs]=1.0`,
    );
    page.goto(nextUrl);
  });
  it('default data request should now be in french then table', async () => {
    await page.waitForResponse(res => {
      const url = res.url();
      const request = res.request();
      const headers = request.headers();
      return (
        R.includes(
          '/data/OECD.SDD.NAD,DSD_QNA_COMB@DF_QNA_COMB,1.0/Q.CZE............',
          url,
        ) && R.propEq('fr', 'accept-language', headers)
      );
    });
    await page.waitForSelector(testidSelector('vis-table'));
  });
});
