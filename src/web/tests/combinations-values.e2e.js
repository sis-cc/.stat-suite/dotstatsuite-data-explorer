import { changeVibe, testidSelector } from './utils';
import * as R from 'ramda';

describe('vis: combinations of values', () => {
  let page;

  const urlFactory = (display = 'nm') =>
    `/vis?tenant=oecd:de&pg=0&fs[0]=datasourceId%2Cds%3Aqa%3Astable&fc=datasourceId&bp=true&snb=34&vw=tb&df[ds]=hybrid&df[id]=DF_EO_N&df[ag]=OECD.ECO&df[vs]=1.2&pd=%2C&dq=...PT_CONT%2BPT_GDP.A&ly[rw]=MEASURE%2CUNIT_MEASURE%2CCOMBINATION&to[TIME_PERIOD]=false&lo=5&lom=LASTNPERIODS&lb=${display}`;
  // important to 'sync' header and cell
  const invariantSelector = 'tr th:nth-child(3)';
  const testData = [
    {
      label: 'name',
      value: 'nm',
      expectedHeaderText: 'my combination',
      expectedCellText: 'France, Nominal value, 2018',
    },
    {
      label: 'code',
      value: 'id',
      expectedHeaderText: 'COMBINATION',
      expectedCellText: 'FR, N, 2018',
    },
    {
      label: 'both',
      value: 'bt',
      expectedHeaderText: '(COMBINATION) my combination',
      expectedCellText: '(FR, N, 2018) France, Nominal value, 2018',
    },
  ];

  beforeAll(async done => {
    await changeVibe('config')('combinations');
    await changeVibe('nsi')('oecd');
    page = await global.__BROWSER__.newPage();
    await page.setViewport({ width: 1200, height: 800 });
    done();
  });

  R.forEach(({ label, value, expectedHeaderText, expectedCellText }) => {
    it(`should with display ${label} (${value}) render '${expectedHeaderText}' and '${expectedCellText}'`, async done => {
      const url = global._E2E_.makeUrl(urlFactory(value));
      await page.goto(url);

      const headerHandle = await page.waitForSelector(
        `${testidSelector('vis-table')} thead ${invariantSelector}`,
      );
      const headerText = await page.evaluate(
        el => el.textContent,
        headerHandle,
      );
      expect(headerText).toEqual(expectedHeaderText);

      const cellHandle = await page.waitForSelector(
        `${testidSelector('vis-table')} tbody ${invariantSelector}`,
      );
      const cellText = await page.evaluate(el => el.textContent, cellHandle);
      expect(cellText).toEqual(expectedCellText);

      done();
    });
  }, testData);
});
