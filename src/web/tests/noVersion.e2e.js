import { changeVibe, testidSelector } from './utils';

describe('vis page: no version in url', () => {
  let page;
  beforeAll(async done => {
    await changeVibe('config')('main');
    await changeVibe('nsi')('sna');
    page = await global.__BROWSER__.newPage();
    await page.setViewport({ width: 1200, height: 800 });
    const url = global._E2E_.makeUrl(
      `/vis?tenant=oecd:de&lc=en&df[ds]=hybrid&df[id]=SNA_TABLE1&df[ag]=OECD&av=true&vw=tb`,
    );
    await page.goto(url);
    done();
  });
  it('load the viz page and wait for the table to be displayed', async () => {
    await page.waitForSelector(testidSelector('vis-table'));
    if (process.env.E2E_ENV === 'debug') await page.waitFor(250);
  });
  it('should have filter LOCATION name from latest structure', async () => {
    const locationFilterLabel = await page.$eval(
      `${testidSelector('filter_panel')}[id="LOCATION"]`,
      node => node.ariaLabel,
    );
    expect(locationFilterLabel).toEqual('Countries(Latest)');
  });
  it('should have latest Dataflow name in header title', async () => {
    const headerTitleLabel = await page.$eval(
      testidSelector('data-header-title'),
      node => node.ariaLabel,
    );
    expect(headerTitleLabel).toEqual('Latest Dataflow');
  });
  it('should have C value disabled in MEASURE filter (latest ACC)', async () => {
    const measureFilterValues = await page.$('[id="MEASURE"][role="region"]');
    const cValueIsDisabled = await measureFilterValues.$eval(
      testidSelector('value_C'),
      node => node.ariaDisabled,
    );
    expect(cValueIsDisabled).toEqual('true');
  });
  it('should click on the first annotation and open the metadata panel', async () => {
    const annotationHandle = await page.$(testidSelector('ref-md-info'));
    await annotationHandle.click();
    annotationHandle.dispose();
    if (process.env.E2E_ENV === 'debug') await page.waitFor(500);
  });
  it('should have latest metadata content in panel', async () => {
    await page.waitForSelector(`${testidSelector('ref-md-panel')} br`);
    const panel = await page.$(testidSelector('ref-md-panel'));
    const panelSpanLabels = await panel.$$eval('span', nodes =>
      nodes.map(n => n.textContent),
    );
    expect(panelSpanLabels).toContain('Boomerang');
  });
});
