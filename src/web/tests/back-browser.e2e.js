import {
  changeVibe,
  itShouldFindNbResults,
  testidSelector,
  getTableLayout,
} from './utils';

describe('back browser flow', () => {
  let page;
  beforeAll(async done => {
    await changeVibe('config')('main');
    await changeVibe('sfs')('oecd');
    await changeVibe('nsi')('sna');
    page = await global.__BROWSER__.newPage();
    await page.setViewport({ width: 1200, height: 800 });
    const url = global._E2E_.makeUrl(`?tm=external&pg=0&snb=1`);
    await page.goto(url);
    done();
  });
  it('should load the search results page and find 2 results', async () => {
    itShouldFindNbResults({ page, nb: 2 });
  });
  it('should find regular dataflow and open it', async () => {
    await page.waitForSelector(testidSelector('search_results'));
    const externalDFLink = await page.$(
      `${testidSelector('ds-demo-release:SNA_TABLE1')} a`,
    );
    await externalDFLink.click();
  });
  it('should display table', async () => {
    await page.waitForSelector(testidSelector('vis-table'));
  });
  it('go back to search by browser', async () => {
    await page.goBack();
  });
  it('open external dataflow', async () => {
    await page.waitForSelector(testidSelector('search_results'));
    await changeVibe('nsi')('qna_comb');
    const externalDFLink = await page.$(
      `${testidSelector('ds-demo-release:DSD_QNA_COMB@DF_QNA_COMB_EXT')} a`,
    );
    await externalDFLink.click();
  });
  it('should display correct header title', async () => {
    await page.waitForSelector(testidSelector('data-header-title'));
    const headerTitle = await page.$eval(
      testidSelector('data-header-title'),
      n => n.textContent,
    );
    expect(headerTitle).toEqual(
      'Quarterly GDP - expenditure approach (combined measure test)',
    );
  });
  it('should diplay correct filters', async () => {
    const filtersLables = await page.$$eval(
      testidSelector('filter_panel'),
      nodes => nodes.map(n => n.ariaLabel),
    );
    expect(filtersLables).toEqual([
      'Reporting institutional sector',
      'Measure',
    ]);
  });
  it('should have correct layout', async () => {
    expect(await getTableLayout(page)).toEqual({
      sections: [],
      header: ['TIME_PERIOD'],
      rows: ['COMBINED_MEASURE', 'COMBINED_UNIT_MEASURE'],
    });
  });
  it('back to search by browser', async () => {
    await page.goBack();
  });
  it('reopen first dataflow', async () => {
    await page.waitForSelector(testidSelector('search_results'));
    await changeVibe('nsi')('sna');
    const externalDFLink = await page.$(
      `${testidSelector('ds-demo-release:SNA_TABLE1')} a`,
    );
    await externalDFLink.click();
  });
  it('should display correct header title', async () => {
    await page.waitForSelector(testidSelector('data-header-title'));
    const headerTitle = await page.$eval(
      testidSelector('data-header-title'),
      n => n.textContent,
    );
    expect(headerTitle).toEqual('1. Gross domestic product (GDP)');
  });
  it('should diplay correct filters', async () => {
    const filtersLables = await page.$$eval(
      testidSelector('filter_panel'),
      nodes => nodes.map(n => n.ariaLabel),
    );
    expect(filtersLables).toEqual(['Country', 'Transaction', 'Measure']);
  });
  it('should have correct layout', async () => {
    expect(await getTableLayout(page)).toEqual({
      sections: ['MEASURE'],
      header: ['TIME_PERIOD'],
      rows: ['LOCATION'],
    });
  });
});
