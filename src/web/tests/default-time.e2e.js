import { changeVibe, testidSelector } from './utils';
import * as R from 'ramda';

describe('default time periods selections', () => {
  let page;
  beforeAll(async done => {
    await changeVibe('config')('main');
    await changeVibe('nsi')('ddown');
    page = await global.__BROWSER__.newPage();
    await page.setViewport({ width: 1200, height: 800 });
    const url = global._E2E_.makeUrl(
      `/vis?tenant=oecd:de&lc=en&df[ds]=hybrid&df[id]=DSD_DEBT_TRANS_DDOWN@DF_DDOWN&df[ag]=OECD.DAF&df[vs]=1.0&vw=tb`,
    );
    await page.goto(url);
    done();
  });
  it('load the viz page and wait for the table to be displayed', async () => {
    await page.waitForSelector(testidSelector('vis-table'));
    if (process.env.E2E_ENV === 'debug') await page.waitFor(250);
  });
  it('should have default start period 2020 selection from structure in url', async () => {
    const url = await page.url();
    expect(url).toContain('pd=2020%2C');
  });
  it('load url with empty time selection', async () => {
    const nextUrl = global._E2E_.makeUrl(
      `/vis?tenant=oecd:de&lc=en&df[ds]=hybrid&df[id]=DSD_DEBT_TRANS_DDOWN@DF_DDOWN&df[ag]=OECD.DAF&df[vs]=1.0&vw=tb&pd=%2C`,
    );
    await page.goto(nextUrl);
  });
  it('load the viz page and wait for the table to be displayed', async () => {
    await page.waitForSelector(testidSelector('vis-table'));
    if (process.env.E2E_ENV === 'debug') await page.waitFor(250);
  });
  it('time selection should be kept empty', async () => {
    const url = await page.url();
    expect(url).toContain('pd=%2C');
  });
  it('load url with lastnperiods', async () => {
    const nextUrl = global._E2E_.makeUrl(
      `/vis?tenant=oecd:de&lc=en&df[ds]=hybrid&df[id]=DSD_DEBT_TRANS_DDOWN@DF_DDOWN&df[ag]=OECD.DAF&df[vs]=1.0&vw=tb&lo=5&lom=LASTNPERIODS`,
    );
    await page.goto(nextUrl);
  });
  it('load the viz page and wait for the table to be displayed', async () => {
    await page.waitForSelector(testidSelector('vis-table'));
    if (process.env.E2E_ENV === 'debug') await page.waitFor(250);
  });
  it('no time selection should be in url and lastNPeriods kept', async () => {
    const url = await page.url();
    expect(url).not.toContain('pd');
    expect(url).toContain('lo=5&lom=LASTNPERIODS');
  });
  it('load url with lastnobservations and without time selection', async () => {
    const nextUrl = global._E2E_.makeUrl(
      `/vis?tenant=oecd:de&lc=en&df[ds]=hybrid&df[id]=DSD_DEBT_TRANS_DDOWN@DF_DDOWN&df[ag]=OECD.DAF&df[vs]=1.0&vw=tb&lo=1&lom=LASTNOBSERVATIONS`,
    );
    await page.goto(nextUrl);
  });
  it('load the viz page and wait for the table to be displayed', async () => {
    await page.waitForSelector(testidSelector('vis-table'));
    if (process.env.E2E_ENV === 'debug') await page.waitFor(250);
  });
  it('time selection should be added in url and lastNObservations kept', async () => {
    const url = await page.url();
    expect(url).toContain('pd');
    expect(url).toContain('lo=1&lom=LASTNOBSERVATIONS');
  });
  it('load url with lastnobservations and custom time selection', async () => {
    const nextUrl = global._E2E_.makeUrl(
      `/vis?tenant=oecd:de&lc=en&df[ds]=hybrid&df[id]=DSD_DEBT_TRANS_DDOWN@DF_DDOWN&df[ag]=OECD.DAF&df[vs]=1.0&vw=tb&lo=1&lom=LASTNOBSERVATIONS&pd=2017%2C2020`,
    );
    await page.goto(nextUrl);
  });
  it('load the viz page and wait for the table to be displayed', async () => {
    await page.waitForSelector(testidSelector('vis-table'));
    if (process.env.E2E_ENV === 'debug') await page.waitFor(250);
  });
  it('time selection and lastNObservations should be unchanged', async () => {
    const url = await page.url();
    expect(url).toContain('pd=2017%2C2020');
    expect(url).toContain('lo=1&lom=LASTNOBSERVATIONS');
  });
  it('load url with lastnperiods and custom time selection', async () => {
    const nextUrl = global._E2E_.makeUrl(
      `/vis?tenant=oecd:de&lc=en&df[ds]=hybrid&df[id]=DSD_DEBT_TRANS_DDOWN@DF_DDOWN&df[ag]=OECD.DAF&df[vs]=1.0&vw=tb&lo=3&lom=LASTNPERIODS&pd=2017%2C2020`,
    );
    await page.goto(nextUrl);
  });
  it('load the viz page and wait for the table to be displayed', async () => {
    await page.waitForSelector(testidSelector('vis-table'));
    if (process.env.E2E_ENV === 'debug') await page.waitFor(250);
  });
  it('only time selection should be kept', async () => {
    const url = await page.url();
    expect(url).toContain('pd=2017%2C2020');
    expect(url).not.toContain('lo=');
    expect(url).not.toContain('lom=');
  });
  it('switch to no frequency version and last n periods set to 6', async () => {
    const nextUrl = global._E2E_.makeUrl(
      `/vis?tenant=oecd:de&lc=en&df[ds]=hybrid&df[id]=DSD_DEBT_TRANS_DDOWN@DF_DDOWN&df[ag]=OECD.DAF&df[vs]=2.0&vw=tb&lo=6&lom=LASTNPERIODS`,
    );
    await page.goto(nextUrl);
  });
  it('correct startPeriod query was made and table is successfully displayed', async () => {
    await page.waitForSelector(testidSelector('vis-table'));
    if (process.env.E2E_ENV === 'debug') await page.waitFor(250);
  });
  it('switch to dataflow without default time selection in annotations', async () => {
    await changeVibe('nsi')('sna');
    const nextUrl = global._E2E_.makeUrl(
      `/vis?tenant=oecd:de&lc=en&df[ds]=hybrid&df[id]=SNA_TABLE1&df[ag]=OECD&df[vs]=1.0&av=true&vw=tb`,
    );
    await page.goto(nextUrl);
  });
  it('load the viz page and wait for the table to be displayed', async () => {
    await page.waitForSelector(testidSelector('vis-table'));
    if (process.env.E2E_ENV === 'debug') await page.waitFor(250);
  });
  it('default last n periods from settings should be applied', async () => {
    const url = await page.url();
    expect(url).not.toContain('pd=');
    expect(url).toContain('lo=5');
    expect(url).toContain('lom=LASTNPERIODS');
  });
  it('switch to last n observations in filter', async () => {
    const filterExpand = await page.$(
      `${testidSelector('expansion_panel_panel')}[id="PANEL_PERIOD"]`,
    );
    await filterExpand.click();
    await page.waitForSelector('input[id="time series value(s)_1"]');
    // alternate way to click on element
    await page.evaluate(() => {
      document.querySelector('input[id="time series value(s)_1"]').click();
    });
    // this way we can wait to be sure new data request was performed before waiting and testing an updated table
    await page.waitForResponse(
      res =>
        R.includes('lastNObservations=5', res.url()) && res.status() === 200,
    );
  });
  it('display updated table', async () => {
    await page.waitForSelector(testidSelector('vis-table'));
    const tableTimePeriods = await page.$$eval(
      'th[headers="header_0"]',
      nodes => nodes.map(n => n.textContent),
    );
    expect(tableTimePeriods).toEqual(['2023', '2024']);
  });
  it('load url to last 2 observations without time selection', async () => {
    const nextUrl = global._E2E_.makeUrl(
      `/vis?tenant=oecd:de&lc=en&df[ds]=hybrid&df[id]=SNA_TABLE1&df[ag]=OECD&df[vs]=1.0&av=true&vw=tb&lo=2&lom=LASTNOBSERVATIONS`,
    );
    await page.goto(nextUrl);
  });
  it('load the viz page and wait for the table to be displayed', async () => {
    await page.waitForSelector(testidSelector('vis-table'));
    if (process.env.E2E_ENV === 'debug') await page.waitFor(250);
  });
  it('last 2 observations should be kept and empty time selection added ', async () => {
    const url = await page.url();
    expect(url).toContain('pd=%2C');
    expect(url).toContain('lo=2');
    expect(url).toContain('lom=LASTNOBSERVATIONS');
  });
});
