import lighthouse from 'lighthouse';
import { launch } from 'chrome-launcher';
import * as R from 'ramda';

const urls = [
  { label: 'home page', url: 'https://de-qa.siscc.org', threshold: 90 },
  {
    label: 'search results (without filters!)',
    url: 'https://de-qa.siscc.org/?tm=Test%20a11y%20dataflow&pg=0&snb=1',
    threshold: 80,
  },
  {
    label: 'viz page (overview)',
    url:
      'https://de-qa.siscc.org/vis?tm=Test%20a11y%20dataflow&pg=0&snb=1&vw=ov&df[ds]=ds%3Aqa%3Astable&df[id]=DF_TEST_ACCESSIBILITY&df[ag]=SISCC.A11Y&df[vs]=1.0&pd=2013%2C&dq=.A...&ly[rw]=MEASURE%2CUNIT_MEASURE&ly[cl]=TIME_PERIOD&ly[rs]=REF_AREA&to[TIME_PERIOD]=false',
    threshold: 75,
  },
  {
    label: 'viz page (table)',
    url:
      'https://de-qa.siscc.org/vis?tm=Test%20a11y%20dataflow&pg=0&snb=1&vw=tb&df[ds]=ds%3Aqa%3Astable&df[id]=DF_TEST_ACCESSIBILITY&df[ag]=SISCC.A11Y&df[vs]=1.0&pd=2013%2C&dq=.A...&ly[rw]=MEASURE%2CUNIT_MEASURE&ly[cl]=TIME_PERIOD&ly[rs]=REF_AREA&to[TIME_PERIOD]=false',
    threshold: 70,
  },
];

const a11yKey = 'accessibility';
const options = {
  logLevel: 'error',
  output: 'json',
  onlyCategories: [a11yKey],
};

const data = {};
afterAll(done => {
  // eslint-disable-next-line no-console
  console.table(data);
  done();
});

describe('lighhouse a11y tests', () => {
  R.forEach(async ({ label, url, threshold }) => {
    it(`${label} should be accessible (>= ${threshold})`, async () => {
      const chrome = await launch({
        chromeFlags: [
          '--headless',
          '--no-sandbox',
          '--disable-gpu',
          '--disable-dev-shm-usage',
        ],
      });
      const runnerResult = await lighthouse(url, {
        ...options,
        port: chrome.port,
      });

      const score = runnerResult.lhr.categories[a11yKey].score * 100;
      expect(score).toBeGreaterThanOrEqual(threshold);
      data[label] = { score, threshold };

      await chrome.kill();
    });
  }, urls);
});
