import { changeVibe, itShouldFindNbResults, testidSelector } from './utils';
import * as R from 'ramda';

describe('external dataflow flow', () => {
  let page;
  beforeAll(async done => {
    await changeVibe('config')('main');
    await changeVibe('sfs')('oecd');
    await changeVibe('nsi')('qna_comb');
    page = await global.__BROWSER__.newPage();
    await page.setViewport({ width: 1200, height: 800 });
    const url = global._E2E_.makeUrl(`?tm=external&pg=0&snb=1`);
    await page.goto(url);
    done();
  });
  it('should load the search results page and find 2 results', async () => {
    itShouldFindNbResults({ page, nb: 2 });
  });
  it('should find external dataflow and open it', async () => {
    await page.waitForSelector(testidSelector('search_results'));
    const externalDFLink = await page.$(
      `${testidSelector('ds-demo-release:DSD_QNA_COMB@DF_QNA_COMB_EXT')} a`,
    );
    await page.setRequestInterception(true);
    page.on('request', req => {
      setTimeout(() => req.continue(), 500);
    });
    await externalDFLink.click();
  });
  it('load initial structure then the external one and finally load the table', async () => {
    await page.waitForResponse(
      res =>
        R.includes(
          '/dataflow/OECD.SDD.NAD/DSD_QNA_COMB@DF_QNA_COMB_EXT/1.0',
          res.url(),
        ) && res.status() === 200,
    );
    await page.waitForResponse(
      res =>
        R.includes(
          '/dataflow/OECD.SDD.NAD/DSD_QNA_COMB@DF_QNA_COMB/1.0',
          res.url(),
        ) && res.status() === 200,
    );
  });
  it('should display correct header title', async () => {
    await page.waitForSelector(testidSelector('vis-table'));
    await page.waitForSelector(testidSelector('data-header-title'));
    const headerTitle = await page.$eval(
      testidSelector('data-header-title'),
      n => n.textContent,
    );
    expect(headerTitle).toEqual(
      'Quarterly GDP - expenditure approach (combined measure test)',
    );
  });
  it('go back to search results', async () => {
    const link = await page.$(testidSelector('back-search-link'));
    await link.click();
  });
  it('should find regular dataflow and open it', async () => {
    await page.waitForSelector(testidSelector('search_results'));
    await changeVibe('nsi')('sna');
    const externalDFLink = await page.$(
      `${testidSelector('ds-demo-release:SNA_TABLE1')} a`,
    );
    await externalDFLink.click();
  });
  it('should display correct header title', async () => {
    await page.waitForSelector(testidSelector('vis-table'));
    await page.waitForSelector(testidSelector('data-header-title'));
    const headerTitle = await page.$eval(
      testidSelector('data-header-title'),
      n => n.textContent,
    );
    expect(headerTitle).toEqual('1. Gross domestic product (GDP)');
  });
  it('go back to search results', async () => {
    const link = await page.$(testidSelector('back-search-link'));
    await link.click();
  });
  it('reopen external dataflow', async () => {
    await page.waitForSelector(testidSelector('search_results'));
    await changeVibe('nsi')('qna_comb');
    const externalDFLink = await page.$(
      `${testidSelector('ds-demo-release:DSD_QNA_COMB@DF_QNA_COMB_EXT')} a`,
    );
    await externalDFLink.click();
  });
  it('should display correct header title', async () => {
    await page.waitForSelector(testidSelector('vis-table'));
    await page.waitForSelector(testidSelector('data-header-title'));
    const headerTitle = await page.$eval(
      testidSelector('data-header-title'),
      n => n.textContent,
    );
    expect(headerTitle).toEqual(
      'Quarterly GDP - expenditure approach (combined measure test)',
    );
  });
  it('reload search page', async () => {
    const nextUrl = global._E2E_.makeUrl(`?tm=external&pg=0&snb=1`);
    await page.goto(nextUrl);
  });
  it('should load the search results page and find 2 results', async () => {
    itShouldFindNbResults({ page, nb: 2 });
  });
  it('should find regular dataflow and open it', async () => {
    await page.waitForSelector(testidSelector('search_results'));
    const externalDFLink = await page.$(
      `${testidSelector('ds-demo-release:SNA_TABLE1')} a`,
    );
    await changeVibe('nsi')('sna');
    await externalDFLink.click();
  });
  it('should display correct header title', async () => {
    await page.waitForSelector(testidSelector('vis-table'));
    await page.waitForSelector(testidSelector('data-header-title'));
    const headerTitle = await page.$eval(
      testidSelector('data-header-title'),
      n => n.textContent,
    );
    expect(headerTitle).toEqual('1. Gross domestic product (GDP)');
  });
  it('go back to search results', async () => {
    const link = await page.$(testidSelector('back-search-link'));
    await link.click();
  });
  it('reopen external dataflow', async () => {
    await page.waitForSelector(testidSelector('search_results'));
    await changeVibe('nsi')('qna_comb');
    const externalDFLink = await page.$(
      `${testidSelector('ds-demo-release:DSD_QNA_COMB@DF_QNA_COMB_EXT')} a`,
    );
    await externalDFLink.click();
  });
  it('should display correct header title', async () => {
    await page.waitForSelector(testidSelector('vis-table'));
    await page.waitForSelector(testidSelector('data-header-title'));
    const headerTitle = await page.$eval(
      testidSelector('data-header-title'),
      n => n.textContent,
    );
    expect(headerTitle).toEqual(
      'Quarterly GDP - expenditure approach (combined measure test)',
    );
  });
  it('go back to search results', async () => {
    const link = await page.$(testidSelector('back-search-link'));
    await link.click();
  });
  it('reopen regular dataflow', async () => {
    await page.waitForSelector(testidSelector('search_results'));
    const externalDFLink = await page.$(
      `${testidSelector('ds-demo-release:SNA_TABLE1')} a`,
    );
    await changeVibe('nsi')('sna');
    await externalDFLink.click();
  });
  it('should display correct header title', async () => {
    await page.waitForSelector(testidSelector('vis-table'));
    await page.waitForSelector(testidSelector('data-header-title'));
    const headerTitle = await page.$eval(
      testidSelector('data-header-title'),
      n => n.textContent,
    );
    expect(headerTitle).toEqual('1. Gross domestic product (GDP)');
  });
});
