import { changeVibe, testidSelector } from './utils';
describe('time period and frequency selections', () => {
  let page;
  beforeAll(async done => {
    await changeVibe('config')('main');
    await changeVibe('nsi')('freq');
    page = await global.__BROWSER__.newPage();
    await page.setViewport({ width: 1200, height: 800 });
    const url = global._E2E_.makeUrl(
      `/vis?lc=en&df[ds]=hybrid&df[id]=DSD_PRICES%40DF_PRICES_ALL&df[ag]=OECD.SDD.TPS&df[vs]=1.0&av=true&pd=2009%2C2010&dq=.A...IX._T..&ly[rw]=REF_AREA&ly[cl]=TIME_PERIOD&ly[rs]=METHODOLOGY&to[TIME_PERIOD]=false&vw=tb`,
    );

    await page.goto(url);
    done();
  });
  it('load the viz and open Time Period and frequency filter', async () => {
    await page.waitForSelector(
      `${testidSelector(
        'expansion_panel_panel',
      )}[aria-label="Frequency of observation & Time period"]`,
    );

    const filter = await page.waitForSelector('div[id="PANEL_PERIOD"]');
    await filter.click();
  });
  it('check current frequency and open frequency list ', async () => {
    const freq = await page.waitForSelector(
      `${testidSelector('select-testid')}[title="Annual"]`,
    );
    const currentFreq = await page.$eval(
      testidSelector('select-testid'),
      node => node.title,
    );
    expect(currentFreq).toEqual('Annual');
    await freq.click();
  });
  it('select freq Monthly', async () => {
    const liCount = await page.$$eval(
      'div > ul[role="listbox"] > li[role="option"]',
      li => li.length,
    );
    expect(liCount).toEqual(3);
    const freqM = await page.$(`div> ul > li[id="Monthly"]`);
    await freqM.press('Enter');
    await page.waitForSelector(
      `${testidSelector('deleteChip-test-id')}[aria-label="Monthly"]`,
    );
  });
  it('url should contain updated time format and dataquery', async () => {
    const url = await page.url();
    expect(url).toContain('dq=.M...IX._T..');
    expect(url).toContain('pd=2009-01%2C2010-12');
  });
  it('load url in annual and last 5 periods selection', async () => {
    const nextUrl = global._E2E_.makeUrl(
      `/vis?lc=en&df[ds]=hybrid&df[id]=DSD_PRICES%40DF_PRICES_ALL&df[ag]=OECD.SDD.TPS&df[vs]=1.0&av=true&dq=.A...IX._T..&vw=tb&lo=5&lom=LASTNPERIODS`,
    );
    await page.goto(nextUrl);
  });
  it('load the viz and open Time Period and frequency filter', async () => {
    await page.waitForSelector(
      `${testidSelector(
        'expansion_panel_panel',
      )}[aria-label="Frequency of observation & Time period"]`,
    );

    const filter = await page.waitForSelector('div[id="PANEL_PERIOD"]');
    await filter.click();
  });
  it('select freq Monthly', async () => {
    const freq = await page.waitForSelector(
      `${testidSelector('select-testid')}[title="Annual"]`,
    );
    await freq.click();
    const freqM = await page.$(`div> ul > li[id="Monthly"]`);
    await freqM.press('Enter');
    await page.waitForSelector(
      `${testidSelector('deleteChip-test-id')}[aria-label="Monthly"]`,
    );
  });
  it('url should contain updated time format and keep last 5 periods selection', async () => {
    const url = await page.url();
    expect(url).toContain('dq=.M...IX._T..');
    expect(url).toContain('lo=5&lom=LASTNPERIODS');
  });
});
