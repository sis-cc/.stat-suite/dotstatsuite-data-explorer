import { changeVibe, testidSelector } from '../utils';
import { ID_HOME_PAGE } from '../../css-api/index';

describe('reset search', () => {
  let page;
  beforeAll(async done => {
    await changeVibe('config')('main');
    await changeVibe('sfs')('oecd');
    page = await global.__BROWSER__.newPage();
    await page.setViewport({ width: 1200, height: 800 });
    const url = global._E2E_.makeUrl(`?tenant=oecd:de`);
    await page.goto(url);
    done();
  });
  it('load the homepage', async done => {
    await page.waitForSelector(testidSelector('collapseButton_Topic'));
    if (process.env.E2E_ENV === 'debug') await page.waitFor(1000);
    done();
  });
  it('should open the facet Topic', async done => {
    await page.click(testidSelector('collapseButton_Topic'));
    if (process.env.E2E_ENV === 'debug') await page.waitFor(1000);
    done();
  });
  it('should click on the facet value Industry', async done => {
    await page.evaluate(() => {
      document
        .querySelector(
          '[data-testid="collapseButton_Topic_value_0|Government#GOV#"]',
        )
        .click();
    });
    done();
  });
  it("should find 'Core government results' in the results", async done => {
    await page.waitForSelector(
      testidSelector('ds:qa:stable:DF_GOV_CORE_RESULTS'),
    );
    if (process.env.E2E_ENV === 'debug') await page.waitFor(1000);
    const text = await page.evaluate(() => document.body.textContent);
    expect(text).toContain('Core government results');
    done();
  });
  it("should open the current filters and click on 'clear all filters'", async done => {
    await page.waitForSelector(testidSelector('usedFilters-vis-test-id'));
    if (process.env.E2E_ENV === 'debug') await page.waitFor(1000);
    const clearAllSelector =
      "[data-testid='usedFilters-vis-test-id'] div:last-child div [data-testid='deleteChip-test-id'] span[role='button']:last-of-type";
    await page.waitForSelector(clearAllSelector);
    await page.click(clearAllSelector);
    done();
  });
  it('should reset the search and go back to the homepage', async done => {
    await page.waitForSelector(testidSelector('spotlight_input'));
    if (process.env.E2E_ENV === 'debug') await page.waitFor(1000);
    done();
  });
  it("type 'tourism' in the free text search and press 'enter' key", async done => {
    await page.click(testidSelector('spotlight_input'));
    await page.keyboard.type('tourism');
    if (process.env.E2E_ENV === 'debug') await page.waitFor(1000);
    await page.keyboard.press('Enter');
    done();
  });
  it('should find the expected dataflow in the results', async done => {
    await page.waitForSelector(
      testidSelector('ds-demo-release:ENT_EMP@TOURISM_KEY'),
    );
    if (process.env.E2E_ENV === 'debug') await page.waitFor(1000);
    const text = await page.evaluate(() => document.body.textContent);
    expect(text).toContain('Enterprises and employment in tourism');
    done();
  });
  it('clear the free text search', async done => {
    const selector = `${testidSelector('spotlight')} button:nth-of-type(2)`;
    await page.click(selector);
    done();
  });
  it('should reset the search and go back to the homepage', async done => {
    await page.waitForSelector(`#${ID_HOME_PAGE}`);
    if (process.env.E2E_ENV === 'debug') await page.waitFor(1000);
    done();
  });
});
