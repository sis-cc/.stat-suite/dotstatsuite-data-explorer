import { changeVibe, testidSelector, itShouldFindNbResults } from '../utils';
import { ID_HOME_PAGE } from '../../css-api/index';

const itShouldLoadTheHomePage = async ({ page }) => {
  await page.waitForSelector(`#${ID_HOME_PAGE}`);
};

describe('search by facet', () => {
  let page;

  beforeAll(async () => {
    await changeVibe('config')('main');
    await changeVibe('sfs')('oecd');
    page = await global.__BROWSER__.newPage();
    await page.setViewport({ width: 1200, height: 800 });
    const url = global._E2E_.makeUrl(
      `?tenant=oecd:de&fs[0]=Reference%20area%2C0%7CFrance%23FR%23&fs[1]=Reporting%20institutional%20sector%2C0%7CGeneral%20government%23S13%23&fs[2]=Stocks%2C0%7CBalance%20of%20primary%20incomes%23B5G%23&pg=0&fc=search-used&snb=2`,
    );
    await page.goto(url);
  });

  beforeEach(async () => {
    if (process.env.E2E_ENV === 'debug') await page.waitFor(1000);
  });

  it(`should find 2 results`, async () => {
    await itShouldFindNbResults({ page, nb: 2 });
  });

  it("remove 'Stocks' facet in the current filters", async () => {
    await page.waitForSelector(testidSelector('chips-test-id'));
    const facetSelector =
      "[data-testid='chips-test-id'][aria-label='Stocks:'] span[role='button']:last-of-type";
    await page.waitForSelector(facetSelector);
    await page.click(facetSelector);
  });

  it(`should find 5 results`, async () => {
    await itShouldFindNbResults({ page, nb: 5 });
  });

  it("should find the current filters and click on 'clear all filters'", async () => {
    await page.waitForSelector(testidSelector('usedFilters-vis-test-id'));
    if (process.env.E2E_ENV === 'debug') await page.waitFor(1000);
    const clearAllSelector =
      "[data-testid='clear-filters-test-id'] span[role='button']:last-of-type";
    await page.waitForSelector(clearAllSelector);
    if (process.env.E2E_ENV === 'debug') await page.waitFor(1000);
    await page.click(clearAllSelector);
  });

  it(`should reset the search and go back to the homepage`, async () => {
    await itShouldLoadTheHomePage({ page });
  });
});
