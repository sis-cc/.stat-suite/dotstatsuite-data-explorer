import { changeVibe, testidSelector, waitForTransition } from '../utils';

describe('search by free text', () => {
  let page;
  beforeAll(async done => {
    await changeVibe('config')('main');
    await changeVibe('sfs')('oecd');
    page = await global.__BROWSER__.newPage();
    await page.setViewport({ width: 1200, height: 800 });
    const url = global._E2E_.makeUrl(`?tenant=oecd:de`);
    await page.goto(url);
    done();
  });
  it('load the homepage', async done => {
    await page.waitForSelector(testidSelector('spotlight_input'));
    if (process.env.E2E_ENV === 'debug') await page.waitFor(1000);
    done();
  });
  it("type 'tourism' in the free text search and press 'enter' key", async done => {
    await page.click(testidSelector('spotlight_input'));
    await page.keyboard.type('tourism');
    if (process.env.E2E_ENV === 'debug') await page.waitFor(1000);
    await page.keyboard.press('Enter');
    done();
  });
  it('should load the results', async done => {
    await page.waitForSelector(
      testidSelector('ds-demo-release:ENT_EMP@TOURISM_KEY'),
    );
    const text = await page.evaluate(() => document.body.textContent);
    expect(text).toContain('Enterprises and employment in tourism');
    done();
  });
  it("click on locale 'Français'", async done => {
    await page.click('#languages');
    //if (process.env.E2E_ENV === 'debug') await page.waitFor(1000);
    await waitForTransition(page, 'div#menu- > div:nth-child(3)');
    await page.click('li#fr');
    done();
  });
  it('should go back to the homepage in French', async done => {
    await page.waitForSelector(testidSelector('collapseButton_Thème'));
    if (process.env.E2E_ENV === 'debug') await page.waitFor(1000);
    done();
  });
  it("should open the facet Topic and find at least 'Société'", async done => {
    await page.click(testidSelector('collapseButton_Thème'));
    if (process.env.E2E_ENV === 'debug') await page.waitFor(1000);
    await page.waitForSelector(
      testidSelector('collapseButton_Thème_value_0|Éducation#EDU#'),
    );
    const text = await page.evaluate(() => document.body.textContent);
    expect(text).toContain('Société');
    done();
  });
});
