import { changeVibe } from '../utils';

describe('customize table in viz', () => {
  let page;
  beforeAll(async done => {
    await changeVibe('config')('main');
    await changeVibe('nsi')('oecd');
    page = await global.__BROWSER__.newPage();
    await page.setViewport({ width: 1200, height: 800 });
    const url = global._E2E_.makeUrl(
      `/vis?tenant=oecd:de&fs[0]=Topic%2C0%7CEconomy%23ECO%23&fs[1]=Reference%20area%2C0%7CFrance%23FR%23&pg=0&fc=Reference%20area&snb=11&df[ds]=ds%3Ademo%3Arelease&df[id]=DF_EO_N&df[ag]=OECD.ECO&df[vs]=1.2&pd=2018%2C2021&dq=FR...EUR.A&ly[rw]=TIME_PERIOD%2CMEASURE&vw=tb`,
    );
    await page.goto(url);
    done();
  });
  beforeEach(async () => {
    if (process.env.E2E_ENV === 'debug') await page.waitFor(1000);
  });
  it('should load the viz page', async done => {
    await page.waitForSelector('#id_vis_page');
    done();
  });
  it("should expand 'Unit of measure' and click 'Euro'", async done => {
    // do stuff
    done();
  });
  it('should update the table and found # cells', async done => {
    // do stuff
    done();
  });
  it("should open customize panel, drag and drop 'Time Period in rows' and apply", async done => {
    // do stuff
    done();
  });
  it('should update the table and found # cells', async done => {
    // do stuff
    done();
  });
});
