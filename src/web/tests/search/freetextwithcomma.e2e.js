import { changeVibe, itShouldFindNbResults, testidSelector } from '../utils';
import * as R from 'ramda';

describe('search by free text', () => {
  let page;
  beforeAll(async done => {
    await changeVibe('config')('main');
    await changeVibe('sfs')('oecd');
    page = await global.__BROWSER__.newPage();
    await page.setViewport({ width: 1200, height: 800 });
    const url = global._E2E_.makeUrl(
      `?tm=indicators%2C%202023%20edition&pg=0&snb=1`,
    );
    await page.goto(url);
    done();
  });
  beforeEach(async () => {
    if (process.env.E2E_ENV === 'debug') await page.waitFor(1000);
  });
  it('should load the search results page and find 1 result', async done => {
    itShouldFindNbResults({ page, nb: 1 });
    done();
  });
  it('should find the expected dataflow in the results', async done => {
    await page.waitForSelector(
      testidSelector('ds-demo-release:DSD_GOV_TDG_SPS_GPC@DF_GOV_SPS_2023'),
    );
    const text = await page.evaluate(() => document.body.textContent);
    expect(text).toContain('indicators, 2023 edition');
    await page.waitForSelector(testidSelector('search_results'));
    done();
  });
  it('should have 7 highlights in the results', async done => {
    await page.waitForSelector(testidSelector('search_results'));
    const markTags = await page.$$(`${testidSelector('search_results')} mark`);
    expect(R.length(markTags)).toEqual(7);
    done();
  });
});
