import { changeVibe, testidSelector } from '../utils';
import * as R from 'ramda';

// if style is not found, expect fails
const expectHighlightStyleHelper = (style = {}) => {
  expect(R.prop('backgroundColor', style)).toEqual('rgb(255, 255, 0)');
  expect(R.prop('borderBottomColor', style)).toEqual('rgb(140, 200, 65)');
  expect(R.prop('borderBottomWidth', style)).toEqual('2px');
  expect(R.prop('borderBottomStyle', style)).toEqual('solid');
};

describe('search by free text', () => {
  let page;
  beforeAll(async done => {
    await changeVibe('config')('main');
    await changeVibe('sfs')('oecd');
    page = await global.__BROWSER__.newPage();
    await page.setViewport({ width: 1200, height: 800 });
    const url = global._E2E_.makeUrl(`?tenant=oecd:de`);
    await page.goto(url);
    done();
  });
  it('load the homepage', async done => {
    await page.waitForSelector(testidSelector('spotlight_input'));
    if (process.env.E2E_ENV === 'debug') await page.waitFor(1000);
    done();
  });
  it("type 'tourism' in the free text search and press 'enter' key", async done => {
    await page.click(testidSelector('spotlight_input'));
    await page.keyboard.type('tourism');
    if (process.env.E2E_ENV === 'debug') await page.waitFor(1000);
    await page.keyboard.press('Enter');
    done();
  });
  it('should find the expected dataflow in the results', async done => {
    await page.waitForSelector(
      testidSelector('ds-demo-release:ENT_EMP@TOURISM_KEY'),
    );
    const text = await page.evaluate(() => document.body.textContent);
    expect(text).toContain('Enterprises and employment in tourism');
    done();
  });
  it('should have 4 highlights in the results', async done => {
    await page.waitForSelector(testidSelector('search_results'));
    const markTags = await page.$$(`${testidSelector('search_results')} mark`);
    expect(R.length(markTags)).toEqual(4);
    done();
  });
  it("should have 'tourism' highlighted in the title (style checked)", async done => {
    await page.waitForSelector(testidSelector('search_results'));
    const style = await page.evaluate(() => {
      const tags = document
        .querySelector(
          '[data-testid="ds-demo-release:ENT_EMP@TOURISM_KEY_title"]',
        )
        .getElementsByTagName('mark');
      return JSON.parse(JSON.stringify(getComputedStyle(tags[0])));
    });
    expectHighlightStyleHelper(style);
    done();
  });
  it("should have 'tourism' highlighted in the activity attribute (style checked)", async done => {
    await page.waitForSelector(testidSelector('search_results'));
    const style = await page.evaluate(() => {
      const tags = document
        .querySelector(
          '[data-testid="ds-demo-release:ENT_EMP@TOURISM_KEY_highlight_Activity"]',
        )
        .getElementsByTagName('mark');
      return JSON.parse(JSON.stringify(getComputedStyle(tags[0])));
    });
    expectHighlightStyleHelper(style);
    done();
  });
});
