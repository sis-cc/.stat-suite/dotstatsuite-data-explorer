import {
  changeVibe,
  testidSelector,
  itShouldFindNbResults,
  itShouldFindNbCells,
} from '../utils';
import * as R from 'ramda';

describe('search results to viz page', () => {
  let page;
  beforeAll(async done => {
    await changeVibe('config')('main');
    await changeVibe('sfs')('oecd');
    page = await global.__BROWSER__.newPage();
    await page.setViewport({ width: 1200, height: 800 });
    const url = global._E2E_.makeUrl(
      `?tenant=oecd:de&fs[0]=Topic%2C0%7CEconomy%23ECO%23&fs[1]=Reference%20area%2C0%7CFrance%23FR%23&pg=0&fc=search-used&snb=11`,
    );
    await page.goto(url);
    done();
  });
  beforeEach(async () => {
    if (process.env.E2E_ENV === 'debug') await page.waitFor(1000);
  });
  it('should load the search results page and find 11 results', async done => {
    itShouldFindNbResults({ page, nb: 11 });
    done();
  });
  it('should not find go back link', async done => {
    const link = await page.$(testidSelector('back-search-link'));
    expect(link).toEqual(null);
    done();
  });
  it("should click on 'Economic Outlook No 106 (new structure)'", async done => {
    await changeVibe('nsi')('oecd');
    const selector = testidSelector('ds-demo-release:DF_EO_N');
    await page.waitForSelector(selector);
    await page.click(`${selector} a`);
    done();
  });
  it('should load the viz page and see a table (configured) with 50 of cells', async done => {
    await page.waitForSelector('#id_viewer_component');
    await itShouldFindNbCells({ page, nb: 50 });
    done();
  });
  it("should not find 'Topic' in the filters (not a facet)", async done => {
    await page.waitForSelector(testidSelector('usedFilters-vis-test-id'));
    const texts = await page.$$eval(
      `${testidSelector('chips-test-id')} > span:first-child`,
      nodes => nodes.map(n => n.textContent),
    );
    expect(texts).not.toContain('Topic:');
    done();
  });
  it("should find 'Reference Area' set to 'France' in the filters (from facet)", async done => {
    await page.waitForSelector(testidSelector('usedFilters-vis-test-id'));
    const FiltersTexts = await page.$$eval(
      `${testidSelector('chips-test-id')} > span:first-child`,
      nodes => nodes.map(n => n.textContent),
    );
    const ValuesTexts = await page.$$eval(
      `${testidSelector('deleteChip-test-id')} > span:first-child`,
      nodes => nodes.map(n => n.textContent),
    );
    const texts = R.pipe(
      R.transpose,
      R.map(R.join(' ')),
    )([FiltersTexts, ValuesTexts]);
    expect(texts).toContain('Reference area: France');
    done();
  });
  it("should find 'Frequency' in the filters (single value)", async done => {
    await page.waitForSelector(testidSelector('usedFilters-vis-test-id'));
    const texts = await page.$$eval(
      `${testidSelector('chips-test-id')} > span:first-child`,
      nodes => nodes.map(n => n.textContent),
    );
    expect(texts).toContain('Frequency of observation:');
    done();
  });
  it('should find go back to search link', async done => {
    const link = await page.$(testidSelector('back-search-link'));
    expect(link).not.toEqual(null);
    done();
  });
  it('clicking the link should go back to search page', async done => {
    const link = await page.$(testidSelector('back-search-link'));
    await link.click();
    itShouldFindNbResults({ page, nb: 11 });
    done();
  });
  it('should not find go back link', async done => {
    const link = await page.$(testidSelector('back-search-link'));
    expect(link).toEqual(null);
    done();
  });
});
