import { changeVibe, testidSelector } from '../utils';
import * as R from 'ramda';

describe('search by facet', () => {
  let page;
  let searchRequests = [];
  beforeAll(async done => {
    await changeVibe('config')('main');
    await changeVibe('sfs')('oecd');
    page = await global.__BROWSER__.newPage();
    await page.setViewport({ width: 1200, height: 800 });
    const url = global._E2E_.makeUrl(`?tenant=oecd:de`);

    await page.setRequestInterception(true);
    page.on('request', request => {
      const url = request.url();
      const method = request.method();
      if (method !== 'OPTIONS' && R.includes('api/search?tenant=oecd', url)) {
        searchRequests.push(request);
      }
      request.continue();
    });

    await page.goto(url);

    done();
  });
  it('load the homepage', async done => {
    await page.waitForSelector(testidSelector('collapseButton_Topic'));
    if (process.env.E2E_ENV === 'debug') await page.waitFor(1000);
    done();
  });
  it('should open the facet Topic', async done => {
    await page.click(testidSelector('collapseButton_Topic'));
    if (process.env.E2E_ENV === 'debug') await page.waitFor(1000);
    done();
  });
  it('should click on the facet value Industry', async done => {
    await page.evaluate(() => {
      document
        .querySelector(
          '[data-testid="collapseButton_Topic_value_0|Government#GOV#"]',
        )
        .click();
    });
    done();
  });
  it("should find 'Core government results' in the results", async done => {
    await page.waitForSelector(
      testidSelector('ds:qa:stable:DF_GOV_CORE_RESULTS'),
    );
    const text = await page.evaluate(() => document.body.textContent);
    expect(text).toContain('Core government results');
    done();
  });
  it("should find 'Open government data' in the results", async done => {
    await page.waitForSelector(testidSelector('ds:qa:stable:DF_GOV_OG'));
    const text = await page.evaluate(() => document.body.textContent);
    expect(text).toContain('Open government data');
    done();
  });
  it("should open the current filters and find 'Topic' and 'Government'", async done => {
    await page.waitForSelector(testidSelector('usedFilters-vis-test-id'));
    if (process.env.E2E_ENV === 'debug') await page.waitFor(1000);
    const text = await page.evaluate(() => document.body.textContent);
    expect(text).toContain('Topic');
    expect(text).toContain('Government');
    done();
  });
  it('should request search exactly twice', () => {
    expect(searchRequests.length).toBe(2);
  });
});
