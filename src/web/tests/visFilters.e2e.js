import { changeVibe, testidSelector, waitForTransition } from './utils';
import * as R from 'ramda';

describe('vis Filters: hierarchical selections', () => {
  let page;
  let hasRequestedTimeAvailability = false;
  let hasRequestedDataAvailability = false;
  beforeAll(async () => {
    await changeVibe('config')('main');
    await changeVibe('nsi')('crs1');
    page = await global.__BROWSER__.newPage();
    await page.setViewport({ width: 1200, height: 800 });
    const url = global._E2E_.makeUrl(
      `/vis?tenant=oecd:de&lc=en&df[ds]=hybrid&df[id]=DSD_CRS%40DF_CRS1&df[ag]=OECD.DCD&df[vs]=1.0&av=true&pd=2019%2C2020&&dq=AUS........A`,
    );
    await page.setRequestInterception(true);
    page.on('request', req => {
      const url = req.url();
      if (R.includes('/availableconstraint', url)) {
        if (R.endsWith('/TIME_PERIOD', url)) {
          hasRequestedTimeAvailability = true;
        } else {
          hasRequestedDataAvailability = true;
        }
      }
      req.continue();
    });
    page.goto(url);
  });
  it('load the viz page and wait for data request', async () => {
    await page.waitForResponse(res => R.includes('/data/', res.url()));
  });
  it('available constraint should have been performed', async () => {
    expect(hasRequestedDataAvailability).toEqual(true);
  });
  it('open RECIPIENT filter', async () => {
    await page.waitForSelector('div[id="RECIPIENT"]');
    await page.evaluate(() => {
      document
        .querySelector('[data-testid="filter_panel"][id="RECIPIENT"]')
        .click();
    });
    await waitForTransition(
      page,
      `${testidSelector('filter_panel')}[id="RECIPIENT"] > div`,
    );
  });
  it('type "albania" in the spotlight', async () => {
    const spotlight = await page.$(
      `div[id="RECIPIENT"] > div > ${testidSelector('spotlight')}`,
    );
    await spotlight.click();
    await page.keyboard.type('albania');
  });
  it('CEEC value should be disabled', async () => {
    const isValueDisabled = await page.$eval(
      testidSelector('value_CEEC'),
      node => node.ariaDisabled,
    );
    expect(isValueDisabled).toEqual('true');
  });
  it('select albania', async () => {
    const albaniaValue = await page.$(testidSelector('value_CEEC.ALB'));
    await albaniaValue.click();
    await page.waitForSelector(
      `${testidSelector('deleteChip-test-id')}[aria-label="Albania"]`,
    );
  });
  it('CEEC value not disabled anymore', async () => {
    const isValueDisabled = await page.$eval(
      testidSelector('value_CEEC'),
      node => node.ariaDisabled,
    );
    expect(isValueDisabled).toEqual('false');
  });
  it('select CEEC', async () => {
    const ceecValue = await page.$(testidSelector('value_CEEC'));
    await ceecValue.click();
    await page.waitForSelector(
      `${testidSelector('deleteChip-test-id')}[aria-label="CEEC"]`,
    );
  });
  it('deselect Albania', async () => {
    const albaniaValue = await page.$(testidSelector('value_CEEC.ALB'));
    await albaniaValue.click();
    await page.waitForTimeout(500);
  });
  it('CEEC should be deselected as well and re-disabled', async () => {
    const usedFilters = await page.$(testidSelector('usedFilters-vis-test-id'));
    const usedFiltersLabels = await usedFilters.$$eval(
      testidSelector('deleteChip-test-id'),
      nodes => nodes.map(n => n.ariaLabel),
    );
    const ceecValueDisabled = await page.$eval(
      testidSelector('value_CEEC'),
      node => node.ariaDisabled,
    );
    expect(usedFiltersLabels).not.toContain('CEEC');
    expect(ceecValueDisabled).toEqual('true');
  });
  it('open advanced filter pop up', async () => {
    const button = await page.$(`div[id="RECIPIENT"] > div > button`);
    await button.click();
    await page.waitForSelector('div[role="dialog"]');
  });
  it('open selection mode menu', async () => {
    const button = await page.$(
      `button[aria-label="Item and all items directly below"]`,
    );
    await button.click();
  });
  it('new url to ALWAYS_DISPLAY_PARENTS version', async () => {
    const nextUrl = global._E2E_.makeUrl(
      `/vis?tenant=oecd:de&lc=en&df[ds]=hybrid&df[id]=DSD_CRS%40DF_CRS1&df[ag]=OECD.DCD&df[vs]=2.0&av=true&pd=2019%2C2020&&dq=AUS........A`,
    );
    await page.goto(nextUrl);
  });
  it('load the viz and open RECIPIENT filter', async () => {
    await page.waitForSelector('div[id="RECIPIENT"]');
    await page.evaluate(() => {
      document
        .querySelector('[data-testid="filter_panel"][id="RECIPIENT"]')
        .click();
    });
    await waitForTransition(
      page,
      `${testidSelector('filter_panel')}[id="RECIPIENT"] > div`,
    );
  });
  it('type "albania" in the spotlight', async () => {
    const spotlight = await page.$(
      `div[id="RECIPIENT"] > div > ${testidSelector('spotlight')}`,
    );
    await spotlight.click();
    await page.keyboard.type('albania');
  });
  it('CEEC value should be disabled', async () => {
    const isValueDisabled = await page.$eval(
      testidSelector('value_CEEC'),
      node => node.ariaDisabled,
    );
    expect(isValueDisabled).toEqual('true');
  });
  it('select albania', async () => {
    const albaniaValue = await page.$(testidSelector('value_CEEC.ALB'));
    await albaniaValue.click();
    await page.waitForSelector(
      `${testidSelector('deleteChip-test-id')}[aria-label="Albania"]`,
    );
  });
  it('all hierarchical parents should be selected and disabled', async () => {
    const ceecDeleteChip = await page.$(
      `${testidSelector('deleteChip-test-id')}[aria-label="CEEC"]`,
    );
    const dpgcDeleteChip = await page.$(
      `${testidSelector(
        'deleteChip-test-id',
      )}[aria-label="Developing countries"]`,
    );
    const eDeleteChip = await page.$(
      `${testidSelector('deleteChip-test-id')}[aria-label="Europe"]`,
    );
    const isCEECValueDisabled = await page.$eval(
      testidSelector('value_CEEC'),
      node => node.ariaDisabled,
    );
    const isDPGCValueDisabled = await page.$eval(
      testidSelector('value_DPGC'),
      node => node.ariaDisabled,
    );
    const isEValueDisabled = await page.$eval(
      testidSelector('value_DPGC.E'),
      node => node.ariaDisabled,
    );
    expect([ceecDeleteChip, dpgcDeleteChip, eDeleteChip]).not.toContain(null);
    expect([
      isCEECValueDisabled,
      isDPGCValueDisabled,
      isEValueDisabled,
    ]).not.toContain('false');
  });
  it('new url to ALWAYS_DISPLAY_PARENTS on level 2 version', async () => {
    const nextUrl = global._E2E_.makeUrl(
      `/vis?tenant=oecd:de&lc=en&df[ds]=hybrid&df[id]=DSD_CRS%40DF_CRS1&df[ag]=OECD.DCD&df[vs]=3.0&av=true&pd=2019%2C2020&&dq=AUS........A`,
    );
    await page.goto(nextUrl);
  });
  it('load the viz and open RECIPIENT filter', async () => {
    await page.waitForSelector('div[id="RECIPIENT"]');
    await page.evaluate(() => {
      document
        .querySelector('[data-testid="filter_panel"][id="RECIPIENT"]')
        .click();
    });
    await waitForTransition(
      page,
      `${testidSelector('filter_panel')}[id="RECIPIENT"] > div`,
    );
  });
  it('type "albania" in the spotlight', async () => {
    const spotlight = await page.$(
      `div[id="RECIPIENT"] > div > ${testidSelector('spotlight')}`,
    );
    await spotlight.click();
    await page.keyboard.type('albania');
  });
  it('select albania', async () => {
    const albaniaValue = await page.$(testidSelector('value_CEEC.ALB'));
    await albaniaValue.click();
    await page.waitForSelector(
      `${testidSelector('deleteChip-test-id')}[aria-label="Albania"]`,
    );
  });
  it('only "Europe" parents should be selected and disabled', async () => {
    const ceecDeleteChip = await page.$(
      `${testidSelector('deleteChip-test-id')}[aria-label="CEEC"]`,
    );
    const dpgcDeleteChip = await page.$(
      `${testidSelector(
        'deleteChip-test-id',
      )}[aria-label="Developing countries"]`,
    );
    const eDeleteChip = await page.$(
      `${testidSelector('deleteChip-test-id')}[aria-label="Europe"]`,
    );
    const isEValueDisabled = await page.$eval(
      testidSelector('value_DPGC.E'),
      node => node.ariaDisabled,
    );
    expect(ceecDeleteChip).toEqual(null);
    expect(dpgcDeleteChip).toEqual(null);
    expect(eDeleteChip).not.toEqual(null);
    expect(isEValueDisabled).toEqual('true');
  });
  it('new url to DISABLE_AVAILABILITY_REQUESTS version', async () => {
    const nextUrl = global._E2E_.makeUrl(
      `/vis?tenant=oecd:de&lc=en&df[ds]=hybrid&df[id]=DSD_CRS%40DF_CRS1&df[ag]=OECD.DCD&df[vs]=4.0&av=true&dq=AUS........A`,
    );
    hasRequestedTimeAvailability = false;
    hasRequestedDataAvailability = false;
    page.goto(nextUrl);
  });
  it('load the viz page and wait for data request', async () => {
    await page.waitForResponse(res => R.includes('/data/', res.url()));
  });
  it('should have requested for time availability but not data availability', () => {
    expect(hasRequestedDataAvailability).toEqual(false);
    expect(hasRequestedTimeAvailability).toEqual(true);
  });
  it('should display disclaimer in header', async () => {
    const disclaimer = await page.$(testidSelector('data-header-disclaimer'));
    expect(disclaimer).not.toEqual(null);
  });
  it('open RECIPIENT filter', async () => {
    await page.waitForSelector('div[id="RECIPIENT"]');
    await page.evaluate(() => {
      document
        .querySelector('[data-testid="filter_panel"][id="RECIPIENT"]')
        .click();
    });
    await waitForTransition(
      page,
      `${testidSelector('filter_panel')}[id="RECIPIENT"] > div`,
    );
  });
  it('type "albania" in the spotlight', async () => {
    const spotlight = await page.$(
      `div[id="RECIPIENT"] > div > ${testidSelector('spotlight')}`,
    );
    await spotlight.click();
    await page.keyboard.type('albania');
  });
  it('CEEC value should not be disabled', async () => {
    const isValueDisabled = await page.$eval(
      testidSelector('value_CEEC'),
      node => node.ariaDisabled,
    );
    expect(isValueDisabled).toEqual('false');
  });
});
