import {
  changeVibe,
  testidSelector,
  dragAndDrop,
  getTableLayout,
  getCustomizeLayout,
  waitForTransition,
} from './utils';
import * as R from 'ramda';

describe('table layout customization', () => {
  let page;
  beforeAll(async done => {
    await changeVibe('config')('main');
    await changeVibe('nsi')('sna');
    page = await global.__BROWSER__.newPage();
    await page.setViewport({ width: 1200, height: 800 });
    const url = global._E2E_.makeUrl(
      `/vis?tenant=oecd:de&lc=en&df[ds]=hybrid&df[id]=SNA_TABLE1&df[ag]=OECD&df[vs]=1.0&av=true&pd=2019%2C2020`,
    );
    await page.goto(url);
    done();
  });
  it('load the viz page and wait for the table to be displayed', async () => {
    await page.waitForSelector(testidSelector('vis-table'));
    if (process.env.E2E_ENV === 'debug') await page.waitFor(250);
  });
  it('table default layout', async () => {
    expect(await getTableLayout(page)).toEqual({
      sections: ['MEASURE'],
      header: ['TIME_PERIOD'],
      rows: ['LOCATION'],
    });
  });
  it('no layout entry should be put in url', async () => {
    const url = await page.url();
    expect(url).not.toContain('ly[rs]');
    expect(url).not.toContain('ly[rw]');
    expect(url).not.toContain('ly[cl]');
  });
  it('open LOCATION filter', async () => {
    await page.evaluate(() => {
      document
        .querySelector('[data-testid="filter_panel"][id="LOCATION"]')
        .click();
    });
    await waitForTransition(
      page,
      '[data-testid="filter_panel"][id="LOCATION"] > div',
    );
  });
  it('select AUS', async () => {
    await page.waitForSelector(testidSelector('value_AUS'));
    const val = await page.$(testidSelector('value_AUS'));
    await val.click();
    await page.waitForResponse(res =>
      R.includes('/data/OECD,SNA_TABLE1,1.0/AUS..', res.url()),
    );
  });
  it('table updated layout', async () => {
    await page.waitForSelector(testidSelector('vis-table'));
    expect(await getTableLayout(page)).toEqual({
      sections: [],
      header: ['TIME_PERIOD'],
      rows: ['MEASURE'],
    });
  });
  it('no layout entry should be put in url', async () => {
    const url = await page.url();
    expect(url).not.toContain('ly[rs]');
    expect(url).not.toContain('ly[rw]');
    expect(url).not.toContain('ly[cl]');
  });
  it('deselect AUS', async () => {
    await page.waitForSelector(testidSelector('value_AUS'));
    const val = await page.$(testidSelector('value_AUS'));
    await val.click();
    await page.waitForResponse(res =>
      R.includes('/data/OECD,SNA_TABLE1,1.0/all', res.url()),
    );
  });
  it('click on customize button', async () => {
    const toolbar = await page.$(testidSelector('detoolbar'));
    const customizeButton = await toolbar.$(testidSelector('customize'));
    await customizeButton.click();
  });
  it('wait for table layout menu to be opened', async () => {
    await page.waitForSelector(testidSelector('table-layout-test-id'));
  });
  it('single value in rows should not be draggable', async () => {
    // only way found to test non draggability through the cursor style
    const rowDraggable = await page.$(testidSelector('draggable-LOCATION'));
    expect(
      await page.evaluate(
        element => window.getComputedStyle(element).getPropertyValue('cursor'),
        rowDraggable,
      ),
    ).toEqual('no-drop');
  });
  it('change table layout: MEASURE into rows', async () => {
    await dragAndDrop(
      page,
      testidSelector('draggable-MEASURE'),
      testidSelector('droppable-rows'),
    );
    await page.waitForTimeout(1500);
  });
  it('multi values in rows are now draggable', async () => {
    const rowsDroppable = await page.$(testidSelector('droppable-rows'));
    expect(
      await rowsDroppable.$$eval('div', nodes =>
        nodes.map(n => window.getComputedStyle(n).getPropertyValue('cursor')),
      ),
    ).toEqual(['grab', 'grab']);
  });
  it('change table layout: LOCATION into header and TIME_PERIOD into sections', async () => {
    await dragAndDrop(
      page,
      testidSelector('draggable-LOCATION'),
      testidSelector('droppable-sections'),
    );
    await page.waitForTimeout(1500);
    await dragAndDrop(
      page,
      testidSelector('draggable-LOCATION'),
      testidSelector('droppable-header'),
    );
    await page.waitForTimeout(1500);
    await dragAndDrop(
      page,
      testidSelector('draggable-TIME_PERIOD'),
      testidSelector('droppable-sections'),
    );
    await page.waitForTimeout(1500);
    const applyButton = await page.$(testidSelector('table-layout-apply'));
    await applyButton.click();
  });
  it('table updated layout', async () => {
    await page.waitForTimeout(1500);
    expect(await getTableLayout(page)).toEqual({
      sections: ['TIME_PERIOD'],
      header: ['LOCATION'],
      rows: ['MEASURE'],
    });
  });
  it('layout should have been updated in the url', async () => {
    const url = await page.url();
    expect(url).toContain('ly[cl]=LOCATION');
    expect(url).toContain('ly[rs]=TIME_PERIOD');
    expect(url).toContain('ly[rw]=MEASURE');
  });
  it('go on the overview page by the UI', async () => {
    const overviewButton = await page.$(testidSelector('overview-button'));
    await overviewButton.click();
    await page.waitForSelector('[id="id_overview_component"]');
  });
  it('should have kept the layout in url', async () => {
    const url = await page.url();
    expect(url).toContain('ly[cl]=LOCATION');
    expect(url).toContain('ly[rs]=TIME_PERIOD');
    expect(url).toContain('ly[rw]=MEASURE');
  });
  it('return to table view', async () => {
    const tableButton = await page.$(testidSelector('table-button'));
    await tableButton.click();
    await page.waitForSelector(testidSelector('vis-table'));
  });
  it('select AUS', async () => {
    await page.waitForSelector(testidSelector('value_AUS'));
    const val = await page.$(testidSelector('value_AUS'));
    await val.click();
    await page.waitForResponse(res =>
      R.includes('/data/OECD,SNA_TABLE1,1.0/AUS..', res.url()),
    );
  });
  it('table layout should be updated without LOCATION', async () => {
    await page.waitForSelector(testidSelector('vis-table'));
    expect(await getTableLayout(page)).toEqual({
      sections: ['TIME_PERIOD'],
      header: [],
      rows: ['MEASURE'],
    });
  });
  it('should have kept the same layout in url', async () => {
    const url = await page.url();
    expect(url).toContain('ly[cl]=LOCATION');
    expect(url).toContain('ly[rs]=TIME_PERIOD');
    expect(url).toContain('ly[rw]=MEASURE');
  });
  it('deselect AUS', async () => {
    await page.waitForSelector(testidSelector('value_AUS'));
    const val = await page.$(testidSelector('value_AUS'));
    await val.click();
    await page.waitForResponse(res =>
      R.includes('/data/OECD,SNA_TABLE1,1.0/all', res.url()),
    );
  });
  it('LOCATION should go back into previous customized header and not default rows', async () => {
    await page.waitForSelector(testidSelector('vis-table'));
    expect(await getTableLayout(page)).toEqual({
      sections: ['TIME_PERIOD'],
      header: ['LOCATION'],
      rows: ['MEASURE'],
    });
  });
});

describe('table layout combinations', () => {
  let page;
  beforeAll(async done => {
    await changeVibe('nsi')('qna_comb');
    page = await global.__BROWSER__.newPage();
    await page.setViewport({ width: 1200, height: 800 });
    const url = global._E2E_.makeUrl(
      `/vis?tenant=oecd:de&lc=en&df[ds]=hybrid&df[id]=DSD_QNA_COMB@DF_QNA_COMB&df[ag]=OECD.SDD.NAD&df[vs]=1.0&vw=tb&ly[rs]=REF_SECTOR&ly[rw]=MEASURE&ly[cl]=TIME_PERIOD`,
    );
    await page.goto(url);
    done();
  });
  it('load the viz page and wait for the table to be displayed', async () => {
    await page.waitForSelector(testidSelector('vis-table'));
    if (process.env.E2E_ENV === 'debug') await page.waitFor(250);
  });
  it('should render a table with 2 combinations in rows', async () => {
    expect(await getTableLayout(page)).toEqual({
      sections: ['REF_SECTOR'],
      header: ['TIME_PERIOD'],
      rows: ['COMBINED_MEASURE', 'COMBINED_UNIT_MEASURE'],
    });
  });
  it('table should have 1 combination COMBINED_STUFF in header', async () => {
    const headerCombinations = await page.$$eval(
      testidSelector('data-header-combination'),
      nodes => nodes.map(n => n.ariaLabel),
    );
    expect(headerCombinations).toEqual(['Combined stuff:']);
  });
  it('table hierarchical display with empty line for missing parent in rows', async () => {
    const table = await await page.$(testidSelector('vis-table'));
    const lastSection = await table.$('tbody:last-of-type');
    const rows = await lastSection.$$('tr:not(:first-child)');
    const rowsContent = await Promise.all(
      rows.map(async row => {
        const headerContent = await row.$$eval('th > div > p', nodes =>
          nodes.map(n => n.innerText),
        );
        const cellsContent = await row.$$eval('td > span', nodes =>
          nodes.map(n => n.innerText),
        );
        return [headerContent, cellsContent];
      }),
    );
    expect(rowsContent).toEqual([
      [
        ['Gross domestic product at market prices', ''],
        ['', '', '', '', '', '', '', ''],
      ],
      [
        ['Domestic demand', ''],
        ['', '', '', '', '', '', '', ''],
      ],
      [
        [
          'Final consumption expenditure',
          'Current prices, Czech koruna, Millions',
        ],
        [
          '..',
          '..',
          '..',
          '385,273.0',
          '303,308.0',
          '325,880.0',
          '329,682.0',
          '403,886.0',
        ],
      ],
      [
        [
          'Individual consumption expenditure',
          'Current prices, Czech koruna, Millions',
        ],
        [
          '..',
          '..',
          '..',
          '224,486.0',
          '180,610.0',
          '191,251.0',
          '192,521.0',
          '234,271.0',
        ],
      ],
      [
        [
          'Collective consumption expenditure',
          'Current prices, Czech koruna, Millions',
        ],
        [
          '..',
          '..',
          '..',
          '160,787.0',
          '122,698.0',
          '134,629.0',
          '137,161.0',
          '169,615.0',
        ],
      ],
    ]);
  });
  it('put REF_SECTOR from sections into rows', async () => {
    const toolbar = await page.$(testidSelector('detoolbar'));
    const customizeButton = await toolbar.$(testidSelector('customize'));
    await customizeButton.click();
    await page.waitForSelector(testidSelector('table-layout-test-id'));
    await dragAndDrop(
      page,
      testidSelector('draggable-REF_SECTOR'),
      testidSelector('droppable-rows'),
    );
    await page.waitForSelector(
      `${testidSelector('droppable-rows')} > ${testidSelector(
        'draggable-REF_SECTOR',
      )}`,
    );
    const applyButton = await page.$(testidSelector('table-layout-apply'));
    await applyButton.click();
  });
  it('has drag and drop succeeded', async () => {
    const layout = await getCustomizeLayout(page);
    expect(layout).toEqual({
      sections: [],
      header: ['Time period'],
      rows: ['Combined measure'],
    });
  });
  it('table should keep 2 combinations in rows', async () => {
    expect(await getTableLayout(page)).toEqual({
      sections: [],
      header: ['TIME_PERIOD'],
      rows: ['COMBINED_MEASURE', 'COMBINED_UNIT_MEASURE'],
    });
  });
  it('every table footnotes should have a single unit mult entry', async () => {
    const footnotesSpans = await page.$$eval(
      `${testidSelector('cell-flags-footnotes-icon')} > span`,
      nodes => nodes.map(n => n.textContent),
    );
    const invalidContents = R.reject(
      span => span === '* Unit multiplier: Millions',
      footnotesSpans,
    );
    expect(invalidContents).toEqual([]);
  });
  it('switch display from rows to header', async () => {
    const url = global._E2E_.makeUrl(
      `/vis?tenant=oecd:de&lc=en&df[ds]=hybrid&df[id]=DSD_QNA_COMB@DF_QNA_COMB&df[ag]=OECD.SDD.NAD&df[vs]=1.0&vw=tb&ly[cl]=REF_SECTOR,MEASURE&ly[rw]=TIME_PERIOD`,
    );
    await page.goto(url);
    await page.waitForSelector(testidSelector('vis-table'));
  });
  it('empty columns for missing parents', async () => {
    const table = await await page.$(testidSelector('vis-table'));
    const lastSection = await table.$('tbody');
    const rows = await lastSection.$$('tr:not(:first-child)');
    const rowsContent = await Promise.all(
      rows.map(async row => {
        const cellsContent = await row.$$eval('td > span', nodes =>
          nodes.map(n => n.innerText),
        );
        return cellsContent;
      }),
    );
    const contentInColumns = R.transpose(rowsContent);
    expect([contentInColumns[7], contentInColumns[9]]).toEqual([
      ['', '', '', '', '', '', ''],
      ['', '', '', '', '', '', ''],
    ]);
  });
});
