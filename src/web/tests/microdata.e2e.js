import { changeVibe, testidSelector } from './utils';
import * as R from 'ramda';

describe('microdata table', () => {
  let page;
  beforeAll(async done => {
    await changeVibe('config')('main');
    await changeVibe('nsi')('ddown');
    page = await global.__BROWSER__.newPage();
    await page.setViewport({ width: 1200, height: 800 });
    const url = global._E2E_.makeUrl(
      `/vis?tenant=oecd:de&lc=en&df[ds]=hybrid&df[id]=DSD_DEBT_TRANS_DDOWN@DF_DDOWN&df[ag]=OECD.DAF&df[vs]=1.0&vw=tb`,
    );
    await page.goto(url);
    done();
  });
  it('load the viz page and wait for the table to be displayed', async () => {
    await page.waitForSelector(testidSelector('vis-table'));
    if (process.env.E2E_ENV === 'debug') await page.waitFor(250);
  });
  it('should display disabled microdata button in toolbar', async () => {
    const toolbar = await page.$(testidSelector('detoolbar'));
    const hasDisabledMicrodataButton = await toolbar.$eval(
      '[id="microdata"]',
      button => button.disabled,
    );
    expect(hasDisabledMicrodataButton).toEqual(true);
  });
  it('click obs cell hyperlink', async () => {
    const cell = await page.$('td[headers="REF_AREA=TUR_0_0 2_0 MEASURE=GDO"]');
    const hyperlink = await cell.$('.MuiLink-underlineAlways');
    await hyperlink.click();
  });
  it('should display appropriate microdata table', async () => {
    const table = await page.waitForSelector(testidSelector('microdata-table'));
    const tableFirstTimeCell = await table.$eval('tr > td', n => n.textContent);
    expect(tableFirstTimeCell).toEqual('2022');
  });
  it('microdata button is not disabled anymore', async () => {
    const toolbar = await page.$(testidSelector('detoolbar'));
    const hasDisabledMicrodataButton = await toolbar.$eval(
      '[id="microdata"]',
      button => button.disabled,
    );
    expect(hasDisabledMicrodataButton).toEqual(false);
  });
  it('go back to table', async () => {
    const tableButton = await page.$(testidSelector('table-button'));
    await tableButton.click();
  });
  it('previous table should be displayed', async () => {
    await page.waitForSelector(testidSelector('vis-table'));
  });
  it('microdata button is still not disabled', async () => {
    const toolbar = await page.$(testidSelector('detoolbar'));
    const hasDisabledMicrodataButton = await toolbar.$eval(
      '[id="microdata"]',
      button => button.disabled,
    );
    expect(hasDisabledMicrodataButton).toEqual(false);
  });
  it('microdata button click', async () => {
    const toolbar = await page.$(testidSelector('detoolbar'));
    const microdataButton = await toolbar.$('[id="microdata"]');
    await microdataButton.click();
  });
  it('should return to microdata table', async () => {
    await page.waitForSelector(testidSelector('microdata-table'));
  });
  it('microdata hierarchical content', async () => {
    const table = await page.$(testidSelector('microdata-table'));
    const tableContent = await table.$$eval('td', nodes =>
      nodes.map(n => n.textContent),
    );
    const hierarchicalColumn = R.pipe(
      R.splitEvery(10),
      R.map(R.nth(4)),
      R.unnest,
    )(tableContent);
    expect(hierarchicalColumn).toEqual([
      'ROOT_VALUE_1',
      // eslint-disable-next-line no-irregular-whitespace
      '·  LEVEL_1_VALUE_1',
      // eslint-disable-next-line no-irregular-whitespace
      '·  ·  LEVEL_2_VALUE_1',
      // eslint-disable-next-line no-irregular-whitespace
      '·  ·  LEVEL_2_VALUE_2',
      // eslint-disable-next-line no-irregular-whitespace
      '·  LEVEL_1_VALUE_2',
      'ROOT_VALUE_2',
      'ROOT_VALUE_3',
      'ROOT_VALUE_1',
      // eslint-disable-next-line no-irregular-whitespace
      '·  LEVEL_1_VALUE_1',
      // eslint-disable-next-line no-irregular-whitespace
      '·  ·  LEVEL_2_VALUE_1',
    ]);
  });
  it('go back to table', async () => {
    const tableButton = await page.$(testidSelector('table-button'));
    await tableButton.click();
  });
  it('click other obs cell hyperlink', async () => {
    const cell = await page.$('td[headers="REF_AREA=TUR_0_0 3_0 MEASURE=GDO"]');
    const hyperlink = await cell.$('.MuiLink-underlineAlways');
    await hyperlink.click();
  });
  it('should display appropriate microdata table', async () => {
    const table = await page.waitForSelector(testidSelector('microdata-table'));
    const tableFirstTimeCell = await table.$eval('tr > td', n => n.textContent);
    expect(tableFirstTimeCell).toEqual('2023');
  });
  it('go back to table', async () => {
    const tableButton = await page.$(testidSelector('table-button'));
    await tableButton.click();
  });
  it('return to initial microdata table', async () => {
    const cell = await page.$('td[headers="REF_AREA=TUR_0_0 2_0 MEASURE=GDO"]');
    const hyperlink = await cell.$('.MuiLink-underlineAlways');
    await hyperlink.click();
  });
  it('initial microdata table is back', async () => {
    const table = await page.waitForSelector(testidSelector('microdata-table'));
    const tableFirstTimeCell = await table.$eval('tr > td', n => n.textContent);
    expect(tableFirstTimeCell).toEqual('2022');
  });
  it('go back to table', async () => {
    const tableButton = await page.$(testidSelector('table-button'));
    await tableButton.click();
  });
  it('return to second microdata table', async () => {
    const cell = await page.$('td[headers="REF_AREA=TUR_0_0 3_0 MEASURE=GDO"]');
    const hyperlink = await cell.$('.MuiLink-underlineAlways');
    await hyperlink.click();
  });
  it('second microdata table is back', async () => {
    const table = await page.waitForSelector(testidSelector('microdata-table'));
    const tableFirstTimeCell = await table.$eval('tr > td', n => n.textContent);
    expect(tableFirstTimeCell).toEqual('2023');
  });
  it('browser back to initial microdata table', async () => {
    await page.evaluate(() => {
      history.go(-3);
    });
  });
  it('initial microdata table is back', async () => {
    const table = await page.waitForSelector(testidSelector('microdata-table'));
    const tableFirstTimeCell = await table.$eval('tr > td', n => n.textContent);
    expect(tableFirstTimeCell).toEqual('2022');
  });
  it('browser forward to second microdata table', async () => {
    await page.evaluate(() => {
      history.go(3);
    });
  });
  it('second microdata table is back', async () => {
    const table = await page.waitForSelector(testidSelector('microdata-table'));
    const tableFirstTimeCell = await table.$eval('tr > td', n => n.textContent);
    expect(tableFirstTimeCell).toEqual('2023');
  });
  it('open data downloads', async () => {
    await page.evaluate(() => {
      document.querySelector('[data-testid="downloads-button"]').click();
    });
    await page.setRequestInterception(true);
    page.on('request', req => {
      setTimeout(() => req.continue(), 500);
    });
    page.waitFor(100);
  });
  it('should have not disabled filtered data link', async () => {
    await page.waitForSelector(testidSelector('downloads-menu'));
    const menu = await page.$(testidSelector('downloads-menu'));
    const optionIsDisabled = await menu.$eval(
      'a[id="csv.selection"]',
      n => n.ariaDisabled,
    );
    expect(optionIsDisabled).toEqual(null);
  });
  it('click should trigger filtered microdata request', async () => {
    await page.waitForSelector(testidSelector('downloads-menu'));
    await page.evaluate(() => {
      document.querySelector('a[id="csv.selection"]').click();
    });
    await page.waitForResponse(res => {
      const url = res.url();
      const [query, params] = R.split('?', url);
      return (
        R.includes(
          '/data/OECD.DAF,DSD_DEBT_TRANS_DDOWN@DF_DDOWN,1.0/.TUR......GDO.DD.',
          query,
        ) &&
        R.includes('startPeriod=2023', params) &&
        R.includes('endPeriod=2023', params) &&
        R.includes('format=csvfilewithlabels', params)
      );
    });
  });
  it('should display dl info message', async () => {
    const info = await page.$(testidSelector('csv-dl-start'));
    expect(info).not.toEqual(null);
  });
  it('reopen data downloads', async () => {
    await page.evaluate(() => {
      document.querySelector('[data-testid="downloads-button"]').click();
    });
    page.waitFor(100);
  });
  it('should have disabled full data and metadata links', async () => {
    await page.waitForSelector(testidSelector('downloads-menu'));
    const menu = await page.$(testidSelector('downloads-menu'));
    const fullDataOptionIsDisabled = await menu.$eval(
      'a[id="csv.all"]',
      n => n.ariaDisabled,
    );
    const fullMetadataOptionIsDisabled = await menu.$eval(
      'a[id="metadata.all"]',
      n => n.ariaDisabled,
    );
    expect(fullDataOptionIsDisabled).toEqual('true');
    expect(fullMetadataOptionIsDisabled).toEqual('true');
  });
});
