/*
 * This file contains all ids and classes you can find in the data-explorer
 *
 * classes and ids are define like following
 *
 *  - ID_[NAME] are uniq in the all application
 *  - CLASS_[NAME] those classes are commons for all application
 *
 */

export const CLASS_PAGE = 'class_page';

// HOME PAGE
export const ID_HOME_PAGE = 'id_home_page';

// ERROR PAGE
export const ID_ERROR_PAGE = 'id_error_page';

// SEARCH PAGE
export const ID_SEARCH_PAGE = 'id_search_page';

// VIS PAGE
export const ID_VIS_PAGE = 'id_vis_page';

// AUTH PAGE
export const ID_AUTH_PAGE = 'id_auth_page';

// VIEWER COMPONENT
export const ID_VIEWER_COMPONENT = 'id_viewer_component';

// OVERVIEW COMPONENT
export const ID_OVERVIEW_COMPONENT = 'id_overview_component';

// APPBAR
export const ID_APPBAR = 'id_appbar';
