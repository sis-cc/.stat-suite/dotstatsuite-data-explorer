import md5 from 'md5';
import * as R from 'ramda';

export const hashObject = obj => {
  const stringifiedProperties = R.map(val => {
    if (!R.is(Object, val)) {
      return val;
    }
    return JSON.stringify(val, Object.keys(val).sort());
  }, obj);
  const sortedString = JSON.stringify(
    stringifiedProperties,
    Object.keys(stringifiedProperties).sort(),
  );
  return md5(sortedString);
};

export const getDlErrorLog = error => {
  const log = error.response
    ? {
        statusCode: error.response.status,
        message: error.message,
      }
    : { error };
  return log;
};

export const isSameDataflow = (dfA, dfB) =>
  dfA.dataflowId === dfB.dataflowId &&
  dfA.agencyId === dfB.agencyId &&
  dfA.version === dfB.version &&
  dfA.datasourceId === dfB.datasourceId;
