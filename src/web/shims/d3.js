// d3 v3 was exposed in the global scope
// webpack and babel were not as strict as vite and rollup
// with vite and rollup, the global scope is not updated
// which is a very good practice!
// in order to use vite and rollup with d3 v3
// a shim is required to expose manually d3 in the global scope
// this is a temporary shim that will be removed when using d3 v4 or above

import * as d3Module from 'd3';
window.d3 = d3Module;
