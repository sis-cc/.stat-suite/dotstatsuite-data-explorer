import './shims/d3';
import { Provider } from 'react-redux';
import React, { StrictMode } from 'react';
import { createRoot } from 'react-dom/client';
import * as R from 'ramda';
import CssBaseline from '@mui/material/CssBaseline';
import { getLocale } from './selectors/router';
import configureStore, { history } from './configureStore';
import { I18nProvider, initialize as initializeI18n } from './i18n';
import { initialize as initializeAnalytics } from './utils/analytics';
import { ThemeProvider } from './theme';
import searchApi from './api/search';
import shareApi from './api/share';
import { search, i18n, theme } from './lib/settings';
import ErrorBoundary from './components/error-boundary';
import Helmet from './components/helmet';
import meta from '../../package.json';
import { AuthPopUp as Auth } from './lib/oidc/auth';
import { OidcProvider } from './lib/oidc';
import { userSignedIn, userSignedOut, refreshToken } from './ducks/app';
import { getIsFirstRendering } from './selectors/app';
import { QueryCache, QueryClient, QueryClientProvider } from 'react-query';
import { ReactQueryDevtools } from 'react-query/devtools';
import {
  Route,
  Routes,
  createRoutesFromChildren,
  matchRoutes,
  useLocation,
  useNavigationType,
} from 'react-router-dom';
import App from './components/app';
import { ReduxRouter } from '@lagunovsky/redux-react-router';
import Search from './components/search';
import Vis from './components/vis';
import NotFound from './components/not-found';
import Share from './share';
import { StyledEngineProvider } from '@mui/material';
import * as Sentry from '@sentry/react';

console.info(`${meta.name}@${meta.version}`); // eslint-disable-line no-console

const initSentry = () => {
  if (R.isNil(window.CONFIG.sentryDSN)) return;

  Sentry.init({
    dsn: window.CONFIG.sentryDSN,
    environment: window.CONFIG.sentryEnv,
    release: window.CONFIG.sentryRelease,
    integrations: [
      Sentry.reactRouterV6BrowserTracingIntegration({
        useEffect: React.useEffect,
        useLocation,
        useNavigationType,
        createRoutesFromChildren,
        matchRoutes,
      }),
      Sentry.replayIntegration(),
    ],
    profilesSampleRate: 1.0,
    tracesSampleRate: 1.0,
    tracePropagationTargets: ['localhost'],
    replaysSessionSampleRate: 0.1,
    replaysOnErrorSampleRate: 1.0,
  });
};

const initQueryClient = () => {
  return new QueryClient({
    defaultOptions: {
      queries: {
        refetchOnWindowFocus: false,
        refetchOnMount: false,
        retry: false,
        staleTime: 5 * 60 * 1000,
        cacheTime: 10 * 60 * 1000,
      },
    },
    queryCache: new QueryCache({
      onSuccess: (data, query) => {
        if (!R.is(Function, query?.meta?.successHandler)) return;
        query.meta.successHandler({ data, queryKey: query.meta.queryKey });
      },
      onError: (error, query) => {
        if (!R.is(Function, query?.meta?.errorHandler)) return;
        /* if (error.code === 'E_JSON_PARSE' && R.isNil(spaceId)) {
          // redirect to invalid page with proper arg (ie invalid space)
          // when router will be full setup
        }*/
        query.meta.errorHandler({ error, queryKey: query.meta.queryKey });
        /*if (error?.response?.status === 401 && hasExternalAuth)
          dispatch(failedExtAuth(spaceId)); */
      },
    }),
  });
};

const initAuth = (oidc = {}) => {
  if (R.isEmpty(oidc)) return;

  return Auth({
    authority: oidc.authority,
    client_id: oidc.client_id,
    redirect_uri: `${window.location.origin}/signed`,
    scope: R.defaultTo('openid email offline_access', oidc.scope),
    autoRefresh: true,
  });
};

const run = async () => {
  const initialState = {};

  initSentry();
  const queryClient = initQueryClient();
  const store = configureStore(initialState);
  const auth = initAuth(window.CONFIG?.member?.scope?.oidc, store);
  const locale = getLocale(store.getState());

  searchApi.setConfig(search);
  shareApi.setConfig(window.SETTINGS?.share);
  initializeI18n(i18n, locale);

  const container = document.getElementById('root');
  const root = createRoot(container);

  const SentryRoutes = Sentry.withSentryReactRouterV6Routing(Routes);

  const render = () => {
    if (!getIsFirstRendering(store.getState())) return;
    initializeAnalytics({ locale });

    root.render(
      <StrictMode>
        <ErrorBoundary>
          <Provider store={store}>
            <OidcProvider value={auth}>
              <StyledEngineProvider injectFirst>
                <ThemeProvider theme={theme}>
                  <I18nProvider messages={window.I18N}>
                    <ErrorBoundary>
                      <Helmet />
                      <CssBaseline />
                      <ReduxRouter history={history}>
                        <QueryClientProvider client={queryClient}>
                          <App>
                            <SentryRoutes>
                              <Route path="/" element={<Search />} />
                              <Route path="/vis" element={<Vis />} />
                              <Route path="/share" element={<Share />} />
                              <Route path="*" element={<NotFound />} />
                            </SentryRoutes>
                          </App>
                          <ReactQueryDevtools initialIsOpen={true} />
                        </QueryClientProvider>
                      </ReduxRouter>
                    </ErrorBoundary>
                  </I18nProvider>
                </ThemeProvider>
              </StyledEngineProvider>
            </OidcProvider>
          </Provider>
        </ErrorBoundary>
      </StrictMode>,
    );
  };

  if (auth) {
    auth.on('signedIn', ({ access_token, payload }) => {
      const user = {
        given_name: payload.given_name,
        family_name: payload.family_name,
        email: payload.email,
      };
      store.dispatch(userSignedIn(access_token, user));
      render();
    });
    auth.on('tokensRefreshed', ({ access_token }) => {
      store.dispatch(refreshToken(access_token));
    });
    auth.on('signedOut', () => {
      store.dispatch(userSignedOut());
    });
    auth.on('error', event => {
      console.log('error', event); // eslint-disable-line no-console
      render();
    });

    await auth.init();

    window.addEventListener(
      'message',
      event => {
        if (event.origin !== window.location.origin) return;
        if (event.data?.type === 'signed') {
          auth.getAccessTokenFromCode(event.data?.code);
        }
        if (event.data?.type === 'login_required') {
          render();
        }
        if (event.data?.type === 'invalid_request') {
          console.error('invalid_request', event.data?.error_description); // eslint-disable-line no-console
        }
      },
      false,
    );

    if (window.location.pathname === '/signed') {
      const searchParams = new URLSearchParams(window.location.search);
      const error = searchParams.get('error');
      const key = error ? 'error_description' : 'code';
      const payload = {
        type: error ? error : 'signed',
        [key]: searchParams.get(key),
      };
      if (window.opener) {
        // popup
        window.opener.postMessage(payload, window.location.origin);
        window.close();
      } else {
        // iframe
        window.parent.postMessage(payload, window.location.origin);
      }

      return;
    }

    const _frame = window.document.createElement('iframe');

    // shotgun approach
    _frame.style.visibility = 'hidden';
    _frame.style.position = 'absolute';
    _frame.width = 0;
    _frame.height = 0;

    window.document.body.appendChild(_frame);

    _frame.src = `${auth.authorizationUrl}&prompt=none`;
  } else {
    render();
  }
};

run();
