import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { IntlProvider } from 'react-intl';
import { getLocale } from '../selectors/router';

const Provider = ({ messages, children }) => {
  const localeId = useSelector(getLocale);
  return (
    <IntlProvider locale={localeId} key={localeId} messages={messages[localeId]}>
      {React.Children.only(children)}
    </IntlProvider>
  );
};

Provider.propTypes = {
  localeId: PropTypes.string,
  messages: PropTypes.object,
  children: PropTypes.element.isRequired,
};

export default Provider;
