import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  en: { id: 'en' },
  it: { id: 'it' },
  fr: { id: 'fr' },
  ar: { id: 'ar' },
  es: { id: 'es' },
  km: { id: 'km' },
  nl: { id: 'nl' },
  de: { id: 'de' },
  th: { id: 'th' },
  ru: { id: 'ru' },
  zh: { id: 'zh' },
});
