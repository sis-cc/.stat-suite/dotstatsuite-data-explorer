import * as R from 'ramda';
import {
  getActualContentConstraintsArtefact,
  getArtefactCategories,
  getDataflowDescription,
  parseStructure,
} from '@sis-cc/dotstatsuite-sdmxjs';
import { rules2 } from '@sis-cc/dotstatsuite-components';
import { getDefaultCombinations } from '../../settings';
import { getAutomatedSelections } from '../selection';
import {
  getAllHiddenValuesAnnotation,
  getDSDIdentifiers,
  getDataRequestSize,
  getIsTimePeriodInversed,
  getObservationsType,
  getRelatedDataAnnotation,
  getTextAlignAnnotation,
  getAnnotations,
  getVersion,
  getDisableAvailabilityAnnotation,
} from '../accessors/structure';
import {
  getDataRequestRange,
  getIsIncreased,
  getIsTimePeriodDisable,
  getObservationCount,
  getTime,
  getTimePeriodArtefact,
  getTimePeriodBoundaries,
  getTimePeriodContraints,
} from '../structure';
import { getHierarchicalCodelistsReferences } from '../hierarchical-codelist';
import { hclDependentsComputor } from './hcl-dependents';

export const structureComputor = ({ locale, dataflow } = {}) => structure => {
  // version is optional and if not requested, default to response
  const { dataflowId, version, agencyId } = R.mergeRight(
    { version: getVersion(structure) }, // default
    dataflow,
  );

  const artefactId = `${agencyId}:${dataflowId}(${version})`;

  const constraintsArtefact = getActualContentConstraintsArtefact(structure);
  const dataRequestSize = getDataRequestSize(structure);
  const parsedStructure = parseStructure(structure);
  const isTimePeriodDisable = getIsTimePeriodDisable(
    parsedStructure?.timePeriod,
  );
  const timePeriodContraints = getTimePeriodContraints(
    isTimePeriodDisable,
    parsedStructure?.timePeriod,
    parsedStructure?.contentConstraints,
  );
  const timePeriodBoundaries = getTimePeriodBoundaries(timePeriodContraints);
  const automatedSelections = getAutomatedSelections(structure);
  const hiddenValuesAnnotation = getAllHiddenValuesAnnotation(structure);
  const dimensions = parsedStructure?.dimensions;
  const attributes = parsedStructure?.attributes;
  const contentConstraints = parsedStructure?.contentConstraints;
  const timePeriod = parsedStructure?.timePeriod;

  const annotations = getAnnotations(structure);
  const combinationsDefinition = R.pipe(
    rules2.getCombinationDefinitions,
    R.when(R.isEmpty, R.always(getDefaultCombinations(locale))),
  )(annotations, locale);
  const isAvailabilityDisabled = !R.isNil(
    getDisableAvailabilityAnnotation(structure),
  );

  const preparedHclDependentsComputor = hclDependentsComputor({
    isTimePeriodDisable,
    automatedSelections,
    hiddenValuesAnnotation,
    dimensions,
    selection: parsedStructure?.selection,
    attributes,
    contentConstraints,
    params: parsedStructure?.params,
    timePeriodContraints,
    timePeriodBoundaries,
  });

  return {
    // IN_STATE_PROPS
    hiddenValuesAnnotation,
    dimensions,
    attributes,
    microdataDimensionId: parsedStructure?.microdataDimensionId,
    name: parsedStructure?.name,
    dataRequestRange: getDataRequestRange(dataRequestSize),
    timePeriodArtefact: getTimePeriodArtefact({
      timePeriod,
      isTimePeriodDisable,
      timePeriodBoundaries,
    }),
    combinationsDefinition,
    isAvailabilityDisabled,

    // IN_CACHE_PROPS
    dataflowId,
    version: getVersion(structure),
    agencyId,
    relatedData: getRelatedDataAnnotation(structure),
    dsdIdentifiers: getDSDIdentifiers(structure),
    automatedSelections,
    externalResources: parsedStructure?.externalResources,
    dataflowDescription: getDataflowDescription(structure),
    observationsCount: getObservationCount(constraintsArtefact),
    validFrom: R.prop('validFrom', constraintsArtefact),
    contentConstraints,
    hierarchySchemes: getArtefactCategories({
      data: structure?.data,
      artefactId,
    }),
    isIncreased: getIsIncreased(dataRequestSize),
    textAlign: getTextAlignAnnotation(locale)(structure),
    observationsType: getObservationsType(structure),
    defaultLayoutIds: parsedStructure?.layout,

    // IN_ROUTE_PROPS
    time: getTime({
      timePeriod,
      isTimePeriodInversed: getIsTimePeriodInversed(structure),
    }),

    // OTHER PROPS
    preparedHclDependentsComputor,
    hclRefs: R.pipe(getHierarchicalCodelistsReferences, R.values)(structure),
  };
};
