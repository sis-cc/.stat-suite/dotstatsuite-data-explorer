import * as R from 'ramda';
import {
  getActualContentConstraintsDefaultSelection,
  getDataquery,
} from '@sis-cc/dotstatsuite-sdmxjs';
import { parseDataquery, selectAutomatedSelections } from '../selection';
import {
  getDefaultSelectionContentConstrained,
  getOnlyHasDataDimensions,
} from '..';
import { getDefaultSelection } from '../../search/sdmx-constraints-bridge';

const computeStructureSelection = ({
  hasValidDataAvailability,
  selection,
  contentConstraints,
}) => {
  if (R.not(hasValidDataAvailability)) return selection;
  return getActualContentConstraintsDefaultSelection({
    selection,
    contentConstraints,
  });
};

const filterByAnnotationMaybe = hiddenValuesAnnotation => {
  return R.filter(dim =>
    R.pipe(R.propOr([], 'values'), values =>
      hiddenValuesAnnotation[dim.id]
        ? !R.isEmpty(
            R.difference(hiddenValuesAnnotation[dim.id], R.pluck('id')(values)),
          )
        : true,
    )(dim),
  );
};

const computeDefaultSelection = ({
  hiddenValuesAnnotation,
  constraints,
  highlightedContraints,
  contentConstraints = {},
  hasDataAvailability,
  dimensions,
  selection,
}) => {
  const hasValidDataAvailability =
    hasDataAvailability && !R.isEmpty(contentConstraints);

  const structureSelection = computeStructureSelection({
    hasValidDataAvailability,
    selection,
    contentConstraints,
  });

  if (R.not(hasValidDataAvailability)) {
    getDefaultSelection(dimensions, structureSelection, constraints);
  }

  return R.pipe(
    getOnlyHasDataDimensions,
    filterByAnnotationMaybe(hiddenValuesAnnotation),
    dimensions =>
      getDefaultSelection(
        dimensions,
        structureSelection,
        constraints,
        highlightedContraints,
      ),
    getDefaultSelectionContentConstrained(contentConstraints),
    R.reject(R.isEmpty),
  )(dimensions);
};

export const dataqueryComputor = ({
  currentDataquery,
  dimensions,
  automatedSelections,
  hierarchies,
  hiddenValuesAnnotation,
  constraints,
  highlightedContraints,
  contentConstraints,
  hasDataAvailability,
  selection,
}) => {
  if (R.isEmpty(currentDataquery)) {
    return getDataquery(
      dimensions,
      selectAutomatedSelections(
        computeDefaultSelection({
          hiddenValuesAnnotation,
          constraints,
          highlightedContraints,
          contentConstraints,
          hasDataAvailability,
          dimensions,
          selection,
        }),
        dimensions,
        automatedSelections,
        hierarchies,
      ),
    );
  }

  return R.pipe(
    dataquery => parseDataquery(dataquery, dimensions),
    selection =>
      selectAutomatedSelections(
        selection,
        dimensions,
        automatedSelections,
        hierarchies,
      ),
    selection => getDataquery(dimensions, selection),
  )(currentDataquery);
};
