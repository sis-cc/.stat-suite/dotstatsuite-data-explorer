import evtX from 'evtx';
import initContact from './contact';
import initCaptcha from './captcha';
import initSus from './sus';
import { initServices } from '../../utils';
import debug from '../../debug';
import { getUser } from './utils';

const services = [initContact, initCaptcha, initSus];

const init = ctx => {
  const api = evtX(ctx)
    .configure(initServices(services))
    .before(getUser);
  debug.info('api services up');
  return Promise.resolve({ ...ctx, services: { ...ctx.services, api } });
};

export default init;
