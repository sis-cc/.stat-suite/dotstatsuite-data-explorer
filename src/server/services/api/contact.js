import { emitEvent } from './utils';

const SERVICE_NAME = 'contact';

const contact = {
  form(form) {
    return new Promise(resolve => {
      resolve(form);
    });
  },
};

export default evtx =>
  evtx
    .use(SERVICE_NAME, contact)
    .service(SERVICE_NAME)
    .after({
      form: [emitEvent('contact:form')],
    });
