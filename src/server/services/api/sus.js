import got from 'got';
import urljoin from 'url-join';
import { checkUser } from './utils';

const NAME = 'sus';

const service = {
  createOne({ sourceUrl }) {
    const {
      config: { susUrl, appId, shortUrl },
    } = this.globals;
    const {
      req: { member },
      user,
    } = this.locals;

    const url = urljoin(susUrl, '/api/urls/createOne');
    const baseShortenedUrl = urljoin(process.env.SITE_URL, shortUrl);
    const json = {
      authorEmail: user.email,
      appId,
      tenantId: member?.id,
      sourceUrl,
      baseShortenedUrl,
    };

    return got.post(url, { json }).json();
  },
};

export default evtx =>
  evtx
    .use(NAME, service)
    .service(NAME)
    .before({
      createOne: [checkUser()],
    });
