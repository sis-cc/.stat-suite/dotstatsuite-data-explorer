import * as R from 'ramda';
import HTTPStatus from 'http-status';
import { UnknownServiceError, UnknownMethodError } from 'evtx';
import { HTTPError } from '../../utils/errors';

export const parseUrl = url => {
  const re = new RegExp(/^\/+(\w+)\/+(\w+)\/*(\w*)/);
  const [, service, method, id] = re.exec(url) || [];
  return R.map(x => x || undefined, [service, method, id]);
};

const getInput = (id, req) => R.mergeAll([req.query, req.body, id && { id }]);
export const getMessage = req => {
  const [service, method, id] = parseUrl(req.path);
  const input = getInput(id, req);
  return { service, method, input };
};

export default evtx => (req, res, next) => {
  const { service, method, input } = getMessage(req);

  evtx
    .run({ service, method, input }, { req, res })
    .then(result => {
      if (!result) return;
      return res.status(HTTPStatus.OK).json(result);
    })
    .catch(err => {
      if (err instanceof UnknownMethodError)
        return next(new HTTPError(404, `method "${method}" not found`));
      if (err instanceof UnknownServiceError)
        return next(new HTTPError(404, `service "${service}" not found`));
      if (err.code === 'ERR_NON_2XX_3XX_RESPONSE') {
        return next(
          new HTTPError(
            400,
            `${err.message}, service: ${service}, method: ${method}`,
          ),
        );
      }
      return next(err);
    });
};
