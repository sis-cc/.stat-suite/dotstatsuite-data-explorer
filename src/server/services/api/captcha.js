import got from 'got';

const SERVICE_NAME = 'captcha';

// https://developers.google.com/recaptcha/docs/verify
const CAPTCHA_ENDPOINT = 'https://www.google.com/recaptcha/api/siteverify';

const captcha = {
  verify: async function verify({ token }) {
    const { body } = await got.post(
      `${CAPTCHA_ENDPOINT}?secret=${process.env.CAPTCHA_SECRET_KEY}&response=${token}`,
    );
    return new Promise(resolve =>
      resolve({
        isSuccess: JSON.parse(body)?.success,
      }),
    );
  },
};

export default evtx => evtx.use(SERVICE_NAME, captcha).service(SERVICE_NAME);
