/**
 * @jest-environment node
 */

import init from '..';
import ConfigProvider from '../../../configProvider';

const mockedDate = new Date(2017, 11, 10);
global.Date = jest.fn(() => mockedDate);
jest.mock('axios', () => ({
  get: () => Promise.resolve({ data: {} }),
}));

describe('server | services | healthcheck', () => {
  it('should run healthcheck service', done => {
    const ctx = {
      config: { gitHash: 'HASH' },
      configProvider: ConfigProvider({}),
    };
    const tenant = { id: 'TENANT', name: 'LABEL' };
    init(ctx).then(({ services: { healthcheck } }) => {
      healthcheck
        .run({ service: 'healthcheck', method: 'get' }, { req: { tenant } })
        .then(res => {
          expect(res.status).toEqual('OK');
          done();
        });
    });
  });

  it('should have a git hash, a git tag and a version', done => {
    const gitHash = 'HASH';
    const gitTag = 'TAG';
    const version = 'V';
    const ctx = {
      config: { gitHash, gitTag, version },
      configProvider: ConfigProvider({}),
    };
    const tenant = { id: 'TENANT', name: 'LABEL' };
    init(ctx).then(({ services: { healthcheck } }) => {
      healthcheck
        .run({ service: 'healthcheck', method: 'get' }, { req: { tenant } })
        .then(res => {
          expect(res.gitHash).toEqual(gitHash);
          expect(res.gitTag).toEqual(gitTag);
          expect(res.version).toEqual(version);
          done();
        });
    });
  });
});
