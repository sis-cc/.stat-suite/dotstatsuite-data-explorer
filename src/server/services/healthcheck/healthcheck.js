import debug from '../../debug';

const { HTTPError } = require('../../utils/errors');

async function doHealthcheck() {
  const { startTime, gitHash, gitTag, version, configProvider } = this.globals;
  const {
    req: { member },
  } = this.locals;
  try {
    const tenants = await configProvider.getTenants();
    return {
      startTime,
      gitHash,
      gitTag,
      version,
      status: 'OK',
      member: member?.id,
      configProvider: {
        status: 'OK',
        tenants: Object.values(tenants).length,
      },
    };
  } catch (err) {
    debug.error(err);
    throw new HTTPError(500, 'ConfigServer is unreachable');
  }
}

const healthcheck = {
  name: 'healthcheck',
  get: doHealthcheck,
};

const init = evtx => evtx.use(healthcheck.name, healthcheck);

module.exports = init;
