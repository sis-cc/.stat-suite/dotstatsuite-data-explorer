const R = require('ramda');
const evtX = require('evtx').default;
const initHealthcheck = require('./healthcheck');

const services = [initHealthcheck];

const initServices = evtx =>
  R.reduce((acc, service) => acc.configure(service), evtx, services);

const init = ctx => {
  const {
    config: { gitHash },
    configProvider,
  } = ctx;
  const globals = { configProvider, startTime: new Date(), gitHash };
  const healthcheck = evtX(globals).configure(initServices);
  return Promise.resolve({
    ...ctx,
    services: { ...ctx.services, healthcheck },
  });
};

module.exports = init;
