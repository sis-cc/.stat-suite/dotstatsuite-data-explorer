import * as R from 'ramda';
import htmlescape from 'htmlescape';
import debug from '../debug';
import { getLinkTags, getScriptsTags } from '../utils/tags';

const renderHtml = ({
  config,
  assets,
  i18n,
  settings,
  stylesheetUrl,
  app,
  cspNonce,
  scriptTags,
  linkTags,
}) => `
  <!doctype html>
  <html lang="en">
    <head>
      <meta charset="utf-8">
      <meta name="robots" content="${config.robotsPolicy}">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      ${R.pipe(
        R.map(link => `<link href="${link}" rel="stylesheet" async />`),
        R.join('\n'),
      )(linkTags)}
      <link rel="shortcut icon" href="${app.favicon}">
      <link rel="stylesheet" href="/css/preloader.css">
      <style id="insertion-point-jss"></style>
      <style type="text/css">html, body, #root { height: 100%; width: 100%; display: flex; }</style>
      <link rel="stylesheet" href="${stylesheetUrl}">
      <script nonce="${cspNonce}"> CONFIG = ${htmlescape(config)} </script>
      <script nonce="${cspNonce}"> SETTINGS = ${htmlescape(settings)} </script>
      <script nonce="${cspNonce}"> I18N = ${htmlescape(i18n)} </script>
      ${R.pipe(
        R.map(
          script =>
            `<script type="text/javascript" src="${script}" nonce="${cspNonce}" async></script>`,
        ),
        R.join('\n'),
      )(scriptTags)}
    </head>
    <body>
      <noscript>
        You need to enable JavaScript to run this app.
      </noscript>
      <div id="root">
        <div class="loader">
          <svg class="circular" viewBox="22 22 44 44">
            <circle class="path" cx="44" cy="44" r="20.2" fill="none" stroke-width="3.6" />
          </svg>
        </div>
      </div>
      ${
        config.isProduction
          ? `<link rel="modulepreload" crossorigin href="${assets.vendors}" nonce="${cspNonce}"/>`
          : `<script type="text/javascript" src="${assets.vendors}" nonce="${cspNonce}"></script>`
      }
      <script type="${
        config.isProduction ? 'module' : 'text/javascript'
      }" src="${assets.main}" nonce="${cspNonce}"></script>
    </body>
  </html>`;

const ssr = ({ config, assets, configProvider }) => async (req, res) => {
  const { member } = req;
  const settings = await configProvider.getSettings(member);
  const locales = R.pipe(R.pathOr([], ['i18n', 'locales']), R.keys)(settings);
  const i18n = await configProvider.getI18n(member, locales);
  const hasCaptcha = !R.all(R.isNil, [
    config.captchaSiteKey,
    process.env.CAPTCHA_SECRET_KEY,
  ]);

  const html = renderHtml({
    config: {
      ...R.pick(
        [
          'env',
          'gaToken',
          'gtmToken',
          'robotsPolicy',
          'captchaSiteKey',
          'siteEnv',
          'isProduction',
          'sentryDSN',
          'sentryEnv',
          'sentryRelease',
        ],
        config,
      ),
      member,
      hasCaptcha,
    },
    i18n: R.zipObj(locales, i18n),
    assets,
    settings,
    stylesheetUrl: settings.styles,
    app: { favicon: R.pathOr('/favicon.ico', ['app', 'favicon'], settings) },
    cspNonce: res.locals.cspNonce,
    scriptTags: getScriptsTags(settings, req?.member?.scope),
    linkTags: getLinkTags(settings, req?.member?.scope),
  });

  res.set('X-Robots-Tag', config.robotsPolicy);
  res.send(html);

  debug.info(
    `render site for member '${member.id}' with scope '${member?.scope?.id}'`,
  );
};

export default ssr;
