const R = require('ramda');
const parser = require('accept-language-parser');

const getLanguage = req => {
  const acceptedLanguages = req.headers['accept-language'];
  const languages = parser.parse(acceptedLanguages);

  return {
    lang: R.pathOr('en', [0, 'code'], languages),
    region: R.compose(R.toLower, R.pathOr('en', [0, 'region']))(languages),
  };
};

module.exports = {
  getLanguage,
};
