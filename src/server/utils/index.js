import * as R from 'ramda';

export const initResources = resources => ctx =>
  R.reduce((acc, initFn) => acc.then(initFn), Promise.resolve(ctx), resources);

export const initServices = services => evtx =>
  R.reduce((acc, service) => acc.configure(service), evtx, services);
