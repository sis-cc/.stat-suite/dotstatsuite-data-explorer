import { sendMail } from '../../lib/mailer';

const init = ctx => {
  const {
    config: { smtp },
  } = ctx;
  return Promise.resolve({ ...ctx, email: { send: sendMail(smtp) } });
};

export default init;
