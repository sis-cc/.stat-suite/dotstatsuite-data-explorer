const debug = require('../debug');
const path = require('path');
const md5File = require('md5-file/promise');

const BUNDLE_JS_URL = '/static/js/bundle.js';
const VENDORS_JS_URL = '/static/js/vendors~main.chunk.js';
const ASSETS_PATH = '../../../build';

const getUrl = async (isProduction, assetsPath, url) => {
  if (!isProduction) return url;
  const hash = await md5File(path.join(__dirname, assetsPath, url));
  return `${url}.br?${hash}`;
};

const init = async ctx => {
  const {
    config: { isProduction },
  } = ctx;

  const assets = {
    main: await getUrl(isProduction, ASSETS_PATH, BUNDLE_JS_URL),
    vendors: await getUrl(false, ASSETS_PATH, VENDORS_JS_URL),
  };

  debug.info(`compute bundle.js asset: '${assets.main}'`);
  debug.info(`compute vendors.js asset: '${assets.vendors}'`);

  return { ...ctx, assets };
};

module.exports = init;
