import * as R from 'ramda';
import { html, skeleton, header, footer, body } from '../../mail_templates';
import debug from '../../debug';

export const sendEmail = async ({ send, mailOptions, content }) => {
  const options = {
    ...mailOptions,
    from: process.env.MAIL_FROM,
    hfrom: process.env.HFROM,
    html: html({ content }),
  };
  return send(options)
    .then(() => debug.info(`email sent to '${options.to}'`))
    .catch(err => debug.error(err));
};

const prepareEmail = send => async ctx => {
  const { output } = ctx;
  const content = skeleton({
    header: header(R.prop('logo', output)),
    body: body(R.omit(['subject', 'logo', 'mailFrom'], output)),
    footer,
  });
  return sendEmail({
    send,
    content,
    mailOptions: {
      subject: output.subject,
      to: R.defaultTo(process.env.MAIL_TO, output.mailTo),
    },
  });
};

const init = ctx => {
  const {
    email: { send },
    services: { api },
  } = ctx;
  api.service('contact').on('contact:form', prepareEmail(send));
  return Promise.resolve(ctx);
};

export default init;
