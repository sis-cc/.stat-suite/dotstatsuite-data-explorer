import * as R from 'ramda';
import initContact from './contact';

const ressources = [initContact];
const init = ctx =>
  R.reduce((acc, initFn) => acc.then(initFn), Promise.resolve(ctx), ressources);
export default init;
