const helmet = require('helmet');
import cors from 'cors';
import compression from 'compression';
import { crypto } from 'jsrsasign';
import path from 'path';
import express from 'express';
import bodyParser from 'body-parser';
import { healthcheckConnector } from '../services/healthcheck/connector';
import apiConnector from '../services/api/connector';
import errorHandler from '../middlewares/errors';
import tenant from '../middlewares/tenant';
import helmetCSP from '../middlewares/helmet-csp';
import ssr from '../ssr';
import { HTTPError } from '../utils/errors';
import shortener from '../shortener';
import * as R from 'ramda';
import brotli from '../middlewares/brotli';

const BUILD_PATH = path.join(__dirname, '../../../build');
const PUBLIC_PATH = path.join(__dirname, '../../../public');

const checkTenant = (req, _, next) => {
  if (!req.member) return next(new HTTPError(400, 'Tenant required'));
  next();
};

const init = ctx => {
  const app = express();
  app.disable('x-powered-by');

  const {
    services: { healthcheck, api },
    configProvider,
    config: { shortUrl, siteUrl, isProduction },
  } = ctx;

  app.use(cors({ origin: 'http://localhost' }));
  app.use(bodyParser.json({ limit: '50mb' }));
  app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
  app.use(compression());
  app.use(express.static(PUBLIC_PATH));
  app.get('*.js', brotli({ buildPath: BUILD_PATH }));
  app.use(
    express.static(BUILD_PATH, {
      setHeaders: (res, path) => {
        if (path.endsWith('.js.br')) {
          res.setHeader('Content-Type', 'application/javascript');
          res.setHeader('Content-Encoding', 'br');
        }
      },
    }),
  );
  app.use(tenant(configProvider));
  app.get('/robots.txt', async (req, res) => {
    const settings = await configProvider.getSettings(req?.member);
    const robotsPath = R.path(['app', 'robots'], settings);

    res.redirect(302, `${siteUrl}${robotsPath}`);
  });
  app.use((_, res, next) => {
    res.locals.cspNonce = crypto.Util.getRandomHexOfNbytes(16).toString('hex');
    next();
  });
  if (isProduction) {
    app.use(helmet({ crossOriginOpenerPolicy: false }));
    app.use(helmetCSP(configProvider));
  }
  app.use((_, res, next) => {
    res.setHeader(
      'Permissions-Policy',
      'camera=(), fullscreen=(), microphone=(), payment=()',
    );
    next();
  });
  app.get(`${shortUrl}/:id`, shortener(ctx));
  app.get('/api/healthcheck', healthcheckConnector(healthcheck));
  app.use('/api', apiConnector(api));
  app.use(checkTenant, ssr(ctx));
  app.use(errorHandler);

  return Promise.resolve({ ...ctx, app });
};

module.exports = init;
