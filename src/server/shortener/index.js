import axios from 'axios';
import * as R from 'ramda';
import urlJoin from 'url-join';

export default ctx => {
  const {
    config: { susUrl },
  } = ctx;
  return (req, res) => {
    const sid = req.params.id;
    const url = urlJoin(susUrl, `/api/urls/findOne/${sid}`);
    axios
      .get(url)
      .then(R.prop('data'))
      .then(data => {
        res.redirect(302, data.sourceUrl);
      })
      .catch(err => {
        let errorUrl = `/error/shortener/${err.code}`;
        if (err.response?.status) errorUrl += `/${err.response.status}`;
        res.redirect(302, errorUrl);
      });
  };
};
