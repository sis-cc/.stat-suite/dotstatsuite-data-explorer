export default ({ header, body, footer }) => {
  return `
<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#fff">
  <tbody>
    <tr>
      <td>
        <table width="50%" border="0">
          <tbody><tr>
            <td>&nbsp;</td>
          </tr>
        </tbody></table>
      </td>
      <td width="600" valign="top">
        ${header}
        ${body}
        ${footer}
      </td>
      <td>
        <table width="50%" border="0">
          <tbody><tr>
            <td>&nbsp;</td>
          </tr>
        </tbody></table>
      </td>
    </tr>
  </tbody>
</table>`;
};
