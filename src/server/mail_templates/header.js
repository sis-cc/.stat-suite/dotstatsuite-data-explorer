export default logo => {
  return `
<table width="600" border="0" cellpadding="0" cellspacing="0">
  <tbody>
    <tr style="background: #0965c1;">
      <td style="background: #0965c1; padding: 0px 10px;">
        <a rel="noopener noreferrer" href="#">
          <img align="left" src="https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-config-data/-/raw/develop/assets/siscc/share/data-explorer-logo.png" alt="data explorer logo">
        </a>
      </td>
      <td align="right" style="background: #0965c1; padding: 0px 10px;">
        <a rel="noopener noreferrer" href="#">
          <img align="right" src=${logo} alt="siscc logo">
        </a>
      </td>
    </tr>
  </tbody>
</table>`;
};
