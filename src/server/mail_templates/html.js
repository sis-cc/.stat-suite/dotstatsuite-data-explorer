export default ({ content, css = '' }) => {
  return `
      <!doctype html>
        <html>
          <head>
           <style>${css}</style>
         </head>
       <body>
       <div>${content}</div>
       </body>
       </html>
    `;
};
