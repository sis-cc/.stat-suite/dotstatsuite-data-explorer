import * as R from 'ramda';

export default props => {
  const tableRows = R.pipe(
    R.mapObjIndexed((value, key) => {
      return `
        <tr width="600">
          <td style="text-align: start; vertical-align: top;background-color: #e2f2fb; font-weight:bold;padding: 10px;">${key}</td>
          <td style="text-align: start; vertical-align: top;background-color: #e2f2fb;padding: 10px; word-break:break-all;">${value}</td>
        </tr>
      `;
    }),
    R.values,
    R.join('\n'),
  )(props);

  return `
<table  border="1" frame=hsides rules=rows width="600" cellpadding="0" cellspacing="0" border="0" style="table-layout: fixed; color:#222; font-family: Arial, Helvetica, sans-serif; font-size:16px; background-color: #e2f2fb;">
  <thead>
    <tr >
      <th style="padding: 10px; width: 30%">Label</th>
      <th style="padding: 10px;width: 70%">Value</th>
    </tr>
    </thead>
  <tbody width="600">
    ${tableRows}
  </tbody>
</table>
`;
};
