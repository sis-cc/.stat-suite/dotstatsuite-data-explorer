/**
 * @jest-environment node
 */

import axios from 'axios';
import jsdom from 'jsdom';
import initHttp from '../init/http';
import initRouter from '../init/router';
import initServices from '../services';
import initConfig from '../init/config';
import initAssets from '../init/assets';

const { JSDOM } = jsdom;
let CTX;

jest.mock('../configProvider', () => () => ({
  getI18n: () => Promise.resolve(['FAKE_TRANSLATIONS']),
  getTenants: () => Promise.resolve({ testTenant: { id: 'testTenant' } }),
  getTenant: () =>
    Promise.resolve({
      id: 'testTenant',
      scopes: {
        de: {
          scriptTags: [
            'https://from-tenants-json?queryparam=invisible',
            'https://should-be-uniq?queryparam=invisible',
          ],
        },
      },
    }),
  getSettings: () =>
    Promise.resolve({
      i18n: { locales: { FAKE_LANG: {} } },
      app: {
        scriptTags: [
          'https://from-settings-json?queryparam=invisible',
          'https://should-be-uniq',
        ],
      },
    }),
}));

describe('Main', function() {
  beforeAll(() =>
    initConfig()
      .then(ctx => ({ ...ctx, config: { ...ctx.config, env: 'ENV' } }))
      .then(initAssets)
      .then(ctx => ({
        ...ctx,
        assets: { ...ctx.assets, main: 'MAIN', vendors: 'VENDORS' },
      }))
      .then(initServices)
      .then(initRouter)
      .then(initHttp)
      .then(ctx => (CTX = ctx)),
  );

  afterAll(() => CTX.httpServer.close());

  it('should render page', () => {
    const url = `${CTX.httpServer.url}`;
    return axios({ url }).then(({ data }) => {
      const dom = new JSDOM(data, { runScripts: 'dangerously' });
      expect(dom.window.I18N).toEqual({ FAKE_LANG: 'FAKE_TRANSLATIONS' });
      expect(dom.window.SETTINGS).toEqual({
        i18n: { locales: { FAKE_LANG: {} } },
        app: {
          scriptTags: [
            'https://from-settings-json?queryparam=invisible',
            'https://should-be-uniq',
          ],
        },
      });
      expect(dom.window.CONFIG.member.id).toEqual('testTenant');
      expect(dom.window.CONFIG.env).toEqual('ENV');
      expect(
        dom.window.document.querySelector('script[src=VENDORS]'),
      ).toBeDefined();
      expect(
        dom.window.document.querySelector('script[src=MAIN]'),
      ).toBeDefined();
    });
  });

  xit('should have scriptTags in CSP headers', async () => {
    const url = `${CTX.httpServer.url}`;
    const response = await axios.get(url);
    expect(response.headers).toBeDefined();
  });
});
