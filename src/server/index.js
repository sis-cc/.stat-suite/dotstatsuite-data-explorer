import debug from './debug';
import initConfig from './init/config';
import initAssets from './init/assets';
import initRouter from './init/router';
import initHttp from './init/http';
import initEmails from './init/emails';
import initListeners from './init/listeners';
import initServices from './services';
import { initResources } from './utils';

const resources = [
  initAssets,
  initServices,
  initRouter,
  initHttp,
  initEmails,
  initListeners,
];

initConfig()
  .then(initResources(resources))
  .then(() => debug.info('🚀 server started'))
  .catch(err => debug.error(err));
