const server = {
  host: process.env.SERVER_HOST || '0.0.0.0',
  port: Number(process.env.SERVER_PORT) || 7002,
};

const proxy = {
  host: process.env.PROXY_HOST || '0.0.0.0',
  port: Number(process.env.PROXY_PORT) || 7000,
};

module.exports = {
  server,
  proxy,
  configUrl: process.env.CONFIG_URL || 'http://localhost:5007',
};
