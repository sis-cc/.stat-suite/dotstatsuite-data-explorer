import path from 'path';
import { getSMTPEnv } from '../../lib/mailer';
import meta from '../../../package.json';

const config = {
  appId: 'data-explorer',
  env: process.env.NODE_ENV || 'development',
  isProduction: process.env.NODE_ENV === 'production',
  gitHash: process.env.GIT_HASH || 'local',
  gitTag: process.env.GIT_TAG || 'local',
  version: meta.version,
  publicPath: path.join(__dirname, '../../../public'),
  buildPath: path.join(__dirname, '../../../build'),
  gaToken: process.env.GA_TOKEN || '',
  gtmToken: process.env.GTM_TOKEN || '',
  robotsPolicy: process.env.ROBOTS_POLICY || 'noindex, nofollow',
  smtp: getSMTPEnv(process.env),
  captchaSiteKey: process.env.CAPTCHA_SITE_KEY,
  siteEnv: process.env.SITE_ENV || '',
  susUrl: process.env.SUS_URL || 'http://localhost:7104',
  siteUrl: process.env.SITE_URL || '',
  shortUrl: '/s',
  sentryDSN: process.env.SENTRY_DSN,
  sentryEnv: process.env.SENTRY_ENV || 'local',
  sentryRelease: process.env.SENTRY_RELEASE || 'none',
};

module.exports = config;
