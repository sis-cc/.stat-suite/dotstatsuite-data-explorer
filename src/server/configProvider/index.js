const axios = require('axios');
const R = require('ramda');
const { HTTPError } = require('../utils/errors');
const debug = require('../debug');

export const getResourceUri = (uri, path, base = '/configs') =>
  `${uri}${base}${path}`;
export const getI18nUri = (tenantId, appId, lang) =>
  `/${tenantId}/${appId}/i18n/${lang}.json`;
export const getSettingsUri = (tenantId, appId) =>
  `/${tenantId}/${appId}/settings.json`;
export const getTenantsUri = () => '/tenants.json';

const getResource = ({ configUrl }, options = {}) => path =>
  axios
    .get(getResourceUri(configUrl, path, options.base))
    .then(({ data }) => data)
    .catch(err => {
      if (
        err.response &&
        err.response.status === 404 &&
        'notFoundValue' in options
      )
        return options.notFoundValue;
      if (err.response)
        debug.error(`Cannot get ${getResourceUri(configUrl, path)}`);
      if (err.code === 'ECONNREFUSED') {
        debug.error(`Cannot connect to config server: ${configUrl}`);
        throw err;
      }
      const status = R.prop('status', err.response);
      if (status) throw new HTTPError(status);
      throw err;
    });

const getI18n = config => ({ id }, langs = []) =>
  Promise.all(
    langs.map(lang =>
      Promise.all([
        getResource(config, { notFoundValue: {}, base: '/i18n' })(
          `/${lang}.json`,
        ),
        getResource(config, { notFoundValue: {} })(
          getI18nUri(id, config.appId, lang),
        ),
      ]).then(([base, overrides]) => R.mergeRight(base, overrides)),
    ),
  );

const getDefaultTenant = R.compose(
  R.find(tenant => tenant.default),
  R.values,
);
const getTenants = config => () => getResource(config)(getTenantsUri());
const getTenant = config => async id => {
  const tenants = await getTenants(config)();
  const tenant = id ? tenants?.[id] : getDefaultTenant(tenants);
  if (!tenant)
    throw new HTTPError(404, `Unknown tenant '${id}' from config server`);
  return tenant;
};

const getSettings = config => ({ id }) =>
  getResource(config, { notFoundValue: {} })(getSettingsUri(id, config.appId));

const provider = config => ({
  getI18n: getI18n(config),
  getTenant: getTenant(config),
  getTenants: getTenants(config),
  getSettings: getSettings(config),
  getResource: getResource(config),
});

export default provider;
