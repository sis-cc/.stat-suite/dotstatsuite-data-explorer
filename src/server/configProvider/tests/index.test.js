/**
 * @jest-environment node
 */

import axios from 'axios';
import ConfigProvider, { getI18nUri, getResourceUri } from '..';

const HTTP_FAKE_URI = 'http://FAKE_URI';
const TENANT_ID = 'TENANT_ID';
const APP_ID = 'APP_ID';
const FAKE_LANGS = ['FAKE_LANG'];
const PATH = 'PATH';

jest
  .spyOn(axios, 'get')
  .mockImplementation(uri => Promise.resolve({ data: { uri } }));

describe('server | configProvider', () => {
  it('should get i18n content via http', () => {
    const config = { appId: 'APP_ID', configUrl: HTTP_FAKE_URI };
    const provider = ConfigProvider(config);
    return provider.getI18n({ id: TENANT_ID }, FAKE_LANGS).then(([data]) => {
      const uri = getI18nUri(TENANT_ID, APP_ID, 'FAKE_LANG');
      expect(data.uri).toEqual(getResourceUri(config.configUrl, uri));
    });
  });

  it('should get settings.json content via http', () => {
    const config = { appId: 'APP_ID', configUrl: HTTP_FAKE_URI };
    const provider = ConfigProvider(config);
    return provider.getResource(PATH).then(data => {
      const uri = getResourceUri(config.configUrl, PATH);
      expect(data.uri).toEqual(uri);
    });
  });
});
