const helmet = require('helmet');
const { getScripts, getLinks, getDomains } = require('../utils/tags');
import * as R from 'ramda';

const helmetCSP = configProvider => async (req, res, next) => {
  const settings = await configProvider.getSettings(req?.member);

  const authorisedDomains = getDomains({ path: ['app', 'authorisedDomains'] })(
    settings,
  );

  return helmet.contentSecurityPolicy({
    useDefaults: true,
    directives: {
      'upgrade-insecure-requests': null,
      'default-src': ["'none'"], // fallback to none
      'connect-src': ['*'], // enabling to connect to sfs, nsi, share...
      'script-src': [
        `'nonce-${res.locals.cspNonce}'`,
        "'self'",
        "'strict-dynamic'",
        "'unsafe-inline'",
        'https://www.google-analytics.com',
        'https://www.googletagmanager.com',
        ...authorisedDomains,
        ...getScripts(settings, req?.member?.scope),
      ],
      'style-src': [
        "'self'",
        "'unsafe-inline'", // app don't work
        ...authorisedDomains,
        ...getLinks(settings, req?.member?.scope),
      ],
      'font-src': ["'self'", 'data:', 'fonts.gstatic.com'],
      'frame-src': [
        "'self'",
        'https://www.google.com', // recaptcha
        req => {
          const authority = req?.member?.scope?.oidc?.authority;
          if (!authority) return '';
          return R.pipe(
            R.defaultTo([]),
            R.prepend(authority),
            R.map(alias => new URL(alias).host),
            R.join(' '),
          )(req?.member?.scope?.oidc?.authority_aliases);
        },
      ],
      'img-src': ['*', 'data:', ...authorisedDomains],
    },
  })(req, res, next);
};

module.exports = helmetCSP;
