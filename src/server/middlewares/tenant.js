import * as R from 'ramda';
import debug from '../debug';

const getSpace = space => ({
  ...R.omit(['urlv2'], space),
  url: space.urlv2 || space.url,
});

const getScopedMember = (tenant, scopeId) => {
  const member = R.omit(['spaces', 'datasources', 'scopes'], tenant);
  const scope = tenant?.scopes?.[scopeId];
  if (!scope) return member;
  member.scope = R.omit(['datasources', 'spaces'], scope);
  member.scope.id = scopeId;

  member.scope.spaces = {};
  for (const spaceId of R.propOr([], 'spaces', scope)) {
    const space = tenant.spaces?.[spaceId];
    if (space) member.scope.spaces[spaceId] = getSpace(space);
  }

  member.scope.datasources = {};
  for (const datasourceId of R.propOr([], 'datasources', scope)) {
    // using member.scope.spaces for datasources could spare the preparation of the space
    const space =
      tenant.spaces?.[tenant.datasources?.[datasourceId]?.dataSpaceId];
    if (space) member.scope.datasources[datasourceId] = getSpace(space);
  }

  return member;
};

const getTenant = configProvider => async (req, res, next) => {
  try {
    const t0 = new Date();
    const tenantSlug = req.query.tenant || req.headers['x-tenant'];
    const [tenantId, scopeId = 'de'] = tenantSlug
      ? tenantSlug.split(':')
      : [null, 'de'];
    const tenant = await configProvider.getTenant(tenantId);
    // useless for now because no tenant is impossible with the current state of express slices
    if (!tenant) return next();
    req.member = getScopedMember(tenant, scopeId);
    if (tenantSlug)
      debug.info(
        `load member '${tenant?.id}' with scope '${
          req.member?.scope?.id
        }' in ${new Date() - t0} ms`,
      );
    else if (tenant)
      debug.info(
        `load default member '${tenant?.id}' with scope '${
          req.member?.scope?.id
        }' in ${new Date() - t0} ms`,
      );
    next();
  } catch (err) {
    next(err);
  }
};

module.exports = getTenant;
