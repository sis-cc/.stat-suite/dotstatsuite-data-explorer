const path = require('path');
const fs = require('fs');

export default ({ buildPath }) => (req, res, next) => {
  const filePath = path.join(buildPath, req.url);
  const brotliFilePath = `${filePath}.br`;

  if (fs.existsSync(brotliFilePath)) {
    res.setHeader('Content-Encoding', 'br');
    res.setHeader('Content-Type', 'application/javascript');
    res.sendFile(brotliFilePath);
  } else {
    next();
  }
};
