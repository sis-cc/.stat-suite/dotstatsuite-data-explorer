const nodemailer = require('nodemailer');
const debug = require('../../server/debug');

const fakeSendMail = () => Promise.resolve();

export const sendMail = smtpConfig => {
  if (!smtpConfig) return fakeSendMail;
  const transporter = nodemailer.createTransport(smtpConfig);
  return mailOptions => {
    const promise = new Promise((resolve, reject) => {
      transporter.sendMail({ ...mailOptions }, (err, res) => {
        if (err) return reject(err);
        debug.info(`Mail started`);
        resolve(res);
      });
    });
    return promise;
  };
};
