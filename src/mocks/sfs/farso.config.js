const path = require('path');

module.exports = {
  host: 'localhost',
  vibes: path.join(__dirname, './vibes.js'),
  endpoints: path.join(__dirname, './endpoints.js'),
};
