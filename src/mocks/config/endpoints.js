const express = require('express');
const path = require('path');
const { endpoint } = require('farso');

endpoint('tenants', { uri: '/configs/tenants.json', method: 'get' });
endpoint('assets', { uri: '/assets', use: express.static(path.join(__dirname, './assets')) });
endpoint('i18n', { uri: '/i18n', use: express.static(path.join(__dirname, './i18n')) });
endpoint('settings', { uri: '/configs/oecd/data-explorer/settings.json', method: 'get' });
