const { vibe } = require('farso');
const R = require('ramda');
const tenants = require('./tenants.js');
const settings = require('./settings.js');

const defaultCombinations = {
  concepts: 'COMBINED_UNIT_MEASURE:UNIT_MEASURE,UNIT_MULT,BASE_PER',
  names: { en: 'COMBINED_UNIT_MEASURE:Combined unit of measure' },
};

vibe.default('main', mock => {
  mock('tenants').reply((_, res) => res.json(tenants(process.env.NSI_URL)));
  mock('settings').reply((_, res) =>
    res.json(settings(process.env.SFS_URL, defaultCombinations)),
  );
});

vibe.default('main:fr', mock => {
  mock('tenants').reply((_, res) => res.json(tenants(process.env.NSI_URL)));
  mock('settings').reply((_, res) =>
    res.json(
      settings(process.env.SFS_URL, defaultCombinations, [0, 2499], 2499, 'fr'),
    ),
  );
});

vibe.default('noCsvLink', mock => {
  mock('tenants').reply((_, res) =>
    res.json(
      R.over(
        R.lensPath(['oecd', 'spaces', 'hybrid']),
        R.assoc('supportsCsvFile', false),
        tenants(process.env.NSI_URL),
      ),
    ),
  );
  mock('settings').reply((_, res) =>
    res.json(settings(process.env.SFS_URL, defaultCombinations)),
  );
});

const combinations = {
  concepts: 'COMBINATION:REF_AREA,VALUATION,TIME_PERIOD',
  names: { en: 'COMBINATION:my combination' },
};

vibe.default('combinations', mock => {
  mock('tenants').reply((_, res) => res.json(tenants(process.env.NSI_URL)));
  mock('settings').reply((_, res) =>
    res.json(settings(process.env.SFS_URL, combinations)),
  );
});

vibe.default('range', mock => {
  mock('tenants').reply((_, res) => res.json(tenants(process.env.NSI_URL)));
  mock('settings').reply((_, res) =>
    res.json(settings(process.env.SFS_URL, combinations, [0, 5], 10)),
  );
});

vibe.default('no_metadata', mock => {
  mock('tenants').reply((_, res) =>
    res.json(
      R.over(
        R.lensPath(['oecd', 'spaces', 'hybrid']),
        R.dissoc('urlv3'),
        tenants(process.env.NSI_URL),
      ),
    ),
  );
  mock('settings').reply((_, res) =>
    res.json(settings(process.env.SFS_URL, defaultCombinations)),
  );
});
