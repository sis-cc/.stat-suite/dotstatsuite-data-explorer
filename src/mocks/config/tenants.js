module.exports = function(nsiUrl) {
  return {
    oecd: {
      id: 'oecd',
      label: 'oecd',
      default: true,
      spaces: {
        'demo-release': {
          id: 'demo-release',
          label: 'demo-release',
          isExternal: false,
          hasExternalAuth: false,
          hasRangeHeader: true,
          supportsReferencePartial: true,
          hasLastNObservations: false,
          url: nsiUrl,
          urlv3: nsiUrl,
          headers: {
            data: {
              csv: 'application/vnd.sdmx.data+json;version=2.0',
              json: 'application/vnd.sdmx.data+json;version=2.0',
            },
          },
        },
        'demo-reset': {
          id: 'demo-reset',
          label: 'demo-reset',
          isExternal: false,
          hasExternalAuth: false,
          hasRangeHeader: true,
          supportsReferencePartial: true,
          hasLastNObservations: false,
          url: nsiUrl,
          urlv3: nsiUrl,
          headers: {
            data: {
              csv: 'application/vnd.sdmx.data+json;version=2.0',
              json: 'application/vnd.sdmx.data+json;version=2.0',
            },
          },
        },
        'demo-design': {
          id: 'demo-design',
          label: 'demo-design',
          hasRangeHeader: true,
          supportsReferencePartial: true,
          hasLastNObservations: true,
          url: nsiUrl,
          urlv3: nsiUrl,
          headers: {
            data: {
              csv: 'application/vnd.sdmx.data+json;version=2.0',
              json: 'application/vnd.sdmx.data+json;version=2.0',
            },
          },
        },
        hybrid: {
          id: 'hybrid',
          label: 'hybrid',
          url: nsiUrl,
          urlv3: nsiUrl,
          hasRangeHeader: true,
          supportsReferencePartial: true,
          hasLastNObservations: true,
          supportsPostLongRequests: true,
          headers: {
            data: {
              csv: 'application/vnd.sdmx.data+json;version=2.0',
              json: 'application/vnd.sdmx.data+json;version=2.0',
            },
          },
        },
      },
      datasources: {
        'ds-demo-release': {
          dataSpaceId: 'demo-release',
          indexed: true,
          dataqueries: [
            {
              version: '1.0',
              categorySchemeId: 'OECDCS1',
              agencyId: 'OECD',
            },
          ],
        },
        'ds-demo-reset': {
          dataSpaceId: 'demo-reset',
          indexed: true,
          dataqueries: [
            {
              version: '1.0',
              categorySchemeId: 'OECDCS1',
              agencyId: 'OECD',
            },
          ],
        },
      },
      scopes: {
        de: {
          type: 'de',
          label: 'de',
          spaces: ['demo-design', 'demo-release', 'hybrid'],
          datasources: ['ds-demo-release', 'ds-demo-reset'],
        },
      },
    },
  };
};
