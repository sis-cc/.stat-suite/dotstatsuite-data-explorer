const R = require('ramda');
const { endpoint } = require('farso');

const ids = (id = 'SNA_TABLE1') => ['OECD', id, '1.0'];
const s_ids = R.join('/');
const c_ids = R.join(',');

endpoint('sna:structure', {
  uri: `/rest/dataflow/${s_ids(ids())}`,
  method: 'get',
});
endpoint('sna:structure:no_ver', {
  uri: `/rest/dataflow/${s_ids(['OECD', 'SNA_TABLE1', ''])}`,
  method: 'get',
});
endpoint('sna:data', {
  uri: `/rest/data/${c_ids(ids())}/:query`,
  method: 'get',
});
endpoint('sna:data-post', {
  uri: `/rest/data/${c_ids(ids())}/:query`,
  method: 'post',
});
endpoint('sna:data:no_ver', {
  uri: `/rest/data/${c_ids(['OECD', 'SNA_TABLE1', ''])}/:query`,
  method: 'get',
});
endpoint('sna:data:constraint', {
  uri: `/rest/availableconstraint/${c_ids(ids())}/:query`,
  method: 'get',
});
endpoint('sna:availableConstraint:time', {
  uri: `/rest/availableconstraint/${c_ids(ids())}/:query/ALL/TIME_PERIOD`,
  method: 'get',
});
endpoint('sna:data:no_ver:constraint', {
  uri: `/rest/availableconstraint/${c_ids(['OECD', 'SNA_TABLE1', ''])}/:query`,
  method: 'get',
});
endpoint('sna:availableConstraint:no_ver:time', {
  uri: `/rest/availableconstraint/${c_ids([
    'OECD',
    'SNA_TABLE1',
    '',
  ])}/:query/ALL/TIME_PERIOD`,
  method: 'get',
});
endpoint('sna:full_metadata', {
  uri: `/rest/data/dataflow/${s_ids(ids())}/`,
  method: 'get',
});
endpoint('sna:metadata', {
  uri: `/rest/data/dataflow/${s_ids(ids())}/:query`,
  method: 'get',
});
endpoint('sna:metadata:no_ver', {
  uri: `/rest/data/dataflow/${s_ids(['OECD', 'SNA_TABLE1', '~'])}/:query`,
  method: 'get',
});
endpoint('sna:metadata:structure', {
  uri: `/rest/metadatastructure/${s_ids(ids('MSD_SNA_TABLE1'))}`,
  method: 'get',
});
endpoint('itcs:structure', {
  uri: `/rest/dataflow/${s_ids([
    'OECD.PAC.DCMI',
    'DSD_ITCS_HS2012@DF_ITCS_HS2012',
    '1.0',
  ])}`,
  method: 'get',
});
endpoint('itcs:data', {
  uri: `/rest/data/${c_ids([
    'OECD.PAC.DCMI',
    'DSD_ITCS_HS2012@DF_ITCS_HS2012',
    '1.0',
  ])}/:query`,
  method: 'get',
});
endpoint('itcs:constraint', {
  uri: `/rest/availableconstraint/${c_ids([
    'OECD.PAC.DCMI',
    'DSD_ITCS_HS2012@DF_ITCS_HS2012',
    '1.0',
  ])}/:query`,
  method: 'get',
});

endpoint('eon:structure', {
  uri: `/rest/dataflow/OECD.ECO/DF_EO_N/1.2`,
  method: 'get',
});
endpoint('eon:data', {
  uri: `/rest/data/OECD.ECO,DF_EO_N,1.2/:dq`,
  method: 'get',
});
endpoint('eon:availableconstraint', {
  uri: `/rest/availableconstraint/OECD.ECO,DF_EO_N,1.2/:dq`,
  method: 'get',
});
endpoint('eon:availableconstraint:time', {
  uri: `/rest/availableconstraint/OECD.ECO,DF_EO_N,1.2/:dq/ALL/TIME_PERIOD`,
  method: 'get',
});

endpoint('qna_comb:external:structure', {
  uri: `/rest/dataflow/OECD.SDD.NAD/DSD_QNA_COMB@DF_QNA_COMB_EXT/1.0`,
  method: 'get',
});
endpoint('qna_comb:structure', {
  uri: `/rest/dataflow/OECD.SDD.NAD/DSD_QNA_COMB@DF_QNA_COMB/1.0`,
  method: 'get',
});
endpoint('qna_comb:hierarchicalcodelist:sector', {
  uri: `/rest/hierarchicalcodelist/OECD.SDD.NAD/HCL_SECTOR/1.1`,
  method: 'get',
});
endpoint('qna_comb:hierarchicalcodelist:measure', {
  uri: `/rest/hierarchicalcodelist/OECD.SDD.NAD/HCL_MEASURE/1.0`,
  method: 'get',
});
endpoint('qna_comb:data', {
  uri: `/rest/data/OECD.SDD.NAD,DSD_QNA_COMB@DF_QNA_COMB,1.0/:dq`,
  method: 'get',
});
endpoint('qna_comb:availableconstraint', {
  uri: `/rest/availableconstraint/OECD.SDD.NAD,DSD_QNA_COMB@DF_QNA_COMB,1.0/:dq`,
  method: 'get',
});
endpoint('qna_comb:availableconstraint:time', {
  uri: `/rest/availableconstraint/OECD.SDD.NAD,DSD_QNA_COMB@DF_QNA_COMB,1.0/:dq/ALL/TIME_PERIOD`,
  method: 'get',
});

endpoint('ddown:structure', {
  uri: `/rest/dataflow/OECD.DAF/DSD_DEBT_TRANS_DDOWN@DF_DDOWN/1.0`,
  method: 'get',
});
endpoint('ddown:data', {
  uri: `/rest/data/OECD.DAF,DSD_DEBT_TRANS_DDOWN@DF_DDOWN,1.0/........_T.`,
  method: 'get',
});
endpoint('ddown:availableconstraint', {
  uri: `/rest/availableconstraint/OECD.DAF,DSD_DEBT_TRANS_DDOWN@DF_DDOWN,1.0/:dq`,
  method: 'get',
});
endpoint('ddown:availableconstraint:time', {
  uri: `/rest/availableconstraint/OECD.DAF,DSD_DEBT_TRANS_DDOWN@DF_DDOWN,1.0/:dq/ALL/TIME_PERIOD`,
  method: 'get',
});
endpoint('ddown:microdata', {
  uri: `/rest/data/OECD.DAF,DSD_DEBT_TRANS_DDOWN@DF_DDOWN,1.0/.TUR......GDO.DD.`,
  method: 'get',
});
endpoint('ddown:no-freq:structure', {
  uri: `/rest/dataflow/OECD.DAF/DSD_DEBT_TRANS_DDOWN@DF_DDOWN/2.0`,
  method: 'get',
});
endpoint('ddown:no-freq:data', {
  uri: `/rest/data/OECD.DAF,DSD_DEBT_TRANS_DDOWN@DF_DDOWN,2.0/........_T.`,
  method: 'get',
});
endpoint('ddown:no-freq:availableconstraint:time', {
  uri: `/rest/availableconstraint/OECD.DAF,DSD_DEBT_TRANS_DDOWN@DF_DDOWN,2.0/:dq/ALL/TIME_PERIOD`,
  method: 'get',
});

endpoint('types:structure', {
  uri: `/rest/dataflow/${s_ids(['OECD', 'DF_TEST_GREGORIANDAY', '1.0'])}`,
  method: 'get',
});
endpoint('types:data', {
  uri: `/rest/data/${c_ids(['OECD', 'DF_TEST_GREGORIANDAY', '1.0'])}/:query`,
  method: 'get',
});
endpoint('types:availableconstraint', {
  uri: `/rest/availableconstraint/${c_ids([
    'OECD',
    'DF_TEST_GREGORIANDAY',
    '1.0',
  ])}/:query`,
  method: 'get',
});

endpoint('no_time:structure', {
  uri: `/rest/dataflow/${s_ids(['STATSNZ', 'CEN18_HOU', '1.0'])}`,
  method: 'get',
});
endpoint('no_time:data', {
  uri: `/rest/data/${c_ids(['STATSNZ', 'CEN18_HOU', '1.0'])}/:query`,
  method: 'get',
});
endpoint('no_time:constraint', {
  uri: `/rest/availableconstraint/${c_ids([
    'STATSNZ',
    'CEN18_HOU',
    '1.0',
  ])}/:query`,
  method: 'get',
});
endpoint('no_time:metadata', {
  uri: `/rest/data/dataflow/${s_ids(['STATSNZ', 'CEN18_HOU', '1.0'])}/:query`,
  method: 'get',
});
endpoint('no_time:metadata:structure', {
  uri: `/rest/metadatastructure/${s_ids([
    'STATSNZ',
    'MSD_CEN18_HOU_001',
    '1.0',
  ])}`,
  method: 'get',
});

endpoint('irregular:structure', {
  uri: `/rest/dataflow/${s_ids(['OECD', 'DF_KEI', '1.0'])}`,
  method: 'get',
});
endpoint('irregular:data', {
  uri: `/rest/data/${c_ids(['OECD', 'DF_KEI', '1.0'])}/:query`,
  method: 'get',
});
endpoint('irregular:constraint', {
  uri: `/rest/availableconstraint/${c_ids(['OECD', 'DF_KEI', '1.0'])}/:query`,
  method: 'get',
});

endpoint('perf:structure', {
  uri: `/rest/dataflow/${s_ids(['OECD.GOV', 'DF_GOV_8_9', '2.0'])}`,
  method: 'get',
});
endpoint('perf:data', {
  uri: `/rest/data/${c_ids(['OECD.GOV', 'DF_GOV_8_9', '2.0'])}/:query`,
  method: 'get',
});
endpoint('perf:constraint', {
  uri: `/rest/availableconstraint/${c_ids([
    'OECD.GOV',
    'DF_GOV_8_9',
    '2.0',
  ])}/:query`,
  method: 'get',
});

endpoint('crs1:structure', {
  uri: `/rest/dataflow/${s_ids(['OECD.DCD', 'DSD_CRS@DF_CRS1', '*'])}`,
  method: 'get',
});

endpoint('crs1:hierarchicalcodelist:recipient', {
  uri: `/rest/hierarchicalcodelist/OECD/HCL_RECIPIENT/1.0`,
  method: 'get',
});
endpoint('crs1:availableconstraint', {
  uri: `/rest/availableconstraint/OECD.DCD,DSD_CRS@DF_CRS1,*/:dq`,
  method: 'get',
});
endpoint('crs1:timeconstraint', {
  uri: `/rest/availableconstraint/OECD.DCD,DSD_CRS@DF_CRS1,*/:dq/TIME_PERIOD`,
  method: 'get',
});

endpoint('nase:structure', {
  uri: `/rest/dataflow/${s_ids([
    'OECD.SDD.NAD',
    'DSD_NASEC20@DF_T7II_Q',
    '1.0',
  ])}`,
  method: 'get',
});
endpoint('nase:data', {
  uri: `/rest/data/${c_ids([
    'OECD.SDD.NAD',
    'DSD_NASEC20@DF_T7II_Q',
    '1.0',
  ])}/:query`,
  method: 'get',
});

endpoint('freq:structure', {
  uri: `/rest/dataflow/${s_ids([
    'OECD.SDD.TPS',
    'DSD_PRICES@DF_PRICES_ALL',
    '1.0',
  ])}`,
  method: 'get',
});
endpoint('freq:data', {
  uri: `/rest/data/${c_ids([
    'OECD.SDD.TPS',
    'DSD_PRICES@DF_PRICES_ALL',
    '1.0',
  ])}/:query`,
  method: 'get',
});
endpoint('freq:availableconstraint', {
  uri: `/rest/availableconstraint/OECD.SDD.TPS,DSD_PRICES@DF_PRICES_ALL,1.0/:dq`,
  method: 'get',
});

endpoint('relatedDfs:structure', {
  uri: `/rest/dataflow/${s_ids(['OECD.CFE', 'DF_ALL_TOURISM_TRIPS', '5.0'])}`,
  method: 'get',
});

endpoint('relatedDfs:custom:structure', {
  uri: `/rest/dataflow/${s_ids(['OECD.CFE', 'DF_ALL_TOURISM_TRIPS', '5.1'])}`,
  method: 'get',
});

endpoint('relatedDfs:custom:query', {
  uri: `/rest/customQuery`,
  method: 'get',
});

endpoint('relatedDfs:parents', {
  uri: '/rest/datastructure/OECD.CFE/DSD_TOURISM_TRIPS/5.0',
  method: 'get',
});

endpoint('relatedDfs:data', {
  uri: `/rest/data/${c_ids([
    'OECD.CFE',
    'DF_ALL_TOURISM_TRIPS',
    '5.0',
  ])}/:query`,
  method: 'get',
});

endpoint('freqAttr:structure', {
  uri: `/rest/dataflow/${s_ids(['OECD', 'DF_AIR_EMISSIONS', '2.1'])}`,
  method: 'get',
});
endpoint('freqAttr:data', {
  uri: `/rest/data/${c_ids(['OECD', 'DF_AIR_EMISSIONS', '2.1'])}/:dq`,
  method: 'get',
});
endpoint('freqAttr:time-constraint', {
  uri: `/rest/availableconstraint/${c_ids([
    'OECD',
    'DF_AIR_EMISSIONS',
    '2.1',
  ])}/:dq/ALL/TIME_PERIOD`,
  method: 'get',
});
endpoint('freqAttr:constraints', {
  uri: `/rest/availableconstraint/${c_ids([
    'OECD',
    'DF_AIR_EMISSIONS',
    '2.1',
  ])}/:dq`,
  method: 'get',
});
