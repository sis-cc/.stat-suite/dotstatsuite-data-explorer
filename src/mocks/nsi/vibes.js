const { vibe } = require('farso');
const R = require('ramda');
const snaStructure = require('./mocks/sna-structure.json');
const snaData = require('./mocks/sna-data.json');
const snaContraint = require('./mocks/sna-constraint.json');
const snaMetadata = require('./mocks/sna-metadata.json');
const snaMetadataStructure = require('./mocks/sna-metadata.json');
const itcsStructure = require('./mocks/itcs-structure.json');
const itcsData = require('./mocks/itcs-data.json');
const itcsConstraint = require('./mocks/itcs-constraint.json');
const eonStructure = require('./mocks/eon/structure.json');
const eonData = require('./mocks/eon/data.json');
const eonAvailableConstraint = require('./mocks/eon/available-constraint.json');
const qnaCombStructure = require('./mocks/qna_comb-structure.json');
const qnaCombMeasureHcoldelist = require('./mocks/qna_comb-h_codelist-measure.json');
const qnaCombSectorHcoldelist = require('./mocks/qna_comb-h_codelist-sector.json');
const qnaCombData = require('./mocks/qna_comb-data.json');
const qnaCombConstraint = require('./mocks/qna_comb-constraint.json');
const ddownStructure = require('./mocks/ddown-structure.json');
const ddownData = require('./mocks/ddown-data.json');
const ddownConstraint = require('./mocks/ddown-constraint.json');
const ddownMicrodata = require('./mocks/ddown-microdata.json');
const typesStructure = require('./mocks/types-structure.json');
const typesData = require('./mocks/types-data.json');
const typesConstraint = require('./mocks/types-constraint.json');
const noTimeStructure = require('./mocks/no-time-structure.json');
const noTimeData = require('./mocks/no-time-data.json');
const noTimeConstraint = require('./mocks/no-time-constraint.json');
const noTimeMetadataStructure = require('./mocks/no-time-metadata-structure.json');
const noTimeMetadata = require('./mocks/no-time-metadata.json');
const irregularStructure = require('./mocks/irregular-structure.json');
const irregularData = require('./mocks/irregular-data.json');
const irregularConstraint = require('./mocks/irregular-constraint.json');
const perfStructure = require('./mocks/perf/structure.json');
const perfData = require('./mocks/perf/data.json');
const perfConstraint = require('./mocks/perf/constraints.json');
const crs1Structure = require('./mocks/crs1-structure.json');
const crs1HCodelistRecipient = require('./mocks/crs1-h_codelist-recipient.json');
const crs1Constraint = require('./mocks/crs1-constraint.json');
const naseStructure = require('./mocks/nase/structure.json');
const naseData = require('./mocks/nase/data.json');
const multipleFreqStructure = require('./mocks/multiple-freq-structure.json');
const multipleFreqData = require('./mocks/multiple-freq-data.json');
const multipleFreqConstraints = require('./mocks/multiple-freq-constraints.json');
const relatedDfsStructure = require('./mocks/relatedDfs/structure.json');
const relatedDfsStructureParent = require('./mocks/relatedDfs/structure-parents.json');
const relatedDfsData = require('./mocks/relatedDfs/data.json');
const relatedDfsCustom = require('./mocks/relatedDfs/custom-query.json');
const dfAirStructure = require('./mocks/freqAttr/df_air-structure.json');
const dfAirData = require('./mocks/freqAttr/df_air-data.json');
const dfAirTimeConstraint = require('./mocks/freqAttr/df_air-time-constraint.json');
const dfAirConstraints = require('./mocks/freqAttr/df_air-constraints.json');

vibe('sna', mock => {
  mock('sna:structure').reply((_, res) => res.json(snaStructure));
  mock('sna:data')
    .checkQuery({ format: 'csvfilewithlabels' })
    .reply((_, res) => {
      res.attachment('data.csv');
      res.send('content');
    });
  mock('sna:data').reply((req, res) => {
    if (R.propEq('5', 'lastNObservations', req.query)) {
      res.json(
        R.set(
          R.lensPath([
            'data',
            'structures',
            0,
            'dimensions',
            'observation',
            3,
            'values',
          ]),
          [
            {
              start: '2023-01-01T00:00:00Z',
              end: '2023-12-31T23:59:59Z',
              id: '2023',
              name: '2023',
            },
            {
              start: '2024-01-01T00:00:00Z',
              end: '2024-12-31T23:59:59Z',
              id: '2024',
              name: '2024',
            },
          ],
          snaData,
        ),
      );
    } else {
      res.json(snaData);
    }
  });
  mock('sna:data:constraint').reply((_, res) => res.json(snaContraint));
  mock('sna:availableConstraint:time').reply((_, res) =>
    res.json(snaContraint),
  );
  mock('sna:full_metadata')
    .checkQuery({ format: 'csvfilewithlabels' })
    .reply((_, res) => {
      res.attachment('full_metadata.csv');
      res.send('content');
    });
  mock('sna:metadata')
    .checkQuery({ format: 'csvfilewithlabels' })
    .reply((_, res) => {
      res.attachment('metadata.csv');
      res.send('content');
    });
  mock('sna:metadata').reply((_, res) => res.json(snaMetadata));
  mock('sna:metadata:structure').reply((_, res) =>
    res.json(snaMetadataStructure),
  );
  mock('sna:structure:no_ver').reply((_, res) =>
    res.json(
      R.set(
        R.lensPath(['data', 'conceptSchemes', 0, 'concepts', 0, 'name']),
        'Countries(Latest)',
        snaStructure,
      ),
    ),
  );
  mock('sna:data:no_ver').reply((_, res) =>
    res.json(
      R.set(
        R.lensPath(['data', 'structures', 0, 'name']),
        'Latest Dataflow',
        snaData,
      ),
    ),
  );
  mock('sna:data:no_ver:constraint').reply((_, res) =>
    res.json(
      R.over(
        R.lensPath([
          'data',
          'contentConstraints',
          0,
          'cubeRegions',
          0,
          'keyValues',
          2,
          'values',
        ]),
        R.tail,
        snaContraint,
      ),
    ),
  );
  mock('sna:availableConstraint:no_ver:time').reply((_, res) =>
    res.json(
      R.over(
        R.lensPath([
          'data',
          'contentConstraints',
          0,
          'cubeRegions',
          0,
          'keyValues',
          2,
          'values',
        ]),
        R.tail,
        snaContraint,
      ),
    ),
  );
  mock('sna:metadata:no_ver').reply((_, res) =>
    res.json(
      R.set(
        R.lensPath([
          'data',
          'structures',
          0,
          'attributes',
          'dimensionGroup',
          1,
          'values',
          1,
          'value',
          'en',
        ]),
        'Boomerang',
        snaMetadata,
      ),
    ),
  );
});

vibe('sna:dl-error', mock => {
  mock('sna:data')
    .checkQuery({ format: 'csvfilewithlabels' })
    .reply((_, res) => {
      res.status(404).send({ error: 'Not Found' });
    });
  mock('sna:full_metadata')
    .checkQuery({ format: 'csvfilewithlabels' })
    .reply((_, res) => {
      res.status(404).send({ error: 'Not Found' });
    });
  mock('sna:metadata:structure').reply((_, res) =>
    res.json(snaMetadataStructure),
  );
  mock('sna:metadata')
    .checkQuery({ format: 'csvfilewithlabels' })
    .reply((_, res) => {
      res.status(404).send({ error: 'Not Found' });
    });
  mock('sna:metadata').reply((_, res) => res.json(snaMetadata));
});

vibe('sna:content-range', mock => {
  mock('sna:structure').reply((_, res) => res.json(snaStructure));
  mock('sna:data').reply((_, res) => {
    res.set('content-range', 'values 0-9/20');
    return res.json(snaData);
  });
  mock('sna:data:constraint').reply((_, res) => res.json(snaContraint));
});

vibe('sna:too-long', mock => {
  mock('sna:structure').reply((_, res) => res.json(snaStructure));
  mock('sna:data').reply((_, res) => {
    res.status(414).send({ error: 'Too Long' });
  });
  mock('sna:data-post').reply((_, res) => {
    return res.json(snaData);
  });
  mock('sna:data:constraint').reply((_, res) => res.json(snaContraint));
});

vibe('itcs', mock => {
  mock('itcs:structure').reply((_, res) => res.json(itcsStructure));
  mock('itcs:data').reply((_, res) => res.json(itcsData));
  mock('itcs:constraint').reply((_, res) => res.json(itcsConstraint));
});

vibe('oecd', mock => {
  mock('eon:structure').reply((_, res) => res.json(eonStructure));
  mock('eon:data').reply((_, res) => res.json(eonData));
  mock('eon:availableconstraint').reply((_, res) =>
    res.json(eonAvailableConstraint),
  );
  mock('eon:availableconstraint:time').reply((_, res) =>
    res.json(eonAvailableConstraint),
  );
});

vibe.default('qna_comb', mock => {
  mock('qna_comb:external:structure').reply((req, res) => {
    return res.json(
      R.pipe(
        R.over(R.lensProp('data'), R.pick(['dataflows'])),
        R.over(
          R.lensPath(['data', 'dataflows', 0]),
          R.omit(['annotations', 'structure']),
        ),
        R.set(
          R.lensPath(['data', 'dataflows', 0, 'isExternalReference']),
          true,
        ),
        R.set(
          R.lensPath(['data', 'dataflows', 0, 'id']),
          'DSD_QNA_COMB@DF_QNA_COMB_EXT',
        ),
        R.over(
          R.lensPath(['data', 'dataflows', 0, 'links']),
          R.prepend({
            rel: 'external',
            href: `http://localhost:64650/rest/dataflow/OECD.SDD.NAD/DSD_QNA_COMB@DF_QNA_COMB/1.0`,
          }),
        ),
      )(qnaCombStructure),
    );
  });
  mock('qna_comb:structure').reply((_, res) => res.json(qnaCombStructure));
  mock('qna_comb:hierarchicalcodelist:sector').reply((_, res) =>
    res.json(qnaCombSectorHcoldelist),
  );
  mock('qna_comb:hierarchicalcodelist:measure').reply((_, res) =>
    res.json(qnaCombMeasureHcoldelist),
  );
  mock('qna_comb:data').reply((_, res) => res.json(qnaCombData));
  mock('qna_comb:availableconstraint').reply((_, res) =>
    res.json(qnaCombConstraint),
  );
  mock('qna_comb:availableconstraint:time').reply((_, res) =>
    res.json(qnaCombConstraint),
  );
});

vibe('ddown', mock => {
  mock('ddown:structure').reply((_, res) => res.json(ddownStructure));
  mock('ddown:data').reply((_, res) => res.json(ddownData));
  mock('ddown:availableconstraint').reply((_, res) =>
    res.json(ddownConstraint),
  );
  mock('ddown:availableconstraint:time').reply((_, res) =>
    res.json(ddownConstraint),
  );
  mock('ddown:microdata')
    .checkQuery({ format: 'csvfilewithlabels' })
    .reply((_, res) => {
      res.attachment('data.csv');
      res.send('content');
    });
  mock('ddown:microdata').reply((req, res) => {
    if (R.propEq('2023', 'startPeriod', req.query)) {
      return res.json(
        R.set(
          R.lensPath([
            'data',
            'structures',
            0,
            'dimensions',
            'observation',
            10,
            'values',
          ]),
          [
            {
              start: '2023-01-01T00:00:00',
              end: '2023-12-31T23:59:59',
              id: '2023',
              name: '2023',
              names: {
                en: '2023',
              },
            },
            {
              start: '2024-01-01T00:00:00',
              end: '2024-12-31T23:59:59',
              id: '2024',
              name: '2024',
              names: {
                en: '2024',
              },
            },
          ],
          ddownMicrodata,
        ),
      );
    }
    return res.json(ddownMicrodata);
  });
  mock('ddown:no-freq:structure').reply((_, res) =>
    res.json(
      R.over(
        R.lensPath([
          'data',
          'dataStructures',
          0,
          'dataStructureComponents',
          'attributeList',
          'attributes',
        ]),
        R.filter(a => a.id !== 'FREQ'),
      )(ddownStructure),
    ),
  );
  mock('ddown:no-freq:data')
    .checkQuery({ startPeriod: '2065' })
    .reply((_, res) => res.json(ddownData));
  mock('ddown:no-freq:availableconstraint:time').reply((_, res) =>
    res.json(ddownConstraint),
  );
});

vibe('types', mock => {
  mock('types:structure').reply((_, res) => res.json(typesStructure));
  mock('types:data').reply((_, res) => res.json(typesData));
  mock('types:availableconstraint').reply((_, res) =>
    res.json(typesConstraint),
  );
});

vibe('types:boolean', mock => {
  mock('types:structure').reply((_, res) =>
    res.json(
      R.set(
        R.lensPath([
          'data',
          'dataStructures',
          0,
          'dataStructureComponents',
          'measureList',
          'primaryMeasure',
          'localRepresentation',
          'textFormat',
          'textType',
        ]),
        'Boolean',
        typesStructure,
      ),
    ),
  );
  mock('types:data').reply((_, res) => res.json(typesData));
  mock('types:availableconstraint').reply((_, res) =>
    res.json(typesConstraint),
  );
});

vibe('no_time', mock => {
  mock('no_time:structure').reply((_, res) => res.json(noTimeStructure));
  mock('no_time:data').reply((_, res) => res.json(noTimeData));
  mock('no_time:constraint').reply((_, res) => res.json(noTimeConstraint));
  mock('no_time:metadata:structure').reply((_, res) =>
    res.json(noTimeMetadataStructure),
  );
  mock('no_time:metadata').reply((_, res) => res.json(noTimeMetadata));
});

vibe('irregular', mock => {
  mock('irregular:structure').reply((_, res) => res.json(irregularStructure));
  mock('irregular:data').reply((_, res) => res.json(irregularData));
  mock('irregular:constraint').reply((_, res) => res.json(irregularConstraint));
});

vibe('perf', mock => {
  mock('perf:structure').reply((_, res) => res.json(perfStructure));
  mock('perf:data').reply((_, res) => res.json(perfData));
  mock('perf:constraint').reply((_, res) => res.json(perfConstraint));
});

vibe('crs1', mock => {
  mock('crs1:structure').reply((req, res) => {
    if (R.endsWith('2.0', req.path)) {
      return res.json(
        R.over(
          R.lensPath(['data', 'dataflows', 0, 'annotations']),
          R.append({ type: 'ALWAYS_DISPLAY_PARENTS', title: 'RECIPIENT' }),
          crs1Structure,
        ),
      );
    } else if (R.endsWith('3.0', req.path)) {
      return res.json(
        R.over(
          R.lensPath(['data', 'dataflows', 0, 'annotations']),
          R.append({
            type: 'ALWAYS_DISPLAY_PARENTS',
            title: 'RECIPIENT=LEVEL2',
          }),
          crs1Structure,
        ),
      );
    } else if (R.endsWith('4.0', req.path)) {
      return res.json(
        R.over(
          R.lensPath(['data', 'dataflows', 0, 'annotations']),
          R.append({ type: 'DISABLE_AVAILABILITY_REQUESTS' }),
          crs1Structure,
        ),
      );
    }
    return res.json(crs1Structure);
  });
  mock('crs1:hierarchicalcodelist:recipient').reply((_, res) =>
    res.json(crs1HCodelistRecipient),
  );
  mock('crs1:availableconstraint').reply((_, res) => res.json(crs1Constraint));
  mock('crs1:timeconstraint').reply((_, res) => res.json(crs1Constraint));
});

vibe('nase', mock => {
  mock('nase:structure').reply((_, res) => res.json(naseStructure));
  mock('nase:data').reply((_, res) => res.json(naseData));
});

vibe('freq', mock => {
  mock('freq:structure').reply((_, res) => res.json(multipleFreqStructure));
  mock('freq:data').reply((_, res) => res.json(multipleFreqData));
  mock('freq:availableconstraint').reply((_, res) =>
    res.json(multipleFreqConstraints),
  );
});

vibe('relatedDfs', mock => {
  mock('relatedDfs:structure').reply((_, res) => res.json(relatedDfsStructure));
  mock('relatedDfs:custom:structure').reply((_, res) =>
    res.json(
      R.over(
        R.lensPath(['data', 'dataflows', 0, 'annotations']),
        R.append({ type: 'RELATED_DATA', title: 'customQuery' }),
        relatedDfsStructure,
      ),
    ),
  );
  mock('relatedDfs:custom:query').reply((_, res) => res.json(relatedDfsCustom));
  mock('relatedDfs:parents').reply((_, res) =>
    res.json(relatedDfsStructureParent),
  );
  mock('relatedDfs:data').reply((_, res) => res.json(relatedDfsData));
});

vibe('freqAttr', mock => {
  mock('freqAttr:structure').reply((_, res) => res.json(dfAirStructure));
  mock('freqAttr:data').reply((_, res) => res.json(dfAirData));
  mock('freqAttr:time-constraint').reply((_, res) =>
    res.json(dfAirTimeConstraint),
  );
  mock('freqAttr:constraints').reply((_, res) => res.json(dfAirConstraints));
});
