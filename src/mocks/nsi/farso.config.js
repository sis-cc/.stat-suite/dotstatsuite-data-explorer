const path = require('path');

module.exports = {
  host: 'localhost',
  port: '64650',
  vibes: path.join(__dirname, './vibes.js'),
  endpoints: path.join(__dirname, './endpoints.js'),
};
