FROM node:18-alpine

ARG GIT_HASH
ARG GIT_REF_NAME

RUN mkdir -p /opt
WORKDIR /opt

RUN echo $GIT_HASH
RUN echo $GIT_REF_NAME
RUN echo $SENTRY_RELEASE

COPY build /opt/build
COPY dist /opt/dist
COPY src/lib/react-compiler-runtime /opt/src/lib/react-compiler-runtime
COPY package.json package-lock.json /opt/

RUN npm install --omit=dev --legacy-peer-deps --force

ENV GIT_HASH=$GIT_HASH
ENV GIT_TAG=$GIT_REF_NAME

EXPOSE 80
CMD yarn start:run
