module.exports = {
  env: {
    browser: true,
    jest: true,
    node: true,
    es6: true,
  },
  settings: {
    react: {
      version: '18.0',
    },
  },
  extends: [
    'eslint:recommended',
    'plugin:react/recommended',
    'plugin:jsx-a11y/recommended',
  ],
  parser: 'babel-eslint',
  parserOptions: {
    ecmaFeatures: {
      experimentalObjectRestSpread: true,
      jsx: true,
    },
    sourceType: 'module',
  },
  plugins: [
    'prettier',
    'react',
    'import',
    'jsx-a11y',
    'eslint-plugin-react-compiler',
  ],
  rules: {
    'no-console': 'warn',
    'no-unused-vars': 'error',
    'no-duplicate-imports': 'error',
    'react/display-name': 'off',
    'jsx-a11y/no-access-key': 'error',
    'no-use-before-define': 'error',
    'import/no-namespace': [
      'error',
      { ignore: ['ramda', 'd3', '@sentry/react'] },
    ],
    'react-compiler/react-compiler': 'error',
    'react/prop-types': 0,
  },
};
